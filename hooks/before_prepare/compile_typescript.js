#!/usr/bin/env node

var fs = require('fs');
var exec = require('child_process').exec;

function abortIfError(err) {
    if (err != null) {
        console.log(err);
        process.exit(1);
    }
}

function queryConfig(callback) {
    fs.readFile('tsconfig.json', function (error, data) {
        abortIfError(error);

        callback(JSON.parse(data));
    });
}

function processCallback(userCallback) {
    return function (error, stdout, stderr) {
        console.log(stdout);
        console.log(stderr);

        if (error !== null) {
            console.log(error);
        }

        if (stderr || error) {
            process.exit(1);
        } else if (userCallback) {
            userCallback();
        }
    };
}

function editTypescriptOutput(path, callback) {
    fs.readFile(path, function (error, data) {
        abortIfError(error);

        var str = data.toString();
        var moduleName = str.match(/^var (.+);/m)[1];
        var extendsDef = str.match(/^(var __extends [^]*^\};)/m);

        extendsDef = extendsDef ? extendsDef[1] : '';

        str = str.replace(
            new RegExp(
                '(^var ' + moduleName + ';$)'
                + '|'
                + '(^\\(function \\(' + moduleName + '\\) \\{$)'
                + '|'
                + '(\\}\\)\\(' + moduleName + ' \\|\\| \\(' + moduleName + ' = \\{\\}\\)\\);)', 'mg'
            ),
            ''
        );

        str = str
            .replace(extendsDef, '')
            .replace(/^$[\s\S]/gm, '')
            .replace(/^\/\/\/.*$[\s\S]/gm,'')
            .replace(/^\/\*[\s\S]*?\*\/[\s\S]/gm, '');

        var prefix = "'use strict';\n" + extendsDef + "\nvar " + moduleName + ";\n" + "(function (" + moduleName + ") {";
        var suffix = "})(" + moduleName + " || (" + moduleName + " = {}));\n";

        fs.writeFile(path, new Buffer(prefix + str + suffix), function (error) {
            abortIfError(error);

            if (callback) {
                callback();
            }
        });
    });
}



function editTypescriptOutputFromConfig(callback) {
    queryConfig(function (config) {
        editTypescriptOutput(config.compilerOptions.out, callback);
    });
}

function compileTypescript(callback) {
    exec('tsc', processCallback(callback));
}

compileTypescript(function () {
    editTypescriptOutputFromConfig();
});