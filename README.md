RxDiagram demonstrates some capabilities of Javascript RxJS library. 
RxJS 2.5.3 is used internally in this app.

![IMG_0033 copy 2.PNG](https://bitbucket.org/repo/deAgg7/images/2870033881-IMG_0033%20copy%202.PNG)

### You can: ###

* create observables;
* add numeric or error values;
* select Rx.Observable methods;
* adjust method parameters;
* observe the results of actual Javascript code.

### Get ###

* Download RxDiagram in [Yandex Store](https://store.yandex.com/) (Android)
* [Check](http://iyegoroff.bitbucket.org/rxdiagram/www/index.html) it in your browser (Firefox, Chrome, Safari)

### Build ###

To build the app you should have **cordova** and **typescript** installed.

build for android: 
```
#!bash
cordova run android
```

build for ios:
```
#!bash
cordova run ios
```

build for browser:
```
#!bash
cordova run browser
```