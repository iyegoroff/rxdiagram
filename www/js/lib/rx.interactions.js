/**
 * Created by iyegoroff on 17/01/15.
 */


(function () {
    'use strict';

    var rxia = (function () {
        var isTouchSupported = ('ontouchstart' in window)
            || (navigator.maxTouchPoints > 0)
            || (navigator.msMaxTouchPoints > 0);

        var downEvent = isTouchSupported ? 'touchstart' : 'mousedown';
        var moveEvent = isTouchSupported ? 'touchmove' : 'mousemove';
        var upEvent = isTouchSupported ? 'touchend' : 'mouseup';
        var cancelEvent = 'touchcancel';
        var x = isTouchSupported ? 'pageX' : 'clientX';
        var y = isTouchSupported ? 'pageY' : 'clientY';
        var startX  = isTouchSupported ? 'startX' : x;
        var startY  = isTouchSupported ? 'startY' : y;

        var extractTouch = function (observable, preventPointer, prevTouch) {
            var b = typeof preventPointer == 'boolean';
            var pt = b ? prevTouch : preventPointer;
            var pp = b ? preventPointer : true;

            if (pt === undefined) {
                return observable.flatMap(function (event) {
                    var touches = event.targetTouches;

                    for (var i = 0, length = touches.length; i < length; i += 1) {
                        touches[i][startX] = touches[i][x];
                        touches[i][startY] = touches[i][y];
                        touches[i].pointerCount = length;
                    }

                    if (pp) {
                        //console.log('preventDefault');
                        event.preventDefault();
                    }

                    return Rx.Observable.fromArray(touches);
                });
            } else {
                return observable
                    .flatMap(function (event) {
                        var pointerCount = event.targetTouches.length;
                        var touches = event.changedTouches;

                        for (var i = 0, length = touches.length; i < length; i += 1) {
                            touches[i].pointerCount = pointerCount;
                        }

                        return Rx.Observable.fromArray(touches);
                    })
                    .filter(function (touch) {
                        return touch.identifier === pt.identifier;
                    });
            }
        };

        var extractClick = function (observable) {
            return observable.map(function (x) {
                x.pointerCount = 1;
                return x;
            });
        };

        var extractPointer = isTouchSupported ? extractTouch : extractClick;

        var extractPointerFromEvent = function (element, eventName, preventPointer, prevTouch) {
            return extractPointer(Rx.Observable.fromEvent(element, eventName), preventPointer, prevTouch);
        };

        var removeEvent = function () {
            return Rx.Observable.merge(
                Rx.Observable.fromEvent(document, upEvent),
                Rx.Observable.fromEvent(document, cancelEvent)
            );
        };

        var removeStream = function (event) {
            return extractPointer(removeEvent(), event).take(1);
        };

        var preventDefault = isTouchSupported
            ? function () {}
            : function (event) { /*console.log('preventDefault');*/ event.preventDefault(); };

        var distance = function (x1, y1, x2, y2) {
            return Math.sqrt(Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2));
        };

        var angle = function (x1, y1, x2, y2) {
            return (360 - Math.atan2(y2 - y1, x2 - x1) * 180 / Math.PI) % 360;
        };

        var xOf = function (event) {
            return event[x];
        };

        var yOf = function (event) {
            return event[y];
        };

        var extendEvent = function (event, startX, startY, endX, endY) {
            if (arguments.length === 1) {
                event.distance = 0;
                event.angle = 0;
                event.centerX = event[x];
                event.centerY = event[y];
            } else {
                event.distance = distance(startX, startY, endX, endY);
                event.angle = angle(startX, startY, endX, endY);
                event.centerX = (startX + endX) / 2;
                event.centerY = (startY + endY) / 2;
            }

            event.scale = 1;
            event.rotation = 0;

            return event;
        };

        var extendPinchEvent = function (event, downFirst, downSecond, initialDistance) {
            var ev = extendEvent(event, downFirst[x], downFirst[y], downSecond[x], downSecond[y]);
            ev.scale = ev.distance / initialDistance;

            return ev;
        };

        var extendRotateEvent = function (event, downFirst, downSecond, initialAngle) {
            var ev = extendEvent(event, downFirst[x], downFirst[y], downSecond[x], downSecond[y]);
            ev.rotation = initialAngle - ev.angle;

            return ev;
        };

        var controlledBufferWithCount = function (observable, count, adder) {
            var buffer = [];

            return observable
                .map(function (value) {
                    adder(buffer, value);
                    return buffer;
                })
                .filter(function (buf) {
                    return buf.length === count;
                });
        };

        var empty = function () {
            return Rx.Observable.empty();
        };

        var fixLeak = function (hotObservable) {
            return hotObservable
                .merge(Rx.Observable.just(null))
                .skip(1);
        };

        var tapUpEvent = function (down, time, threshold) {
            return extractPointerFromEvent(document, upEvent, down)
                .take(1)
                .takeUntilWithTime(time)
                .takeUntil(Rx.Observable.merge(
                    extractPointerFromEvent(document, cancelEvent, down),
                    extractPointerFromEvent(document, moveEvent, down)
                        .filter(function (move) {
                            return distance(down[startX], down[startY], move[x], move[y]) > threshold;
                        })
                ))
                .map(function (up) {
                    return extendEvent(up, down[startX], down[startY], up[x], up[y]);
                });
        };

        var tapDownEvent = function (element, interval, time, threshold, posThreshold, taps, event, preventPointer) {
            var iterate = function (taps, events) {
                if (taps) {
                    return extractPointerFromEvent(element, downEvent, preventPointer)
                        .take(1)
                        .takeUntilWithTime(interval)
                        .flatMap(function (down) {
                            if (preventPointer) {
                                preventDefault(down);
                            }

                            return tapUpEvent(down, time, threshold)
                                .filter(function (up) {
                                    return events.every(function (ev) {
                                        return distance(ev.centerX, ev.centerY, up.centerX, up.centerY) <= posThreshold;
                                    });
                                })
                                .flatMap(function (up) {
                                    return iterate(taps - 1, events.concat(up));
                                });
                        });
                } else {
                    var first = events[0];
                    var last = events[events.length - 1];

                    first.distance = distance(first[x], first[y], last[x], last[y]);
                    first.angle = angle(first[x], first[y], last[x], last[y]);
                    first.centerX = events.reduce(function (x, ev) { return x + ev.centerX; }, 0) / events.length;
                    first.centerY = events.reduce(function (y, ev) { return y + ev.centerY; }, 0) / events.length;

                    return Rx.Observable.just(first);
                }
            };

            return iterate(taps, [event]);
        };

        var multiTap = function (element, options) {
            var taps = options && options.taps || 3;
            var interval = options && options.interval || 300;
            var time = options && options.time || 250;
            var threshold = (options && (options.threshold !== undefined)) ? options.threshold : 2;
            var posThreshold = (options && (options.posThreshold !== undefined)) ? options.posThreshold : 10;
            var preventPointer = options && options.preventDefault;

            if (preventPointer === undefined) {
                preventPointer = true;
            }

            var sequenceFinished = true;

            return fixLeak(extractPointerFromEvent(element, downEvent, preventPointer)
                .flatMap(function (down) {
                    if (preventPointer) {
                        preventDefault(down);
                    }

                    if (sequenceFinished) {
                        sequenceFinished = false;

                        return tapUpEvent(down, time, threshold)
                            .flatMap(function (up) {
                                return tapDownEvent(
                                    element,
                                    interval,
                                    time,
                                    threshold,
                                    posThreshold,
                                    taps - 1,
                                    up,
                                    preventPointer
                                );
                            })
                            .doOnCompleted(function () {
                                sequenceFinished = true;
                            });

                    } else {
                        return Rx.Observable.empty();
                    }
                }));
        };

        var tap = function (element, options) {
            return multiTap(element, {
                taps: 1,
                time: options && options.time,
                threshold: options && options.threshold,
                onTap: options && options.onTap,
                preventDefault: options && options.preventDefault
            });
        };

        var doubleTap = function (element, options) {
            return multiTap(element, {
                taps: 2,
                interval: options && options.interval,
                time: options && options.time,
                threshold: options && options.threshold,
                posThreshold: options && options.posThreshold,
                onTap: options && options.onTap,
                preventDefault: options && options.preventDefault
            });
        };

        var swipe = function (element, options) {
            var threshold = (options && (options.threshold !== undefined)) ? options.threshold : 10;
            var velocity = options && options.velocity || 0.65;
            var preventPointer = options && options.preventDefault;

            if (preventPointer === undefined) {
                preventPointer = true;
            }

            return fixLeak(extractPointerFromEvent(element, downEvent, preventPointer)
                .filter(function (down) {
                    return down.pointerCount === 1;
                })
                .flatMap(function (down) {
                    if (preventPointer) {
                        preventDefault(down);
                    }

                    var startTime = Date.now();

                    return extractPointerFromEvent(document, upEvent, down)
                        .take(1)
                        .takeUntil(extractPointerFromEvent(element, downEvent))
                        .filter(function (up) {
                            var dist = distance(down[startX], down[startY], up[x], up[y]);
                            return (dist / (Date.now() - startTime) >= velocity) && (dist >= threshold);
                        })
                        .map(function (up) {
                            return extendEvent(up, down[startX], down[startY], up[x], up[y]);
                        });
                }));
        };

        var press = function (element, options) {
            var threshold = (options && (options.threshold !== undefined)) ? options.threshold : 5;
            var time = options && options.time || 500;
            var preventPointer = options && options.preventDefault;

            if (preventPointer === undefined) {
                preventPointer = true;
            }

            return fixLeak(extractPointerFromEvent(element, downEvent, preventPointer)
                .filter(function (down) {
                    return down.pointerCount === 1;
                })
                .flatMap(function (down) {

                    if (preventPointer) {
                        preventDefault(down);
                    }

                    return Rx.Observable.just(down)
                        .delay(time)
                        .take(1)
                        .takeUntil(Rx.Observable.merge(
                            extractPointer(removeEvent(), down),
                            extractPointerFromEvent(element, downEvent)
                                .filter(function (another) {
                                    return another.identifier !== down.identifier;
                                }),
                            extractPointerFromEvent(document, moveEvent, down)
                                .filter(function (move) {
                                    return distance(down[startX], down[startY], move[x], move[y]) >= threshold;
                                })
                        ))
                        .map(function (down) {
                            return extendEvent(down);
                        });

                }));
        };

        var createGestureResult = function (start, end, move) {
            return {
                start: fixLeak(start),
                end: fixLeak(end),
                move: fixLeak(move)
            };
        };

        var subscribeGestureResult = function (start, end, startStream, endStream) {
            startStream.subscribe(function (x) {
                start.onNext(x);
            });

            endStream.subscribe(function (x) {
                end.onNext(x);
            });
        };

        var pan = function (element, options) {
            var threshold = (options && (options.threshold !== undefined)) ? options.threshold : 5;
            var preventPointer = options && options.preventDefault;

            if (preventPointer === undefined) {
                preventPointer = true;
            }

            var start = new Rx.Subject();
            var end = new Rx.Subject();

            return createGestureResult(start, end, extractPointerFromEvent(element, downEvent, preventPointer)
                .filter(function (down) {
                    return down.pointerCount === 1;
                })
                .flatMap(function (down) {
                    if (preventPointer) {
                        preventDefault(down);
                    }

                    var cancelStream = extractPointer(removeEvent(), down);

                    var moveStream = extractPointerFromEvent(document, moveEvent, down)
                        .filter(function (move) {
                            return distance(down[startX], down[startY], move[x], move[y]) >= threshold;
                        })
                        .takeUntil(cancelStream.merge(extractPointerFromEvent(element, downEvent)))
                        .map(function (move) {
                            return extendEvent(move, down[startX], down[startY], move[x], move[y]);
                        });

                    var startStream = moveStream
                        .take(1)
                        .map(function () {
                            return extendEvent(down);
                        });

                    var endStream = cancelStream
                        .skipUntil(startStream)
                        .take(1)
                        .map(function (up) {
                            return extendEvent(up, down[startX], down[startY], up[x], up[y]);
                        });

                    subscribeGestureResult(start, end, startStream, endStream);

                    return moveStream;
                }));
        };

        var addToBuffer = function (buffer, down) {
            var shouldAddTouch = !buffer.some(function (touch) {
                return touch.identifier === down.identifier;
            });

            if (shouldAddTouch) {
                buffer.push(down);

                extractPointer(removeEvent(), down)
                    .take(1)
                    .subscribe(function (event) {
                        buffer.splice(buffer.findIndex(function (p) {
                            return p.identifier === event.identifier;
                        }), 1);
                    });
            }
        };

        var twoPointerGesture = function (diff, extend) {
            return function (element, options) {
                var threshold = (options && (options.threshold !== undefined)) ? options.threshold : 0;
                var preventPointer = options && options.preventDefault;

                if (preventPointer === undefined) {
                    preventPointer = true;
                }

                var event = Rx.Observable.fromEvent(element, downEvent)
                    .filter(function (down) {
                        return down.targetTouches.length <= 2;
                    });

                var start = new Rx.Subject();
                var end = new Rx.Subject();

                return createGestureResult(
                    start,
                    end,
                    controlledBufferWithCount(extractPointer(event, preventPointer), 2, addToBuffer)
                    .flatMap(function (down) {
                        var downFirst = down[0];
                        var downSecond = down[1];
                        var initialValue = diff(downFirst[x], downFirst[y], downSecond[x], downSecond[y]);

                        var cancelStream = Rx.Observable.merge(
                            extractPointer(removeEvent(), downFirst),
                            extractPointer(removeEvent(), downSecond)
                        );

                        var moveStream = Rx.Observable.merge(
                            extractPointerFromEvent(document, moveEvent, downFirst),
                            extractPointerFromEvent(document, moveEvent, downSecond)
                        )
                            .filter(function () {
                                return (diff(downFirst[x], downFirst[y], downSecond[x], downSecond[y]) / initialValue)
                                    >= threshold;
                            })
                            .takeUntil(cancelStream)
                            .map(function (move) {
                                return extend(move, downFirst, downSecond, initialValue);
                            });

                        var startStream = moveStream
                            .take(1)
                            .map(function (move) {
                                return extend(move, downFirst, downSecond, initialValue);
                            });

                        var endStream = cancelStream
                            .skipUntil(startStream)
                            .take(1)
                            .map(function (up) {
                                return extend(up, downFirst, downSecond, initialValue);
                            });

                        subscribeGestureResult(start, end, startStream, endStream);

                        return moveStream;
                    }));
            };
        };

        var pinch = isTouchSupported
            ? twoPointerGesture(distance, extendPinchEvent)
            : empty;

        var rotate = isTouchSupported
            ? twoPointerGesture(angle, extendRotateEvent)
            : empty;

        return {
            xOf: xOf,
            yOf: yOf,

            tap: tap,
            doubleTap: doubleTap,
            multiTap: multiTap,
            swipe: swipe,
            press: press,
            pan: pan,
            pinch: pinch,
            rotate: rotate
        };
    })();

    if (typeof module != 'undefined' && module.exports) {
        module.exports = rxia;
    } else {
        window.rxia = rxia;
    }
}());
