/**
 * Created by iyegoroff on 25/06/15.
 */

module RxDiagram {

    export function l(text: string): string {
        return Locales.localize(text);
    }

    export class Locales {

        static setLocale(locale: string) {
            Locales.currentLocale = locale.match(/[^-]*/)[0];
        }

        static localize(text: string): string {
            var locale = Locales.currentLocale;

            return (Locales.defaultLocale === Locales.currentLocale)
                ? text
                : Locales.strings[text][locale];
        }

        private static defaultLocale: string = 'en';
        private static currentLocale: string = 'en';

        private static strings = {
            'Tutorial': {
                ru: 'Туториал'
            },

            'About': {
                ru: 'О приложении'
            },

            "This app demonstrates some capabilities of awesome ": {
                ru: 'Это приложение демонстрирует некоторые возможности замечательной библиотеки '
            },

            " library. Inspired by André Staltz's ": {
                ru: '. Основано на идее André Staltz\'а - '
            },

            "RxJS version: ": {
                ru: 'Версия RxJS: '
            },

            'Send feedback': {
                ru: 'Написать отзыв'
            },

            '1. General info.': {
                ru: '1. Общая информация.'
            },

            'Screen is divided into three parts: ': {
                ru: 'Экран разделен на три части: '
            },

            'observables input area': {
                ru: 'область ввода наблюдаемых объектов (observables)'
            },

            'Rx.Observable method selection area': {
                ru: 'область выбора методов класса Rx.Observable'
            },

            ' and ': {
                ru: ' и '
            },

            'result area': {
                ru: 'область вывода'
            },

            '. You can create up to three observables, add numeric (0..99) or error values, ': {
                ru: '. Вы можете создать от одного до трех наблюдаемых объектов, добавлять числовые значения (0..99)' +
                    ' и значения-ошибки, '
            },

            'select methods and adjust some of their parameters.': {
                ru: 'выбирать методы и изменять некоторые из их параметров.'
            },

            '2. Creating and deleting observables.': {
                ru: '2. Создание и удаление наблюдаемых объектов.'
            },

            'To create an observable just tap in input area. ': {
                ru: 'Чтобы создать наблюдаемый объект просто нажмите в области ввода. '
            },

            'Observables are marked as \'A\', \'B\' and \'C\'. ': {
                ru: 'Наблюдаемые объекты отмечены буквами \'A\', \'B\' и \'C\'. '
            },

            'Drag the right end of observable to change its length. ': {
                ru: 'Потяните за правый край наблюдаемого объекта, чтобы изменить его длину. '
            },

            'To delete created observable - swipe near it horizontally.': {
                ru: 'Чтобы удалить наблюдаемый объект - проведите около него пальцем горизонтально.'
            },

            '3. Creating values.': {
                ru: '3. Создание значений.'
            },

            'To create a value you need to tap on existing observable. ': {
                ru: 'Для создания значения нужно нажать на наблюдаемый объект. '
            },

            'Note that result area is also updated when you add values to \'A\' observable. ': {
                ru: 'Обратите внимание, что область вывода тоже обновляется, когда Вы добавляете значения к ' +
                    'наблюдаемому объекту \'A\'. '
            },

            'Also you can drag values to change their positions.': {
                ru: 'Также Вы можете тянуть значения пальцем, чтобы поменять их позиции.'
            },

            '4. Editing and deleting values.': {
                ru: '4. Изменение и удаление значений.'
            },

            'To edit or delete some value - press on it and hold until small menu appears. ': {
                ru: 'Чтобы изменить или удалить значение - нажмите на нем и удерживайте, пока не появится ' +
                    'небольшое меню. '
            },

            'Now it is possible ': {
                ru: 'Теперь можно '
            },

            'to edit selected value by scrolling vertically the left panel': {
                ru: 'изменить выбранное значение с помощью прокрутки левой панели'
            },

            'delete the value by pressing the right \'X\'-button ': {
                ru: 'удалить значение, нажав на правую кнопку \'X\' '
            },

            'or ': {
                ru: 'или '
            },

            'close this menu by tapping outside of it.': {
                ru: 'закрыть это меню нажав за его пределами.'
            },

            '7. Value types.': {
                ru: '7. Типы значений.'
            },

            '5. Choosing methods.': {
                ru: '5. Выбор методов.'
            },

            'You can choose one of the Rx.Observable class or instance methods ': {
                ru: 'Вы можете выбрать один из методов класса Rx.Observable или объекта типа Rx.Observable '
            },

            'with help of the drop down menu. ': {
                ru: 'с помощью выпадающего меню. '
            },

            'Also it is possible to customize colored arguments by vertical scrolling. ': {
                ru: 'Также возможно изменить цветные аргументы с помощью вертикальной прокрутки. '
            },

            'In case when the whole method invocation string is not visible ': {
                ru: 'В случае, когда строка метода не видна полностью, '
            },

            'you can scroll it horizontally.': {
                ru: 'Вы можете прокрутить ее горизонтально.'
            },

            '6. Interacting with result.': {
                ru: '6. Взаимодействие с результатом.'
            },

            'If you tap on a partially covered value it will be moved to the front. ': {
                ru: 'Если Вы нажмете на частично закрытое значение, то оно будет показано поверх остальных значений. '
            },

            'When vertical \'capsule\' is not fully visible - just drag it up.': {
                ru: 'Если вертикальная \'капсула\' видна не полностью - просто потяните ее вверх.'
            },

            'Boolean (T or F)': {
                ru: 'Boolean (T или F)'
            },

            'Values with equal positions': {
                ru: 'Значения с одинаковыми позициями'
            },

            'Do you want to see the tutorial?': {
                ru: 'Хотите ли Вы посмотреть туториал?'
            },

            'Yes': {
                ru: 'Да'
            },

            'No': {
                ru: 'Нет'
            },

            'Confirm': {
                ru: 'Вопрос'
            }
        }
    }

}
