/**
 * Created by iyegoroff on 15/01/15.
 */

module RxDiagram {

    export class Geometry {

        static degToRad(degrees) {
            return degrees * Math.PI / 180;
        }

        static radToDeg(radiens) {
            return radiens * 180 / Math.PI;
        }

        static distance(x1, y1, x2, y2): number {
            return Math.sqrt(Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2));
        }

        static angle(x1, y1, x2, y2): number {
            return (360 - Math.atan2(y2 - y1, x2 - x1) * 180 / Math.PI) % 360;
        }

        static isInsideRange(value, startRange, endRange): boolean {
            return value >= startRange && value < endRange;
        }

        static isInsideRect(x, y, rectX, rectY, rectWidth, rectHeight): boolean {
            return Geometry.isInsideRange(x, rectX, rectX + rectWidth)
                   && Geometry.isInsideRange(y, rectY, rectY + rectHeight);
        }

        static pointOnCircle(cx, cy, radius, angle): { x: number; y: number } {
            return { x: cx + radius * Math.cos(angle), y: cy + radius * Math.sin(angle) };
        }

        static arc(x, y, radius, startAngle, endAngle): string {
            var start = Geometry.pointOnCircle(x, y, radius, endAngle);
            var end = Geometry.pointOnCircle(x, y, radius, startAngle);
            var arcSweep = endAngle - startAngle <= Math.PI ? "0" : "1";

            return [
                "M", start.x, start.y,
                "A", radius, radius, 0, arcSweep, 0, end.x, end.y
            ].join(" ");
        }

        static capsule(radius, offset): string {
            var PI_2 = Math.PI / 2;

            return Geometry.arc(offset, 0, radius, -PI_2, PI_2)
                + 'L-' + offset + ',-' + radius
                + Geometry.arc(-offset, 0, radius, PI_2, -PI_2)
                + 'L' + offset + ',' + radius;
        }
    }
}
