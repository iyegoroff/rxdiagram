/**
 * Created by iyegoroff on 12/12/14.
 */

///<reference path='pages/MainPage/MainPage.ts' />
///<reference path='pages/TutorialPage/TutorialPage.ts' />
///<reference path='pages/AboutPage/AboutPage.ts' />
///<reference path='Constants.ts' />
///<reference path='Locales.ts' />
///<reference path='../lib/StatusBar.d.ts' />
///<reference path='../lib/Globalization.d.ts' />
///<reference path='../lib/Dialogs.d.ts' />

module RxDiagram {

    export class App extends Framework.App {

        constructor() {
            super(MainPage.create(0, 0, screenWidth, screenHeight), () => {
                window.StatusBar.hide();

                navigator.globalization.getPreferredLanguage(
                    (language) => Locales.setLocale(language.value),
                    () => {}
                );
            });

            this.askForTurorial();
        }

        transitToAbout = (onFinish?: () => void) => {
            this.transitToPage(AboutPage.create(), Framework.Transition.slideFromLeft(App.delay), onFinish);
        };

        transitToTutorial = (onFinish?: () => void) => {
            if (App.isTutorialNeeded()) {
                App.tutorialWasShown();
            }

            this.transitToPage(TutorialPage.create(), Framework.Transition.slideFromLeft(App.delay), onFinish);
        };

        transitBackToMain = (onFinish?: () => void) => {
            this.popBack(Framework.Transition.slideFromRight(App.delay), onFinish);
        };

        private askForTurorial() {
            if (App.isTutorialNeeded()) {

                _.delay(() => {
                    if (App.isTutorialNeeded()) {

                        navigator.notification.confirm(
                            l('Do you want to see the tutorial?'),
                            choice => {
                                if (choice === 1) {
                                    m.startComputation();
                                    this.transitToTutorial();
                                    m.endComputation();
                                }

                                App.tutorialWasShown();
                            },
                            l('Confirm'),
                            [l('Yes'), l('No')]
                        );
                    }
                }, App.tutorialAlertDelay);

            }
        }

        private static isTutorialNeeded(): boolean {
            return !localStorage.getItem(App.tutorialKey);
        }

        private static tutorialWasShown(): void {
            localStorage.setItem(App.tutorialKey, 'true');
        }

        private static tutorialAlertDelay = 5000;
        private static delay = 150;
        private static tutorialKey = 'tutorial_was_shown';
    }

    export var app = new App();
}
