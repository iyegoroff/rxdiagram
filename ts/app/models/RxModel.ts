/**
 * Created by iyegoroff on 23/12/14.
 */

///<reference path='../../lib/rx.all.d.ts' />
///<reference path='../../lib/lodash.d.ts' />
///<reference path='../Constants.ts' />

module RxDiagram {

    interface MethodOptions {
        method: string;
        params: any[];
        colorMethod?: string;
        colorParams?: any[];
    }

    export interface ColoredValue {
        value: any;
        color: number;
    }

    export interface ObservableInput extends Array<Rx.Timestamp<ColoredValue>> {
        endTime: number;
    }

    export class RxModel {

        static errorValue() {
            return 'E';
        }

        static operations() {
            return Object.keys(RxModel.reactives).sort((x, y) => x > y ? 1 : -1);
        }

        static format(operation: string, obsNames: string[], obsAmount: number): any[] {
            return RxModel.reactives[operation]
                .format(obsNames, obsAmount)
                .filter(v => v.length);
        }

        static computeWithColors(
            operation: string,
            inputs: any[],
            onComplete: (result: Rx.Timestamp<ColoredValue>[], isFinishedOnTime: boolean, endTime: number) => void
        ) {
            RxModel.compute(
                RxModel.reactives[operation].call,
                inputs,
                (result: Rx.Timestamp<ColoredValue>[], isFinishedOnTime: boolean, endTime: number) => {

                    if (result.length && result[0].value && !_.isUndefined((<any>result[0]).value.interval)) {
                        result = result.map(x => ({ value: (<any>x.value).interval, timestamp: x.timestamp }));
                    }

                    var shouldComputeColorSeparately = RxModel.reactives[operation].colorCall;
                    var valueIsArray = result.length && _.isArray(result[0].value);

                    if (valueIsArray) {

                        onComplete(
                            result.map(arr => ({
                                timestamp: arr.timestamp,
                                value: {
                                    value: _.isArray(arr.value)
                                        ? (<any>arr.value).map(v => v.value)
                                        : arr.value,
                                    color: (_.isArray(arr.value)
                                        ? (<any>arr.value).reduce((acc, v) => acc | v.color, 0)
                                        : RxModel.defaultColor)
                                    || RxModel.defaultColor
                                }
                            })),
                            isFinishedOnTime,
                            endTime
                        );

                    } else if (shouldComputeColorSeparately) {

                        RxModel.compute(
                            RxModel.reactives[operation].colorCall,
                            inputs.map(arr => {
                                if (!arr.length) {
                                    return [];
                                }

                                var result = arr.filter(x => x.value ? x.value.value !== RxModel.errorValue() : true);
                                result.endTime = arr.endTime;

                                return result;
                            }),
                            (colorResult: Rx.Timestamp<ColoredValue>[]) => {

                                var combined = _.zipWith(result, colorResult.slice(0, result.length), (num, col) => {
                                    var color = _.isUndefined(col && col.value) ? RxModel.defaultColor : col.value;
                                    return {
                                        value: {
                                            value: num.value,
                                            color: _.isUndefined(color.color) ?  color : color.color
                                        },
                                        timestamp: num.timestamp
                                    };
                                });

                                onComplete(combined, isFinishedOnTime, endTime);
                            }
                        );

                    } else {

                        onComplete(
                            result.map(o => _.isObject(o.value)
                                ? o
                                : <any>_.extend(o, { value: { value: o.value, color: RxModel.defaultColor }})
                            ),
                            isFinishedOnTime,
                            endTime
                        );

                    }
                }
            );
        }

        private static compute(
            operation: Function,
            inputs: any[],
            onComplete: (result: Rx.Timestamp<ColoredValue>[], isFinishedOnTime: boolean, endTime: number) => void
        ) {
            var subscription = RxModel.subscription;
            if (subscription) {
                subscription.dispose();
            }

            var scheduler = new Rx.HistoricalScheduler();
            RxModel.virtualScheduler = scheduler;

            var args = inputs.map(input => (_.isString(input) || _.isNumber(input) || !_.isUndefined(input.color))
                ? input
                : RxModel.createObservable(input, scheduler)
            );

            var result = [];
            var fixedEndTime = timeScale + RxModel.timeScaleFix;

            var source = operation(args, scheduler).timestamp(scheduler);
            var finiteSource = source.takeUntilWithTime(fixedEndTime, scheduler);

            RxModel.subscription = finiteSource
                .finally(() => {
                    var now = scheduler.now();
                    var endTime = now > timeScale ? fixedEndTime : now;

                    onComplete(result.filter(x => x.timestamp <= timeScale), endTime <= timeScale, endTime);
                })
                .subscribe(
                    (x) => result.push(x),
                    () => result.push({ value: RxModel.errorValue(), timestamp: scheduler.now() })
                );

            scheduler.start();
        }

        static mapFormatIndex(operation, idx) {
            return RxModel.reactives[operation].mapFormatIndex(idx);
        }

        static formatArguments(format: any[]) {
            return format.filter(x => typeof x !== 'string');
        }

        private static createObservable(
            sequence: ObservableInput,
            scheduler: Rx.HistoricalScheduler
        ): Rx.Observable<ColoredValue> {
            var endTime = sequence.endTime;

            var observable = Rx.Observable.merge<ColoredValue>(sequence.map(x => {
                var value = x.value, timestamp = x.timestamp;
                var delay = timestamp === endTime ? endTime - 1 : timestamp;

                return value.value === RxModel.errorValue()
                    ? Rx.Observable
                        .empty<ColoredValue>()
                        .delay(delay, scheduler)
                        .doOnCompleted(() => { throw new Error(); })
                    : Rx.Observable
                        .just(value)
                        .delay(delay, scheduler);
            }));

            return observable
                .concat(Rx.Observable.never<ColoredValue>())
                .takeUntilWithTime(endTime, scheduler);
        }

        private static staticMapFormatIndex() {
            return (idx) => (idx - 1) / 2;
        }

        private static prototypeMapFormatIndex() {
            return (idx) => idx / 2;
        }

        private static methodInfo(options: MethodOptions) {
            var methodInfo = {
                call: RxModel.methodCall(options.method, options.params),
                format: RxModel.methodFormat(options.method, options.params),
                mapFormatIndex: RxModel.methodMapFormatIndex(options.params[0])
            };

            return options.colorMethod
                ? _.extend(methodInfo, { colorCall: RxModel.methodCall(options.colorMethod, options.colorParams) })
                : methodInfo;
        }

        private static methodCall(method: string, params: any[]) {
            return (inputs: any[], scheduler: Rx.HistoricalScheduler) => {

                var inpIndex = 0;
                var args = params.reduce((result, param) => {

                    if (param === RxModel.observablePlaceholder || _.isArray(param)) {

                        result.push(inputs[inpIndex++]);

                    } else if (param === RxModel.hotObservablePlaceholder) {

                        var hot = inputs[inpIndex++].publish();

                        result.push(hot);
                        hot.connect();

                    } else if (param === RxModel.colorlessObservablePlaceholder) {

                        result.push(inputs[inpIndex++].map(x => x.value));

                    } else if (param === RxModel.schedulerPlaceholder) {

                        result.push(scheduler);

                    } else if (param === RxModel.restObservablesPlaceholder) {

                        for (var len = inputs.length; inpIndex < len; inpIndex += 1) {
                            result.push(inputs[inpIndex]);
                        }

                        if (!len) {
                            result.push(Rx.Observable.empty());
                        }

                    } else {

                        result.push(RxModel.isInvisible(param) ? param.invisible : param);

                    }

                    return result;
                }, []);

                return args[0][method].apply(args[0], args.slice(1));
            };
        }

        private static methodFormat(method: string, params: any[]) {
            return (argNames, argAmount) => {
                var restAmount = argAmount;

                var format = params.reduce((result, param) => {

                    var isObservableInstance = param === RxModel.observablePlaceholder
                        || param == RxModel.hotObservablePlaceholder
                        || param == RxModel.colorlessObservablePlaceholder;

                    if (param === Rx.Observable) {

                        result.push('Rx.Observable.' + method + '(');

                    } else if (_.isFunction(param)) {

                        result[result.length - 1] += ((param + '')
                            .replace(/\{\s*?value:\s*([^,]*)[^\}]*\}/, '$1')
                            .replace(/\.value/g, '') + ',')
                            .replace(/(?:[,\s]*RxModel\.scheduler\(\))|(?:\"use strict\";\s)/g, '');

                    } else if (isObservableInstance) {

                        result.push(argNames);

                        if (result.length > 1) {
                            result.push(',');
                        } else {
                            result.push('.' + method + '(');
                        }

                        restAmount -= 1;

                    } else if (param === RxModel.restObservablesPlaceholder) {

                        for (var i = 0; i < restAmount; i += 1) {
                            result.push(argNames);
                            result.push(',');
                        }

                    } else if (param !== RxModel.schedulerPlaceholder && !RxModel.isInvisible(param)) {

                        result.push(_.isString(param) ? '"' + param + '"' : param);
                        result.push(',');

                    }

                    return result;
                }, []);

                var lastIndex = format.length - 1;
                format[lastIndex] = format[lastIndex].replace(/,$/, ')').replace(/\($/, '()');

                if (method === 'timeInterval') {
                    format[lastIndex] = format[lastIndex] + '.interval';
                }

                return format;
            }
        }

        private static methodMapFormatIndex(thisParam: any) {
            return thisParam === Rx.Observable
                ? RxModel.staticMapFormatIndex()
                : RxModel.prototypeMapFormatIndex();
        }

        private static scheduler() {
            return RxModel.virtualScheduler;
        }

        private static createReactives(methods) {
            return Object.keys(methods).reduce((result, x) => {
                var curMethodInfo = methods[x];

                curMethodInfo.method = x.replace(/ .*/, '');

                var colorMethod = curMethodInfo.colorMethod;
                var generateColors = colorMethod && colorMethod.generateColors;
                var returnColor = colorMethod && colorMethod.returnColor;

                if (generateColors !== undefined) {

                    curMethodInfo.colorMethod = 'take';
                    curMethodInfo.colorParams = [
                        Rx.Observable.generate(
                            0xA0,
                            x => true,
                            x => (x === 0xA00000) ? 0xA0 : (x * 0x100),
                            x => x,
                            RxModel.scheduler()
                        ),
                        generateColors
                    ];

                } else if (returnColor !== undefined) {

                    curMethodInfo.colorMethod = 'return';
                    curMethodInfo.colorParams = [
                        Rx.Observable,
                        returnColor,
                        RxModel.schedulerPlaceholder
                    ];

                } else if (curMethodInfo.colorMethod === RxModel.sumOfColors) {

                    curMethodInfo.colorMethod = 'reduce';
                    curMethodInfo.colorParams = [
                        RxModel.observablePlaceholder,
                        ((acc, x) => acc | x.color),
                        [0]
                    ];

                } else if (curMethodInfo.colorMethod === RxModel.mapColors) {

                    curMethodInfo.colorMethod = 'map';
                    curMethodInfo.colorParams = [
                        RxModel.observablePlaceholder,
                        (x => x.color)
                    ];

                }

                result[x] = RxModel.methodInfo(curMethodInfo);

                return result;
            }, {});
        }

        private static isInvisible(param) {
            return !_.isUndefined(param.invisible);
        }

        private static subscription: Rx.IDisposable = null;
        private static virtualScheduler = null;
        private static timeScaleFix = 50;
        private static defaultColor = 0xB0B0B0;

        private static observablePlaceholder = {};
        private static colorlessObservablePlaceholder = {};
        private static restObservablesPlaceholder = {};
        private static schedulerPlaceholder = {};
        private static hotObservablePlaceholder = {};
        private static invisible = (x) => ({ invisible: x });
        private static generateColors = (n) => ({ generateColors: n });
        private static returnColor = (c = RxModel.defaultColor) => ({ returnColor: c });
        private static sumOfColors = {};
        private static mapColors = {};

        private static methods = {

            'amb *': {
                params: [
                    Rx.Observable,
                    RxModel.restObservablesPlaceholder
                ]
            },

            'concat *': {
                params: [
                    Rx.Observable,
                    RxModel.restObservablesPlaceholder
                ]
            },

            'merge': {
                params: [
                    Rx.Observable,
                    RxModel.restObservablesPlaceholder
                ]
            },

            'count': {
                params: [
                    RxModel.observablePlaceholder
                ],
                colorMethod: RxModel.sumOfColors
            },

            'average': {
                params: [
                    RxModel.observablePlaceholder,
                    RxModel.invisible(x => x.value)
                ],
                colorMethod: RxModel.sumOfColors
            },

            'delay': {
                params: [
                    RxModel.observablePlaceholder,
                    [0, 100, 250, 500],
                    RxModel.schedulerPlaceholder
                ]
            },

            'debounce': {
                params: [
                    RxModel.observablePlaceholder,
                    [0, 100, 250, 500],
                    RxModel.schedulerPlaceholder
                ]
            },

            'distinct': {
                params: [
                    RxModel.observablePlaceholder,
                    RxModel.invisible(x => x.value)
                ]
            },

            'distinctUntilChanged': {
                params: [
                    RxModel.observablePlaceholder,
                    RxModel.invisible(x => x.value)
                ]
            },

            'filter': {
                params: [
                    RxModel.observablePlaceholder,
                    (x => x.value % 2)
                ]
            },

            'map': {
                params: [
                    RxModel.observablePlaceholder,
                    (x => x.value / 2)
                ],
                colorMethod: RxModel.mapColors
            },

            'max': {
                params: [
                    RxModel.observablePlaceholder,
                    RxModel.invisible((x, y) => {
                        var xv = x.value, yv = y.value;
                        return (xv > yv) ? 1 : ((xv < yv) ? -1 : 0);
                    })
                ]
            },

            'min': {
                params: [
                    RxModel.observablePlaceholder,
                    RxModel.invisible((x, y) => {
                        var xv = x.value, yv = y.value;
                        return (xv > yv) ? 1 : ((xv < yv) ? -1 : 0);
                    })
                ]
            },

            'skip': {
                params: [
                    RxModel.observablePlaceholder,
                    [0, 1, 2, 3]
                ]
            },

            'skipLast': {
                params: [
                    RxModel.observablePlaceholder,
                    [0, 1, 2, 3]
                ]
            },

            'take': {
                params: [
                    RxModel.observablePlaceholder,
                    [0, 1, 2, 3]
                ]
            },

            'takeLast': {
                params: [
                    RxModel.observablePlaceholder,
                    [0, 1, 2, 3]
                ]
            },

            'skipUntilWithTime': {
                params: [
                    RxModel.observablePlaceholder,
                    [0, 100, 250, 500],
                    RxModel.schedulerPlaceholder
                ]
            },

            'takeUntilWithTime': {
                params: [
                    RxModel.observablePlaceholder,
                    [0, 100, 250, 500],
                    RxModel.schedulerPlaceholder
                ]
            },

            'takeWhile': {
                params: [
                    RxModel.observablePlaceholder,
                    (x => x.value < 50)
                ]
            },

            'delay *': {
                params: [
                    RxModel.observablePlaceholder,
                    (x => Rx.Observable.timer(x.value * 10, RxModel.scheduler()))
                ]
            },

            'throttleFirst': {
                params: [
                    RxModel.observablePlaceholder,
                    [1, 100, 250, 500],
                    RxModel.schedulerPlaceholder
                ]
            },

            'findIndex': {
                params: [
                    RxModel.observablePlaceholder,
                    (x => x.value > 50)
                ],
                colorMethod: 'find',
                colorParams: [
                    RxModel.observablePlaceholder,
                    (x => x.value > 50)
                ]
            },

            'first': {
                params: [
                    RxModel.observablePlaceholder
                ]
            },

            'scan': {
                params: [
                    RxModel.observablePlaceholder,
                    RxModel.invisible(0),
                    ((acc, x) => acc + x.value)
                ],
                colorMethod: 'scan',
                colorParams: [
                    RxModel.observablePlaceholder,
                    [0],
                    ((acc, x) => acc | x.color)
                ]
            },

            'debounce *': {
                params: [
                    RxModel.observablePlaceholder,
                    (x => Rx.Observable.timer(x.value * 10, RxModel.scheduler()))
                ]
            },

            'last': {
                params: [
                    RxModel.observablePlaceholder
                ]
            },

            'repeat': {
                params: [
                    RxModel.observablePlaceholder,
                    [0, 1, 2, 3]
                ]
            },

            'combineLatest': {
                params: [
                    Rx.Observable,
                    RxModel.restObservablesPlaceholder,
                    ((...args) => Array.prototype.reduce.call(args, (acc,x) => acc + x.value, 0))
                ],
                colorMethod: 'combineLatest',
                colorParams: [
                    Rx.Observable,
                    RxModel.restObservablesPlaceholder,
                    ((...args) => Array.prototype.reduce.call(args, (acc,x) => acc | x.color, 0))
                ]
            },

            'create': {
                params: [
                    Rx.Observable,
                    ((o) => { o.onNext(24); o.onCompleted(); })
                ],
                colorMethod: RxModel.returnColor()
            },

            'empty': {
                params: [
                    Rx.Observable,
                    RxModel.schedulerPlaceholder
                ]
            },

            'return': {
                params: [
                    Rx.Observable,
                    [0, 10, 25],
                    RxModel.schedulerPlaceholder
                ],
                colorMethod: RxModel.returnColor()
            },

            'interval': {
                params: [
                    Rx.Observable,
                    [50, 100, 250],
                    RxModel.schedulerPlaceholder
                ],
                colorMethod: RxModel.generateColors(20)
            },

            'never': {
                params: [
                    Rx.Observable
                ]
            },

            'repeat *': {
                params: [
                    Rx.Observable,
                    [0, 10, 25],
                    [0, 1, 2, 3],
                    RxModel.schedulerPlaceholder
                ],
                colorMethod: RxModel.generateColors(3)
            },

            'timer': {
                params: [
                    Rx.Observable,
                    [0, 100, 250, 500],
                    [50, 100, 250],
                    RxModel.schedulerPlaceholder
                ],
                colorMethod: RxModel.generateColors(21)
            },

            'amb': {
                params: [
                    RxModel.observablePlaceholder,
                    RxModel.observablePlaceholder
                ]
            },

            'includes': {
                params: [
                    RxModel.colorlessObservablePlaceholder,
                    [0, 10, 20, 30, 40, 50, 60, 70, 80, 90]
                ]
            },

            'concat': {
                params: [
                    RxModel.observablePlaceholder,
                    RxModel.restObservablesPlaceholder
                ]
            },

            'every': {
                params: [
                    RxModel.observablePlaceholder,
                    (x => x.value % 2 === 0)
                ],
                colorMethod: RxModel.sumOfColors
            },

            'flatMap': {
                params: [
                    RxModel.observablePlaceholder,
                    (x => Rx.Observable.range(x.value, 2))
                ],
                colorMethod: 'flatMap',
                colorParams: [
                    RxModel.observablePlaceholder,
                    (x => Rx.Observable.from([x.color, x.color]))
                ]
            },

            'sum': {
                params: [
                    RxModel.observablePlaceholder,
                    x => x.value
                ],
                colorMethod: RxModel.sumOfColors
            },

            'takeUntil': {
                params: [
                    RxModel.observablePlaceholder,
                    RxModel.observablePlaceholder
                ]
            },

            'takeLastWithTime': {
                params: [
                    RxModel.observablePlaceholder,
                    [0, 100, 250, 500, 900],
                    RxModel.schedulerPlaceholder
                ]
            },

            'timeInterval': {
                params: [
                    RxModel.observablePlaceholder,
                    RxModel.schedulerPlaceholder
                ],
                colorMethod: RxModel.mapColors
            },

            'defaultIfEmpty': {
                params: [
                    RxModel.observablePlaceholder,
                    [10, 25, 50]
                ]
            },

            'throw': {
                params: [
                    Rx.Observable
                ]
            },

            'mergeDelayError': {
                params: [
                    Rx.Observable,
                    RxModel.restObservablesPlaceholder
                ]
            },

            'catch *': {
                params: [
                    Rx.Observable,
                    RxModel.restObservablesPlaceholder
                ]
            },

            'from': {
                params: [
                    Rx.Observable,
                    "hello"
                ]
            },

            'just': {
                params: [
                    Rx.Observable,
                    [0, 10, 25],
                    RxModel.schedulerPlaceholder
                ],
                colorMethod: RxModel.returnColor()
            },

            'of': {
                params: [
                    Rx.Observable,
                    [1, 2, 3],
                    [4, 5, 6],
                    [7, 8, 9]
                ]
            },

            'onErrorResumeNext': {
                params: [
                    Rx.Observable,
                    RxModel.restObservablesPlaceholder
                ]
            },

            'range': {
                params: [
                    Rx.Observable,
                    [0, 10, 25],
                    [0, 1, 2, 3],
                    RxModel.schedulerPlaceholder
                ],
                colorMethod: RxModel.generateColors(3)
            },

            'ignoreElements': {
                params: [
                    RxModel.observablePlaceholder
                ]
            },

            'sample': {
                params: [
                    RxModel.observablePlaceholder,
                    RxModel.observablePlaceholder,
                    RxModel.schedulerPlaceholder
                ]
            },

            'where': {
                params: [
                    RxModel.observablePlaceholder,
                    (x => x.value % 3)
                ]
            },

            'catch': {
                params: [
                    RxModel.observablePlaceholder,
                    RxModel.observablePlaceholder
                ]
            },

            'elementAt': {
                params: [
                    RxModel.observablePlaceholder,
                    [0, 1, 2, 3]
                ]
            },

            'elementAt *': {
                params: [
                    RxModel.observablePlaceholder,
                    [0, 1, 2, 3],
                    [0, 99]
                ]
            },

            'expand': {
                params: [
                    RxModel.observablePlaceholder,
                    (x => Rx.Observable.just({ value: x.value + 5, color: x.color }).delay(200, RxModel.scheduler())),
                    RxModel.schedulerPlaceholder
                ]
            },

            'find': {
                params: [
                    RxModel.observablePlaceholder,
                    ((x, i) => x.value < 25 && i % 2)
                ]
            },

            'firstOrDefault': {
                params: [
                    RxModel.observablePlaceholder,
                    (x => x.value % 2),
                    [1, 51 , 99]
                ]
            },

            'selectMany': {
                params: [
                    RxModel.observablePlaceholder,
                    (x => Rx.Observable.interval((x.value + 1) * 2, RxModel.scheduler()))
                ],
                colorMethod: 'selectMany',
                colorParams: [
                    RxModel.observablePlaceholder,
                    (x => Rx.Observable.interval((x.value + 1) * 2, RxModel.scheduler()).map(() => x.color))
                ]
            },

            'forkJoin': {
                params: [
                    RxModel.observablePlaceholder,
                    RxModel.observablePlaceholder,
                    ((x,y) => x.value + y.value)
                ],
                colorMethod: 'forkJoin',
                colorParams: [
                    RxModel.observablePlaceholder,
                    RxModel.observablePlaceholder,
                    ((x,y) => x.color | y.color)
                ]
            },

            'lastOrDefault': {
                params: [
                    RxModel.observablePlaceholder,
                    (x => x.value % 2 === 0),
                    [0, 50 , 100]
                ]
            },

            'isEmpty': {
                params: [
                    RxModel.observablePlaceholder
                ],
                colorMethod: RxModel.returnColor()
            },

            'indexOf': {
                params: [
                    RxModel.colorlessObservablePlaceholder,
                    [0, 10, 20, 30, 40, 50, 60, 70, 80, 90]
                ]
            },

            'select': {
                params: [
                    RxModel.observablePlaceholder,
                    ((x, i) => x.value * i)
                ],
                colorMethod: RxModel.mapColors
            },

            'single': {
                params: [
                    RxModel.observablePlaceholder,
                    (x => x.value % 2)
                ]
            },

            'singleOrDefault': {
                params: [
                    RxModel.observablePlaceholder,
                    (x => x.value % 2 === 0),
                    [0, 10, 20]
                ]
            },

            'forkJoin *': {
                params: [
                    Rx.Observable,
                    RxModel.restObservablesPlaceholder
                ]
            },

            'zipArray': {
                params: [
                    Rx.Observable,
                    RxModel.restObservablesPlaceholder
                ]
            },

            'zip *': {
                params: [
                    Rx.Observable,
                    RxModel.restObservablesPlaceholder,
                    ((...args) => Array.prototype.reduce.call(args, (acc,x) => acc + x.value, 0))
                ],
                colorMethod: 'zip',
                colorParams: [
                    Rx.Observable,
                    RxModel.restObservablesPlaceholder,
                    ((...args) => Array.prototype.reduce.call(args, (acc,x) => acc | x.color, 0))
                ]
            },

            'zip': {
                params: [
                    RxModel.observablePlaceholder,
                    RxModel.restObservablesPlaceholder,
                    ((...args) => Array.prototype.reduce.call(args, (acc,x) => acc + x.value, 0))
                ],
                colorMethod: 'zip',
                colorParams: [
                    RxModel.observablePlaceholder,
                    RxModel.restObservablesPlaceholder,
                    ((...args) => Array.prototype.reduce.call(args, (acc,x) => acc | x.color, 0))
                ]
            },

            'buffer': {
                params: [
                    RxModel.observablePlaceholder,
                    (() => Rx.Observable.timer(125, RxModel.scheduler()))
                ]
            },

            'bufferWithCount': {
                params: [
                    RxModel.observablePlaceholder,
                    [1, 2, 3]
                ]
            },

            'bufferWithTime': {
                params: [
                    RxModel.observablePlaceholder,
                    [100, 250, 500],
                    RxModel.schedulerPlaceholder
                ]
            },

            'bufferWithTimeOrCount': {
                params: [
                    RxModel.observablePlaceholder,
                    [100, 250, 500],
                    [1, 2, 3],
                    RxModel.schedulerPlaceholder
                ]
            },

            'maxBy': {
                params: [
                    RxModel.observablePlaceholder,
                    (x => x.value)
                ]
            },

            'minBy': {
                params: [
                    RxModel.observablePlaceholder,
                    (x => x.value)
                ]
            },

            'pairwise': {
                params: [
                    RxModel.observablePlaceholder
                ]
            },

            'reduce': {
                params: [
                    RxModel.observablePlaceholder,
                    ((acc, x) => acc + x.value),
                    RxModel.invisible(0)
                ],
                colorMethod: RxModel.sumOfColors
            },

            'sequenceEqual': {
                params: [
                    RxModel.colorlessObservablePlaceholder,
                    RxModel.colorlessObservablePlaceholder
                ]
            },

            'skipUntil': {
                params: [
                    RxModel.observablePlaceholder,
                    RxModel.observablePlaceholder
                ]
            },

            'skipLastWithTime': {
                params: [
                    RxModel.observablePlaceholder,
                    [100, 250, 500],
                    RxModel.schedulerPlaceholder
                ]
            },

            'skipWhile': {
                params: [
                    RxModel.observablePlaceholder,
                    (x => x.value > 50)
                ]
            },

            'some': {
                params: [
                    RxModel.observablePlaceholder,
                    (x => x.value % 2 === 0)
                ]
            },

            'startWith': {
                params: [
                    RxModel.observablePlaceholder,
                    RxModel.schedulerPlaceholder,
                    [1, 10, 25]
                ]
            },

            'takeLastBuffer': {
                params: [
                    RxModel.observablePlaceholder,
                    [0, 1, 2, 3]
                ]
            },

            'takeLastBufferWithTime': {
                params: [
                    RxModel.observablePlaceholder,
                    [0, 100, 250, 500],
                    RxModel.schedulerPlaceholder
                ]
            },

            'timeout': {
                params: [
                    RxModel.observablePlaceholder,
                    [100, 250, 500],
                    RxModel.observablePlaceholder,
                    RxModel.schedulerPlaceholder
                ]
            },

            'timeoutWithSelector': {
                params: [
                    RxModel.observablePlaceholder,
                    (() => Rx.Observable.timer(100, RxModel.scheduler()))
                ]
            },

            'toArray': {
                params: [
                    RxModel.observablePlaceholder
                ]
            },

            'pausable': {
                params: [
                    RxModel.hotObservablePlaceholder,
                    RxModel.colorlessObservablePlaceholder
                ]
            },

            'pausableBuffered': {
                params: [
                    RxModel.hotObservablePlaceholder,
                    RxModel.colorlessObservablePlaceholder
                ]
            },

            'jortSort': {
                params: [
                    RxModel.colorlessObservablePlaceholder
                ]
            },

            'jortSortUntil': {
                params: [
                    RxModel.colorlessObservablePlaceholder,
                    RxModel.observablePlaceholder
                ]
            },

            'for': {
                params: [
                    Rx.Observable,
                    "RxJS rules!",
                    (x => Rx.Observable.just(x).delay(90, RxModel.scheduler()))
                ],
                colorMethod: RxModel.generateColors(11)
            }
        };

        private static reactives = RxModel.createReactives(RxModel.methods);
    }
}
