/**
 * Created by iyegoroff on 16/06/15.
 */

module RxDiagram {

    export interface PointCallback {
        (x: number, y: number): void;
    }

    export interface Point {
        x: number;
        y: number;
    }

}
