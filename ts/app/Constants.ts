/**
 * Created by iyegoroff on 15/12/14.
 */

///<reference path='../lib/framework.d.ts' />

module RxDiagram {
    export var screenWidth = window.innerWidth;
    export var screenHeight = window.innerHeight;
    export var standardGradientId = 'gradient';
    export var timeScale = 1000;
    export var xmlns = 'http://www.w3.org/1999/xhtml';
    export var rxVersion = '4.0.7';
    export var buttonColor = 0xEBEBEB;
    export var cssTransform = Framework.isWebkit() ? 'webkitTransform' : 'MozTransform';
    export var cssTransition = Framework.isWebkit() ? 'webkitTransition' : 'MozTransition';
    export var prefixedTransform = Framework.isWebkit() ? '-webkit-transform' : '-moz-transform';
    export var prefixedTransition = Framework.isWebkit() ? '-webkit-transition' : '-moz-transition';
    export var transitionEndEvent = Framework.isWebkit() ? 'webkitTransitionEnd' : 'transitionend';
    export var uiCornerRoundingCoef = 0.01;
}
