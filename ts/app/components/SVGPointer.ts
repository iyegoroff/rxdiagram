/**
 * Created by iyegoroff on 11/06/15.
 */

///<reference path='../../lib/framework.d.ts' />
///<reference path='../../lib/lodash.d.ts' />
///<reference path='../../lib/rx.all.d.ts' />
///<reference path='../Constants.ts' />

module RxDiagram {

    export class SVGPointer {

        constructor(x: number, y: number, key?: number) {
            this.transform(x, y);
            this.key = key;
        }

        transform(x: number, y: number): SVGPointer {
            return this.moveTo(0, x, y);
        }

        moveBy(duration: number, x: number, y: number): SVGPointer {
            return this.moveTo(duration, this.x + x, this.y + y);
        }

        moveTo(duration: number, x: number, y: number): SVGPointer {
            var scale = this.scale;

            this.x = x;
            this.y = y;

            this.pipeline.push(SVGPointer.createStep(
                Framework.matrix3d(x, y, scale, scale, 0),
                `${prefixedTransform} ${duration}ms`
            ));

            return this;
        }

        tap(duration: number, scale: number = 0.5): SVGPointer {
            duration = duration / 2;

            return this
                .scaleTo(duration, scale)
                .scaleTo(duration, 1);
        }

        tapAndHold(tapDuration: number, holdDuration: number, scale: number = 0.5): SVGPointer {
            tapDuration = tapDuration / 2;

            return this
                .scaleTo(tapDuration, scale)
                .wait(holdDuration)
                .scaleTo(tapDuration, 1);
        }

        press(duration: number, scale: number = 0.5): SVGPointer {
            return this.scaleTo(duration, scale);
        }

        release(duration: number): SVGPointer {
            return this.scaleTo(duration, 1)
        }

        scaleTo(duration: number, value: number) {
            this.scale = value;

            this.pipeline.push(SVGPointer.createStep(
                Framework.matrix3d(this.x, this.y, value, value, 0),
                `${prefixedTransform} ${duration}ms`
            ));

            return this;
        }

        wait(duration: number): SVGPointer {
            var scale = this.scale + 0.001;

            this.pipeline.push(SVGPointer.createStep(
                Framework.matrix3d(this.x, this.y, scale, scale, 0),
                `${prefixedTransform} 0ms ${duration}ms`
            ));

            return this;
        }

        clear(): SVGPointer {
            this.pipeline = [];
            this.isInfinite = false;
            this.restartPipeline = true;

            return this;
        }

        infinite(): SVGPointer {
            this.isInfinite = true;

            return this;
        }

        view(elem: Mithril.VirtualElement, key: number = 0): Mithril.VirtualElement {
            return m('g', {
                style: 'pointer-events: none;',
                key: key
            }, m('g', {
                config: this.config,
                key: this.key
            }, elem));
        }

        private config = (elem: any, init) => {
            if (!init) {
                this.updatePipeline(elem, 0);
            }
        };

        private updatePipeline(elem, idx) {
            var style = elem.style;
            var pipeline = this.pipeline;
            var len = pipeline.length;

            if (this.isInfinite && idx >= len) {
                idx = 0;
            }

            if (idx < len) {
                var transition = pipeline[idx][prefixedTransition];
                var callback = () => this.pipelineCallback(elem, idx);

                style[cssTransform] = pipeline[idx][prefixedTransform];
                style[cssTransition] = transition;

                if (SVGPointer.immediateTransitionCheck.test(transition)) {

                    _.delay(callback, 0);

                } else {

                    Rx.Observable.fromEvent(elem, transitionEndEvent)
                        .take(1)
                        .subscribe(callback);

                }
            }
        }

        private pipelineCallback = (elem ,idx) => {
            this.updatePipeline(elem, this.restartPipeline ? 0 : (idx + 1));
            this.restartPipeline = false;
        };

        private static createStep(transform: string, transition: string): any {
            var step = {};

            step[prefixedTransform] = transform;
            step[prefixedTransition] = transition;

            return step;
        }

        private x: number;
        private y: number;
        private scale: number = 1;
        private pipeline: any[] = [];
        private isInfinite: boolean = false;
        private restartPipeline: boolean = false;
        private key: number;

        private static immediateTransitionCheck = /^.*? 0ms$/;
    }

}
