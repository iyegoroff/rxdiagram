/**
 * Created by iyegoroff on 16/04/15.
 */

///<reference path='SVGButton.ts' />

module RxDiagram {

    export interface SVGButtonWithTextDef extends SVGButtonDef {
        text: string;
        font?: string;
        fontSize?: number;
        fontColor?: number;
    }

    export class SVGButtonWithText {

        static view(def: SVGButtonWithTextDef): Mithril.VirtualElement {
            var width = def.width,
                height = def.height,
                fontSize = def.fontSize,
                fontColor = def.fontColor || 0;

            var text = m('foreignObject', {
                width: width,
                height: height,
                x: def.x,
                y: def.y
            }, m('div', {
                xmlns: 'http://www.w3.org/1999/xhtml',
                style: 'display: table; height: ' + height + 'px; width: 100%; overflow: hidden; ' +
                       'color: ' + SVGButton.colorNumberToString(fontColor) + ' ;'
            }, m('div', {
                style: 'display: table-cell; vertical-align: middle; text-align: center; font-size: ' + fontSize + 'px;'
            }, def.text)));

            if (def.children) {
                def.children.push(text);
            } else {
                def.children = [text];
            }

            return SVGButton.view(def);
        }
    }
}
