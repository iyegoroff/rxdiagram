/**
 * Created by iyegoroff on 02/01/15.
 */

///<reference path='../../lib/framework.d.ts' />
///<reference path='../../lib/lodash.d.ts' />
///<reference path='../../lib/rx.interactions.d.ts' />
///<reference path='../utils/Geometry.ts' />
///<reference path='../Constants.ts' />

module RxDiagram {

    export interface SVGButtonDef {
        x: number;
        y: number;
        width: number;
        height: number;
        radius?: number;
        fill?: number;
        stroke?: number;
        press: Property<boolean>;
        onClick: Function;
        children?: Mithril.VirtualElement[];
        'class'?: string;
        style?: string;
    }

    export class SVGButton {

        static view(def: SVGButtonDef): Mithril.VirtualElement {
            var x = def.x,
                y = def.y,
                width = def.width,
                height = def.height,
                radius = def.radius || 10,
                fill = def.fill || 0xF0F0F0,
                stroke = def.stroke || 2,
                isPressed = def.press(),
                onPointerStart = () => def.press(true),
                onPointerEnd = () => def.press(false),
                onClick = def.onClick,
                children = def.children,
                klass = def.class || '',
                style = def.style || '';

            var pi = Math.PI;

            if (stroke > radius) {
                stroke = radius;
            }

            var s = stroke / 2;
            var r = radius - s;

            var up1 = (fill & 0xFF0000) + 0x0A0000;
            var up2 = (fill & 0x00FF00) + 0x000A00;
            var up3 = (fill & 0x0000FF) + 0x00000A;

            var upStroke = SVGButton.colorNumberToString(
                (up1 > 0xFF0000 ? 0xFF0000 : up1)
                + (up2 > 0x00FF00 ? 0x00FF00 : up2)
                + (up3 > 0x0000FF ? 0x0000FF : up3)
            );

            var downStroke = SVGButton.colorNumberToString(
                ((fill & 0xFF0000) / 2 & 0xFF0000)
                + ((fill & 0x00FF00) / 2 & 0x00FF00)
                + ((fill & 0x0000FF) / 2 & 0x0000FF)
            );

            var f = SVGButton.colorNumberToString(fill);

            var centers = [
                x + radius, y + height - radius,
                x + width - radius, y + height - radius,
                x + width - radius, y + radius,
                x + radius, y + radius
            ];

            var elements = [
                m('rect', {
                    x: x,
                    y: y,
                    width: width,
                    height: height,
                    rx: radius,
                    ry: radius,
                    fill: f
                }),
                m('path', {
                    d: Geometry.arc(centers[0], centers[1], r, 0.5 * pi, 0.75 * pi)
                    + 'L' + centers[2] + ',' + (centers[3] + r)
                    + Geometry.arc(centers[2], centers[3], r, 0, 0.5 * pi)
                    + 'L' + (centers[4] + r) + ',' + centers[5]
                    + Geometry.arc(centers[4], centers[5], r, -0.25 * pi, 0),
                    stroke: isPressed ? upStroke : downStroke,
                    fill: 'transparent',
                    'stroke-width': stroke
                }),
                m('path', {
                    d: Geometry.arc(centers[4], centers[5], r, -0.5 * pi, -0.25 * pi)
                    + 'L' + centers[6] + ',' + (centers[7] - r)
                    + Geometry.arc(centers[6], centers[7], r, -pi, -0.5 * pi)
                    + 'L' + (centers[0] - r) + ',' + centers[1]
                    + Geometry.arc(centers[0], centers[1], r, 0.75 * pi, pi),
                    stroke: isPressed ? downStroke : upStroke,
                    fill: 'transparent',
                    'stroke-width': stroke
                })
            ];

            return m('g', {
                ontouchstart: onPointerStart,
                ontouchend: onPointerEnd,
                onmousedown: onPointerStart,
                onmouseup: onPointerEnd,
                onmouseout: isPressed ? onPointerEnd : null,
                'class': klass,
                style: style,
                config: SVGButton.config(onClick)
            }, children
                ? elements.concat(m('g', {
                      style: `${prefixedTransform}: ${Framework.matrix3d(0, isPressed ? 2 : 0, 1, 1, 0)};` +
                             'pointer-events: none;'
                  }, children))
                : elements);
        }

        static colorNumberToString(color: number): string {
            var c = color.toString(16);

            return '#' + _.repeat('0', 6 - c.length) + c;
        }

        static config(onClick) {

            return (elem, init) => {

                if (!init) {
                    rxia.tap(elem, {
                        time: 1000,
                        threshold: 50,
                        preventDefault: false
                    }).subscribe(() => {
                        m.startComputation();
                        onClick();
                        m.endComputation();
                    });
                }
            };

        }
    }
}
