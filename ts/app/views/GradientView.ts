/**
 * Created by iyegoroff on 06/05/15.
 */

///<reference path='../../lib/framework.d.ts' />

module RxDiagram {

    export class GradientView {

        static draw(gradientId: string): Mithril.VirtualElement {

            return m('defs', [
                m('linearGradient', {
                    id: gradientId,
                    x1: '0%',
                    y1: '100%',
                    x2: '0%',
                    y2: '0%',
                    spreadMethod: 'pad'
                }, [
                    m('stop', {
                        offset: '0%',
                        'stop-color': '#ffffff',
                        'stop-opacity': 1
                    }),
                    m('stop', {
                        offset: '100%',
                        'stop-color': '#dddddd',
                        'stop-opacity': 1
                    })
                ])
            ]);
        }
    }
}
