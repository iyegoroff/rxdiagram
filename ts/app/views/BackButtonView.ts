/**
 * Created by iyegoroff on 06/05/15.
 */

///<reference path='../../lib/framework.d.ts' />
///<reference path='../utils/Geometry.ts' />
///<reference path='../Constants.ts' />

module RxDiagram {

    export class BackButtonView {

        static draw(onClick: Function, key?: number, angle: number = 135): Mithril.VirtualElement {
            var width = screenWidth;

            var x  = width * 0.97;
            var y = width * 0.03;
            var radius = width * 0.0045;
            var offset = width * 0.012;

            var arrowOffset = offset * 0.6;
            var arrowAngle = Geometry.degToRad(angle);
            var arrowCenter = Geometry.pointOnCircle(offset, 0, arrowOffset, arrowAngle);

            return m('g', {
                transform: Framework.matrix(x, y, 1, 1, 0),
                key: key,
                onclick: onClick
            }, [
                m('rect', {
                    x: -y,
                    y: -y,
                    width: 2 * y,
                    height: 2 * y,
                    fill: 'transparent'
                }),
                m('path', {
                    d: Geometry.capsule(radius, arrowOffset),
                    fill: 'gray',
                    transform: Framework.matrix(arrowCenter.x, arrowCenter.y, 1, 1, Geometry.degToRad(angle - 180))
                }),
                m('path', {
                    d: Geometry.capsule(radius, offset),
                    fill: 'gray'
                }),
                m('path', {
                    d: Geometry.capsule(radius, arrowOffset),
                    fill: 'gray',
                    transform: Framework.matrix(arrowCenter.x, -arrowCenter.y, 1, 1, Geometry.degToRad(180 - angle))
                })
            ]);
        }
    }
}
