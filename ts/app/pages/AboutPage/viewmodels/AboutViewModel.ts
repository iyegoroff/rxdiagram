/**
 * Created by iyegoroff on 14/05/15.
 */

///<reference path='../../../../lib/framework.d.ts' />
///<reference path='../../../../lib/lodash.d.ts' />
///<reference path='../../../Constants.ts' />
///<reference path='../../../../lib/cordova.d.ts' />
///<reference path='../../../../lib/cordova-plugin-email-composer.d.ts' />


module RxDiagram {

    export class AboutViewModel {
        sendFeedback = () => {
            var emailComposer = cordova.plugins.email;

            emailComposer.isAvailable((isAvailable) => {
                if (isAvailable) {
                    emailComposer.open({
                        to: 'iyegoroff@yandex.ru',
                        subject: 'RxDiagram'
                    });
                } else {
                    alert('Email is not available.');
                }
            });
        };

        gradientId: Property<string> = m.prop(_.uniqueId(standardGradientId));
        feedbackPressed: Property<boolean> = m.prop(false);
    }
}
