/**
 * Created by iyegoroff on 06/05/15.
 */

///<reference path='views/AboutView.ts' />
///<reference path='../../../lib/lodash.d.ts' />

module RxDiagram {

    export class AboutPage {

        static create(): Framework.Page {
            var vm = new AboutViewModel();

            return () => AboutView.draw(vm);
        }
    }
}
