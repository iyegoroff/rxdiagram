/**
 * Created by iyegoroff on 06/05/15.
 */

///<reference path='../../../views/BackButtonView.ts' />
///<reference path='../../../App.ts' />
///<reference path='../../../Locales.ts' />
///<reference path='../../../views/GradientView.ts' />
///<reference path='../../../components/SVGButtonWithText.ts' />
///<reference path='../viewmodels/AboutViewModel.ts' />

module RxDiagram {

    export class AboutView {

        static draw(vm: AboutViewModel): Mithril.VirtualElement {
            var width = screenWidth, height = screenHeight;

            var x = width * 0.1;
            var y = width * 0.1;
            var aboutWidth = width * 0.8;
            var aboutHeight = height - 2 * x;
            var fontSize = height * 0.04;
            var buttonFontSize = width * 0.02;
            var offset = x * 0.5;

            var buttonWidth = width * 0.21;
            var buttonHeight = width * 0.07;
            var buttonX = x + (aboutWidth - buttonWidth) * 0.5;
            var buttonY = y + aboutHeight - buttonHeight - offset;

            return m('svg', {
                width: width,
                height: height,
                'class': 'noselect'
            }, [
                GradientView.draw(vm.gradientId()),
                BackButtonView.draw(app.transitBackToMain),
                m('rect', {
                    x: x,
                    y: y,
                    width: aboutWidth,
                    height: aboutHeight,
                    rx: 10,
                    ry: 10,
                    stroke: '#cccccc',
                    style: 'fill: url(#' + vm.gradientId() + '); stroke-width: 2;'
                }),
                m('foreignObject', {
                    x: x + offset,
                    y: y + offset,
                    width: aboutWidth - 2 * offset,
                    height: aboutHeight - 2 * offset
                }, m('div', {
                    xmlns: xmlns,
                    style: 'text-align: justify; font-size: ' + fontSize + 'px;',
                    'class': 'noselect'
                }, m.trust(l("This app demonstrates some capabilities of awesome ") +
                    "<a href='#' onclick=\"window.open(\'https://github.com/Reactive-Extensions/RxJS/\', \'_system\');\">RxJS</a>" +
                    l(" library. Inspired by André Staltz's ") +
                    "<a href='#' onclick=\"window.open(\'https://github.com/staltz/rxmarbles/\', \'_system\');\">rxmarbles</a>" +
                    ".<p>" + l("RxJS version: ") + rxVersion + "</p>"
                ))),
                SVGButtonWithText.view({
                    x: buttonX,
                    y: buttonY,
                    fontSize: buttonFontSize,
                    fill: buttonColor,
                    width: buttonWidth,
                    height: buttonHeight,
                    press: vm.feedbackPressed,
                    onClick: vm.sendFeedback,
                    text: l('Send feedback')
                })
            ]);
        }
    }
}
