/**
 * Created by iyegoroff on 13/12/14.
 */

///<reference path='views/MainPageView.ts' />
///<reference path='viewmodels/MainPageViewModel.ts' />

module RxDiagram {

    export class MainPage {

        static create(x, y, width, height): Framework.Page {
            var vm = new MainPageViewModel(x, y, width, height);

            //MainPage.test(vm);
            //MainPage.generateIcon(vm);
            //MainPage.debounceTest(vm);

            return () => MainPageView.draw(vm);
        }

        //static test(vm) {
        //    var line1 = vm.createTestLine(0);
        //    var line2 = vm.createTestLine(1);
        //    var line3 = vm.createTestLine(2);
        //    var range = _.range(100, 1000, 25);
        //
        //    _.forEach(range, (x) => line1.createCircle(x, true));
        //    _.forEach(range, (x) => line2.createCircle(x, true));
        //    _.forEach(range, (x) => line3.createCircle(x, true));
        //}
        //
        //static generateIcon(vm) {
        //    vm.createTestLine(0).createTestCircle(screenWidth / 6, 'Rx');
        //}
        //
        //static debounceTest(vm) {
        //    var line = vm.createTestLine(0);
        //    var leftLimit = line.x1();
        //    var rightLimit = line.x2();
        //    console.log(leftLimit, rightLimit);
        //
        //    line.createCircle(CircleViewModel.timeToX(100, leftLimit, rightLimit), false, true, 0);
        //    line.createCircle(CircleViewModel.timeToX(600, leftLimit, rightLimit), false, true, 1);
        //    line.createCircle(CircleViewModel.timeToX(400, leftLimit, rightLimit), false, true, 2);
        //    line.createCircle(CircleViewModel.timeToX(700, leftLimit, rightLimit), false, true, 3);
        //    line.createCircle(CircleViewModel.timeToX(200, leftLimit, rightLimit), false, true, 4);
        //}

    }
}
