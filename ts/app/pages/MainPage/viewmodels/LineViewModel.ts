/**
 * Created by iyegoroff on 14/12/14.
 */

///<reference path='SegmentViewModel.ts' />
///<reference path='CircleViewModel.ts' />
///<reference path='../../../models/RxModel.ts' />
///<reference path='MainPageViewModel.ts' />

module RxDiagram {

    export interface LineViewModelDef extends SegmentViewModelDef {
        canEditCircles: () => boolean;
        key: string;
        x1: number;
        y1: number;
        x2: number;
        y2: number;
        circleRadius: number;
        isFinite: boolean;
        name: string;
        leftLimit: number;
        rightLimit: number;
        onEndCircleMoved?: Property<PointCallback>;
        onCircleMoved?: Property<PointCallback>;
        onCircleTapped?: Property<PointCallback>;
        onCircleDragged?: Property<Function>;
        mainViewX: number;
        mainViewY: number;
    }

    export class LineViewModel extends SegmentViewModel {

        constructor(def: LineViewModelDef) {
            super(def);

            this.leftLimit = def.leftLimit;
            this.rightLimit = def.rightLimit;
            this.onEndCircleMoved = def.onEndCircleMoved;
            this.onCircleMoved = def.onCircleMoved;
            this.onCircleTapped = def.onCircleTapped;
            this.onCircleDragged = def.onCircleDragged;

            this.x1 = m.prop(def.x1);
            this.x2 = m.prop(def.x2);
            this.y1 = m.prop(def.y1);
            this.y2 = m.prop(def.y2);
            this.key = m.prop(def.key);
            this.canEditCircles = def.canEditCircles;
            this.circleRadius = def.circleRadius;
            this.isFinite = m.prop(def.isFinite);
            this.endCircle(def.x2);
            this.nameCircle(def.name);

            this.mainViewX = def.mainViewX;
            this.mainViewY = def.mainViewY;

            if (def.bounds) {
                this.upperBorder = m.prop(def.bounds.y - def.bounds.height);
            }
        }

        drawInsideGroup(): boolean {
            return true;
        }

        circles(): CircleViewModel[] {
            return this.activeToTop(this.lineCircles);
        }

        groups(): CircleViewModel[][] {
            return this.activeToTop(
                this.circles().reduce(
                    (acc, circle) => {
                        var groupWithSameX = _.find(acc, group => group[0].x() === circle.x());

                        if (acc.length && groupWithSameX) {
                            groupWithSameX.push(circle);
                        } else {
                            acc.push([circle]);
                        }

                        return acc;
                    },
                    []
                ).map(g => g.some(c => !_.isNaN(+c.value()))
                        ? _.sortBy(g, (c: CircleViewModel) => +c.fill() + c.value())
                        : g)
            );
        }

        private activeToTop(circles: any): any {
            var activeKey = CircleViewModel.activeKey();

            var active = _.find(circles, (cvm: any) => {

                if (_.isArray(cvm)) {
                    return cvm.some((c: any) => c.key() === activeKey);
                } else {
                    return cvm.key() === activeKey;
                }

            });

            if (active) {

                if (_.isArray(active)) {
                    _.pull(circles, active).push(active);
                } else {
                    var activeGroup = circles.filter(c => c.x() === active.x());
                    activeGroup.forEach(g => _.pull(circles, g).push(g));
                }

            }

            return circles;
        }

        cleanUp() {
            this.line = null;
            this.lineCircles.forEach(x => x.cleanUp());

            if (this.lineEndCircle) {
                this.lineEndCircle.cleanUp();
            }

            if (this.lineNameCircle) {
                this.lineNameCircle.cleanUp();
            }
        }

        endCircle(x?: number): CircleViewModel {
            if (arguments.length !== 0) {
                this.lineEndCircle = new CircleViewModel({
                    parentLine: this,
                    x: x,
                    y: this.y2(),
                    radius: this.circleRadius,
                    key: +_.uniqueId(),
                    isPressable: false,
                    isInteractable: true,
                    isAnimated: false,
                    fill: 'transparent',
                    stroke: 'transparent',
                    onPositionChange: this.onEndCirclePositionChange(),
                    mainViewWidth: this.mainViewWidth(),
                    mainViewHeight: this.mainViewHeight(),
                    leftLimit: this.leftLimit,
                    rightLimit: this.rightLimit,
                    gradientId: this.gradientId()
                });
            }

            return this.lineEndCircle;
        }

        removeEndCircle() {
            if (this.lineEndCircle) {
                this.lineEndCircle.cleanUp();
                this.lineEndCircle = null;
            }
        }

        nameCircle(value?: string): CircleViewModel {
            if (arguments.length !== 0) {
                this.lineNameCircle = new CircleViewModel({
                    parentLine: this,
                    x: this.x1() / 2,
                    y: this.y1(),
                    radius: this.circleRadius,
                    key: +_.uniqueId(),
                    isInteractable: false,
                    value: value,
                    fill: 'white',
                    textFill: 'black',
                    mainViewWidth: this.mainViewWidth(),
                    mainViewHeight: this.mainViewHeight(),
                    leftLimit: this.leftLimit,
                    rightLimit: this.rightLimit,
                    gradientId: this.gradientId()
                });
            }

            return this.lineNameCircle;
        }

        name(): string {
            return this.lineNameCircle.value();
        }

        createTestCircle(x: number, value: string): CircleViewModel {
            var endCirlePos = this.endCircle().x();

            this.createCircle((x <= endCirlePos) ? x : endCirlePos, false, true, value);

            var circle: CircleViewModel = _.last(this.lineCircles);

            var cb = () => {
                if (circle.domWasCreated()) {
                    circle.x(circle.x() + 0.001);
                } else {
                    requestAnimationFrame(cb);
                }
            };

            cb();

            return circle;
        }

        createCircle(
            x: number,
            isAnimated?: boolean,
            isInteractable?: boolean,
            value?: string,
            color?: string | number
        ): boolean {
            if (x <= this.endCircle().x()) {
                var fill: string;

                if (typeof color === 'number') {
                    var colorString = color.toString(16);
                    fill = '#' + _.repeat('0', 6 - colorString.length) + colorString;
                } else {
                    fill = color;
                }

                var circle = new CircleViewModel({
                    parentLine: this,
                    x: x,
                    y: this.y1(),
                    radius: this.circleRadius,
                    key: +_.uniqueId(),
                    isInteractable: isInteractable,
                    isAnimated: isAnimated,
                    value: value,
                    onPositionChange: this.onCirclePositionChange(),
                    mainViewWidth: this.mainViewWidth(),
                    mainViewHeight: this.mainViewHeight(),
                    mainViewX: this.mainViewX,
                    mainViewY: this.mainViewY,
                    leftLimit: this.leftLimit,
                    rightLimit: this.rightLimit,
                    gradientId: this.gradientId(),
                    onCircleAnimationFinished: this.onCircleAnimationFinished(),
                    fill: fill,
                    onTapped: this.onCircleTapped,
                    onDragged: this.onCircleDragged
                });

                this.lineCircles.push(circle);

                return true;
            }

            return false;
        }

        remove(circle: CircleViewModel) {
            _.pull(this.lineCircles, circle);
        }

        isAnyCircleEdited(): boolean {
            return this.lineCircles.some(circle => circle.isEdited());
        }

        isAnyCircleDragged(): boolean {
            return this.lineCircles.some(circle => circle.isDragged())
        }

        getTimestamps(): ObservableInput {
            var input: any = this.lineCircles.map(x => x.getTimestamp());
            input.endTime = this.endCircle().getTimestamp().timestamp;
            return input;
        }

        config() {
            return (element: Element, isInitialized: boolean) => {
                if (!isInitialized) {
                    this.line = element;
                }
            };
        }

        onCirclePositionChange() {
            return (x: number) => {
                var endCircle = this.endCircle();

                if (x > endCircle.x()) {
                    endCircle.x(x);
                }

                var onCircleMoved: PointCallback = this.onCircleMoved();
                if (onCircleMoved) {
                    onCircleMoved(x, this.y2());
                }
            };
        }

        onEndCirclePositionChange() {
            return (x: number) => {
                this.lineCircles.forEach(circle => {
                    if (circle.x() > x) {
                        circle.x(x);
                    }
                });

                this.line.setAttribute('x2', this.x2(x) + '');

                var onEndMoved: PointCallback = this.onEndCircleMoved();
                if (onEndMoved) {
                    onEndMoved(this.x2(), this.y1());
                }
            };
        }

        onCircleAnimationFinished() {
            return () => {
                this.updateResult(this.index(), true);
            };
        }

        isActive(): boolean {
            return this.lineCircles.some(c => c.key() === CircleViewModel.activeKey());
        }

        x1: Property<number>;
        y1: Property<number>;
        x2: Property<number>;
        y2: Property<number>;
        key: Property<string>;
        isFinite: Property<boolean>;
        canEditCircles: () => boolean;
        upperBorder: Property<number>;

        private lineCircles: CircleViewModel[] = [];
        private lineEndCircle: CircleViewModel;
        private lineNameCircle: CircleViewModel;
        private circleRadius: number;
        private line: Element;
        private leftLimit: number;
        private rightLimit: number;
        private onEndCircleMoved: Property<PointCallback>;
        private onCircleMoved: Property<PointCallback>;
        private onCircleTapped: Property<PointCallback>;
        private onCircleDragged: Property<Function>;
        private mainViewX: number;
        private mainViewY: number;
    }
}
