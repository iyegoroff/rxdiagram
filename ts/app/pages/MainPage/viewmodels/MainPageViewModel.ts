/**
 * Created by iyegoroff on 14/12/14.
 */

///<reference path='../../../utils/Geometry.ts' />
///<reference path='../../../../lib/lodash.d.ts' />
///<reference path='../../../../lib/rx.interactions.d.ts' />
///<reference path='../views/ResultLineView.ts' />
///<reference path='../views/InputView.ts' />
///<reference path='../../../App.ts' />
///<reference path='../../../Types.ts' />
///<reference path='../../../components/SVGPointer.ts' />

module RxDiagram {

    export interface MainPageViewModelDef {
        isUiPresent: boolean;
        isHighlighted?: boolean;
    }

    export class MainPageViewModel {

        constructor(x, y, width, height, options?: MainPageViewModelDef, maxSegments: number = 5) {
            this.x = m.prop(x);
            this.y = m.prop(y);
            this.width = m.prop(width);
            this.height = m.prop(height);
            this.leftLimit = width * 0.1;
            this.rightLimit = width * 0.9;
            this.gradientId = m.prop(_.uniqueId(standardGradientId));
            this.segmentHeight = m.prop(height / (maxSegments + 1));

            this.clearSwipeMinDistance = width * 0.4;
            this.resultSegmentIndex = maxSegments - 1;
            this.inputSegmentIndex = maxSegments - 2;
            this.circleRadius = width * 0.025;

            this.createInput(this.inputSegmentIndex);

            this.delayedUpdateEmitter
                .debounce(MainPageViewModel.computeDelay)
                .subscribeOnNext(this.onUpdate);

            this.instantUpdateEmitter
                .subscribeOnNext(this.onUpdate);

            if (options) {
                this.isUiPresent(options.isUiPresent);
                this.isHighlighted(options.isHighlighted);
            }
        }

        segments(): SegmentViewModel[] {
            var active = _.find(this.drawSegments, seg => seg.isActive());

            if (active) {
                _.pull(this.drawSegments, active).push(active);
            }

            return this.drawSegments;
        }

        lineSegments = (): SegmentViewModel[] => {
            return this.drawSegments.filter(
                seg => seg.index() !== this.resultSegmentIndex && seg.index() !== this.inputSegmentIndex
            );
        };

        createElement(x, y) {
            var segment = this.findSegmentWithY(y);
            var segmentIndex = this.segmentIndex(y);
            var isEmptySegment = !segment;
            var isActiveSegment = segmentIndex < this.inputSegmentIndex;

            if (isActiveSegment) {
                if (isEmptySegment) {
                    this.createLine(segmentIndex);
                } else {
                    var isCircleCreated = segment.createCircle(x);

                    var onCircleCreated = this.onCircleCreated();
                    if (isCircleCreated && onCircleCreated) {
                        onCircleCreated(x, this.segmentY(segmentIndex));
                    }
                }
            }
        }

        clearAll() {
            for (var i = 0, last = this.resultSegmentIndex; i <= last; i += 1) {
                this.clearSegmentWithIndex(i);
            }

            this.onSegmentCleanUp(null);
            this.onSegmentCreated(null);
            this.onCircleCreated(null);
            this.onEndCircleMoved(null);
            this.onCircleMoved(null);
            this.onInputMethodSelected(null);
            this.onInputParameterSelected(null);
            this.onInputMethodScrolled(null);
            this.onResultCircleTapped(null);
            this.onResultCircleDragged(null);

            this.pointer(null);

            this.inputSegment().cleanUp();

            this.cleanUp();
        }

        clearSegmentWithY(y) {
            this.clearSegment(this.findSegmentWithY(y));
        }

        private clearSegmentWithIndex(index) {
            this.clearSegment(this.findSegmentWithIndex(index));
        }

        private clearSegment(segment: SegmentViewModel) {
            var inputSegment = this.inputSegment();
            if (segment && segment !== inputSegment) {
                segment.cleanUp();

                var segments = this.drawSegments;
                segments.splice(segments.indexOf(segment), 1);

                inputSegment.update();
                this.updateResult(segment.index());

                var onSegmentCleanUp = this.onSegmentCleanUp();
                if (onSegmentCleanUp && segment.index() !== this.resultSegmentIndex) {
                    onSegmentCleanUp(this.segmentY(segment.index()));
                }
            }
        }

        private getInputs(): any[] /* (string | number | ObservableInput)[] */ {
            return this.inputSegment().args()
                .map(arg => {
                    var segment: SegmentViewModel = _.find(this.drawSegments, seg => seg.name() === arg);

                    return MainPageViewModel.lineNames.indexOf(arg) > -1
                        ? segment ? segment.getTimestamps() : []
                        : arg
                });
        }

        isAnyCircleDragged() {
            return this.drawSegments.some(seg => seg.isAnyCircleDragged());
        }

        transitToTutorial = () => {
            if (!this.isAboutButtonPressed()) {
                this.stopMenuTransition();

                app.transitToTutorial(this.removeMenu);
            }
        };

        transitToAbout = () => {
            if (!this.isTutorialButtonPressed()) {
                this.stopMenuTransition();

                app.transitToAbout(this.removeMenu);
            }
        };

        private stopMenuTransition() {
            var menu = this.menu();

            if (menu) {
                menu.style[cssTransition] = '';
                menu.style[cssTransform] = '';
            }
        }

        hideAreaConfig() {
            return (element: any, isInitialized: boolean) => {
                if (!isInitialized) {

                    Rx.Observable.merge(
                        rxia.tap(element),
                        rxia.press(element),
                        rxia.swipe(element),
                        rxia.pan(element).move
                    ).subscribe(() => {
                        if (this.isMenuShown()) {
                            this.hidePopup(this.menu, this.isMenuShown, this.menuTransitionStart());
                        }
                    });
                }
            };
        }

        menuConfig(offset: number) {
            return (element: any, isInitialized: boolean) => {
                if (!isInitialized) {
                    this.menu(element);
                    this.showPopup(element, this.menuTransitionStart, offset, 0);
                }
            };
        }

        removeMenu = () => {
            this.isMenuShown(false);
            this.menu(null);
        };

        cleanUp = () => {
            this.drawSegments.map((seg) => seg.cleanUp());
            this.removeMenu();
        };

        private showPopup(element, counter: Property<number>, offsetX: number, offsetY: number) {
            var style = element.style;

            style[cssTransform] = MainPageViewModel.popupTransform();

            _.delay(() => {
                style[cssTransform] = MainPageViewModel.popupTransform(offsetX, offsetY);
                style[cssTransition] = MainPageViewModel.popupTransition(MainPageViewModel.animDelay);

                counter(Date.now());
            }, 10);
        }

        private hidePopup(element: Property<any>, isShown: Property<boolean>, startTime: number) {
            var fallbackDuration = Date.now() - startTime;
            var style = element().style;
            var duration = MainPageViewModel.animDelay;

            style[cssTransform] = MainPageViewModel.popupTransform();
            style[cssTransition] = MainPageViewModel.popupTransition(fallbackDuration > duration
                    ? duration
                    : fallbackDuration
            );

            Rx.Observable.fromEvent(element(), transitionEndEvent)
                .take(1)
                .delay(50)
                .subscribe(() => {
                    m.startComputation();
                    isShown(false);
                    element(null);
                    m.endComputation();
                });
        }

        private static popupTransform(offsetX = 0, offsetY = 0) {
            return 'translate3d(' + offsetX + 'px,' + offsetY + 'px,0)';
        }

        private static popupTransition(duration) {
            return prefixedTransform + ' ' + duration + 'ms linear';
        }

        private updateResult = (segIndex, isDelayed = false) => {
            if (segIndex !== this.resultSegmentIndex) {
                (isDelayed ? this.delayedUpdateEmitter : this.instantUpdateEmitter).onNext(null);
            }
        };

        private onUpdate = () => {
            _.delay(() => {

                m.startComputation();

                this.clearSegmentWithIndex(this.resultSegmentIndex);

                if (this.inputSegment().areAllInputsValid()) {

                    RxModel.computeWithColors(
                        this.inputSegment().operation(),
                        this.getInputs(),
                        (result: Rx.Timestamp<ColoredValue>[], isFinishedOnTime: boolean, endTime: number) => {

                            var line = this.createLine(
                                this.resultSegmentIndex,
                                isFinishedOnTime,
                                CircleViewModel.timeToX(endTime, this.leftLimit, this.rightLimit),
                                { x: this.x(), y: this.height(), width: this.width(), height: this.segmentHeight() * 2 }
                            );

                            line.endCircle().isInteractable(false);

                            result.forEach(x => line.createCircle(
                                CircleViewModel.timeToX(x.timestamp, this.leftLimit, this.rightLimit),
                                false,
                                false,
                                x.value.value === undefined ? 'U' : x.value.value,
                                x.value.color
                            ));

                            if (_.find(result, x => (x.value.value + '') === RxModel.errorValue())) {
                                line.removeEndCircle();
                            }

                            m.endComputation();
                        }
                    );

                } else {

                    m.endComputation();

                }

            }, 0);
        };

        createTestLine(segmentIndex): LineViewModel {
            return this.createLine(segmentIndex);
        }

        private createLine(segmentIndex, isFinite = true, x2?: number, bounds?: any): LineViewModel {
            var rightLimit = x2 || this.rightLimit;
            var posY = this.segmentY(segmentIndex);
            var isResultSegment = segmentIndex === this.resultSegmentIndex;

            var lineViewModel = new LineViewModel({
                viewClass: isResultSegment ? ResultLineView : LineView,
                updateResult: this.updateResult,
                index: segmentIndex,
                canEditCircles: () => this.drawSegments.every((s) => !s.isAnyCircleEdited()),
                key: _.uniqueId(),
                x1: this.leftLimit,
                y1: posY,
                x2: rightLimit,
                y2: posY,
                circleRadius: this.circleRadius,
                isFinite: isFinite,
                name: MainPageViewModel.lineNames[segmentIndex],
                mainViewWidth: this.width(),
                mainViewHeight: this.height(),
                mainViewX: this.x(),
                mainViewY: this.y(),
                leftLimit: this.leftLimit,
                rightLimit: this.rightLimit,
                gradientId: this.gradientId(),
                bounds: bounds,
                onEndCircleMoved: this.onEndCircleMoved,
                onCircleMoved: this.onCircleMoved,
                onCircleTapped: isResultSegment ? this.onResultCircleTapped : undefined,
                onCircleDragged: isResultSegment ? this.onResultCircleDragged : undefined
            });

            this.drawSegments.push(lineViewModel);

            this.inputSegment().update();
            this.updateResult(segmentIndex);

            var callback = this.onSegmentCreated();
            if (callback && !isResultSegment) {
                callback(posY);
            }

            return lineViewModel;
        }

        private createInput(segmentIndex) {
            var segmentHeight = this.segmentHeight();
            var width = this.width();

            var inputViewModel = new InputViewModel({
                viewClass: InputView,
                updateResult: this.updateResult,
                index: this.inputSegmentIndex,
                lineSegmentNames: MainPageViewModel.lineNames.split(''),
                validLineSegmentNames: () => this.lineSegments().map(x => x.name()),
                x: width * 0.02,
                y: this.segmentY(segmentIndex) - segmentHeight / 2,
                width: width * 0.96,
                height: segmentHeight,
                gradientId: this.gradientId(),
                mainViewWidth: this.width(),
                mainViewHeight: this.height(),
                onMethodSelected: this.onInputMethodSelected,
                onParameterSelected: this.onInputParameterSelected,
                onLongMethodScrolled: this.onInputMethodScrolled
            });

            this.inputSelectCenter(inputViewModel.selectCenter());

            this.inputMethodCenter(inputViewModel.methodCenter());

            this.inputMethodFirstArgumentCenter = () => {
                var p = inputViewModel.firstMethodArgumentCenter();
                return {
                    x: p.x - this.x(),
                    y: p.y - this.y()
                };
            };

            this.selectInputOption = opt => inputViewModel.selectedOption(opt);

            this.drawSegments.push(inputViewModel);
        }

        private segmentY(segmentIndex): number {
            return this.segmentHeight() * (segmentIndex + (segmentIndex === this.resultSegmentIndex ? 1 : 0.5));
        }

        private segmentIndex(y): number {
            var step = this.segmentHeight();
            var index = 0;

            while (y > step * index) {
                index += 1;
            }

            return index - 1;
        }

        private findSegmentWithIndex(index): SegmentViewModel {
            return _.find(this.drawSegments, seg => seg.index() === index);
        }

        private findSegmentWithY(y): SegmentViewModel {
            return this.findSegmentWithIndex(this.segmentIndex(y));
        }

        private inputSegment() {
            return this.findSegmentWithIndex(this.inputSegmentIndex);
        }

        config() {
            return (element, isInitialized) => {
                if (!isInitialized) {
                    this.initTap(element);
                    this.initSwipe(element);
                }
            };
        }

        private initTap(element: Element) {
            var onTap = (ev: rxia.Event) => {
                var x = ev.centerX - this.x();
                var y = ev.centerY - this.y();
                var target: Element = <Element>(ev.target);

                var isTapInsideLimits = x >= this.leftLimit && x <= this.rightLimit;
                var shouldHandleTap = target === element || target.tagName === 'line';

                if (isTapInsideLimits && shouldHandleTap) {
                    m.startComputation();
                    this.createElement(x, y);
                    m.endComputation();
                }
            };

            rxia.tap(element, {
                threshold: 50,
                preventDefault: false
            }).subscribe(onTap);
        }

        private initSwipe(element: Element) {
            var onSwipe = (ev: rxia.Event) => {
                var target: Element = <Element>(ev.target);

                var segment = this.findSegmentWithY(ev.centerY);
                var isValidGestureTarget = target === element || target.nodeName === 'line';
                var isLongSwipe = ev.distance >= this.clearSwipeMinDistance;
                var isLastSegment = segment && (segment.index() === this.resultSegmentIndex);
                var isHorizontalSwipe = Geometry.isInsideRange(ev.angle, 135, 225)
                    || Geometry.isInsideRange(ev.angle, 315, 360)
                    || Geometry.isInsideRange(ev.angle, 0, 45);

                var canSwipe = isValidGestureTarget
                    && isLongSwipe
                    && isHorizontalSwipe
                    && !isLastSegment;

                if (canSwipe) {
                    m.startComputation();
                    this.clearSegmentWithY(ev.centerY - this.y());
                    m.endComputation();
                }
            };

            rxia.swipe(element, {
                preventDefault: false
            }).subscribe(onSwipe);
        }

        defaultEndCircleX() {
            return this.rightLimit;
        }

        resultLineY(): number {
            return this.segmentY(this.resultSegmentIndex);
        }

        x: Property<number>;
        y: Property<number>;
        width: Property<number>;
        height: Property<number>;
        gradientId: Property<string>;
        segmentHeight: Property<number>;

        inputSelectCenter: Property<Point> = m.prop<Point>(null);
        inputMethodCenter: Property<Point> = m.prop<Point>(null);
        inputMethodFirstArgumentCenter: () => Point;
        selectInputOption: (opt: string) => void;

        isUiPresent: Property<boolean> = m.prop(true);
        isHighlighted: Property<boolean> = m.prop(false);

        isMenuShown: Property<boolean> = m.prop(false);
        isTutorialButtonPressed: Property<boolean> = m.prop(false);
        isAboutButtonPressed: Property<boolean> = m.prop(false);

        pointer: Property<SVGPointer> = m.prop<SVGPointer>(null);
        onSegmentCreated: Property<(y?: number) => void> = m.prop<(y?: number) => void>(null);
        onSegmentCleanUp: Property<(y?: number) => void> = m.prop<(y?: number) => void>(null);
        onEndCircleMoved: Property<PointCallback> = m.prop<PointCallback>(null);
        onCircleMoved: Property<PointCallback> = m.prop<PointCallback>(null);
        onCircleCreated: Property<PointCallback> = m.prop<PointCallback>(null);
        onInputMethodSelected: Property<Function> = m.prop<Function>(null);
        onInputParameterSelected: Property<Function> = m.prop<Function>(null);
        onInputMethodScrolled: Property<Function> = m.prop<Function>(null);
        onResultCircleTapped: Property<PointCallback> = m.prop<PointCallback>(null);
        onResultCircleDragged: Property<Function> = m.prop<Function>(null);

        private drawSegments: SegmentViewModel[] = [];
        private inputSegmentIndex: number;
        private resultSegmentIndex: number;
        private delayedUpdateEmitter = new Rx.Subject<any>();
        private instantUpdateEmitter = new Rx.Subject<any>();
        private circleRadius: number;
        private clearSwipeMinDistance: number;
        private leftLimit: number;
        private rightLimit: number;
        private menu: Property<any> = m.prop(null);
        private menuTransitionStart: Property<number> = m.prop(0);

        private static computeDelay = 300;
        private static lineNames = 'ABC';
        private static animDelay = 150;
    }
}
