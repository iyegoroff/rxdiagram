/**
 * Created by iyegoroff on 15/12/14.
 */

///<reference path='../../../../lib/framework.d.ts' />
///<reference path='../../../../lib/rx.all.d.ts' />
///<reference path='../../../models/RxModel.ts' />

module RxDiagram {

    export interface Drawable {
        draw(svm: SegmentViewModel): Mithril.VirtualElement[];
    }

    export interface SegmentViewModelDef {
        viewClass: Drawable;
        updateResult: (segIndex: number) => void;
        index: number;
        gradientId: string;
        bounds?: { x: number; y: number; height: number; width: number };
        mainViewWidth: number;
        mainViewHeight: number;
    }

    export class SegmentViewModel {

        constructor(def: SegmentViewModelDef) {
            this.index = m.prop(def.index);
            this.viewClass = def.viewClass;
            this.updateResult = def.updateResult;
            this.gradientId = m.prop(def.gradientId);
            this.mainViewHeight = m.prop(def.mainViewHeight);
            this.mainViewWidth = m.prop(def.mainViewWidth);
        }

        createCircle(x: number, isAnimated?: boolean, isInteractable?: boolean, value?: string): boolean {
            return false;
        }

        drawInsideGroup(): boolean {
            return false;
        }

        drawView(): Mithril.VirtualElement[] {
            return this.viewClass.draw(this);
        }

        isAnyCircleEdited(): boolean {
            return false;
        }

        isAnyCircleDragged(): boolean {
            return false;
        }

        getTimestamps(): ObservableInput {
            return <any>{};
        }

        operation(): string {
            return '';
        }

        args(): string[] {
            return [];
        }

        name(): string {
            return '';
        }

        areAllInputsValid(): boolean {
            return false;
        }

        update() {

        }

        cleanUp() {

        }

        isActive(): boolean {
            return false;
        }

        index: Property<number>;
        updateResult: (segIndex: number, isDelayed?: boolean) => void;
        gradientId: Property<string>;
        mainViewWidth: Property<number>;
        mainViewHeight: Property<number>;

        private viewClass: Drawable;
    }
}
