/**
 * Created by iyegoroff on 14/12/14.
 */

///<reference path='SegmentViewModel.ts' />
///<reference path='../../../Constants.ts' />
///<reference path='../../../Types.ts' />
///<reference path='../../../models/RxModel.ts' />

module RxDiagram {

    export interface InputViewModelDef extends SegmentViewModelDef {
        lineSegmentNames: string[];
        validLineSegmentNames: () => string[];
        x: number;
        y: number;
        width: number;
        height: number;
        onMethodSelected: Property<Function>;
        onParameterSelected: Property<Function>;
        onLongMethodScrolled: Property<Function>;
    }

    export class InputViewModel extends SegmentViewModel {

        constructor(def: InputViewModelDef) {
            super(def);

            var x = def.x, y = def.y, width = def.width, height = def.height;

            this.x = m.prop(x);
            this.y = m.prop(y);
            this.width = m.prop(width);
            this.height = m.prop(height);

            this.selectY = m.prop(y + height * 0.25);

            var xGap = this.selectY() - y;

            this.selectX = m.prop(x + xGap);
            this.selectWidth = m.prop(width * 0.2);
            this.selectHeight = m.prop(height * 0.5);

            this.selectedOption = Framework.prop(InputViewModel.operations()[0], this.onOperationSelected);

            this.lineSegmentNames = def.lineSegmentNames;
            this.validLineSegmentNames = def.validLineSegmentNames;

            var formatScale = (screenHeight > InputViewModel.greaterHeight)
                ? 1
                : InputViewModel.formatScale(screenHeight);

            var formatHeight = this.selectHeight() * formatScale;

            this.formatX = m.prop(this.selectX() + this.selectWidth() + xGap);
            this.formatY = m.prop(this.selectY() - (formatHeight - this.selectHeight()) / 2);
            this.formatWidth = m.prop(width - this.formatX());
            this.formatHeight = m.prop(formatHeight);
            this.formatFontSize = m.prop(this.formatHeight() / 2);
            this.formatScrollWidth = m.prop(xGap * 1.2 * formatScale);
            this.onMethodSelected = def.onMethodSelected;
            this.onParameterSelected = def.onParameterSelected;
            this.onLongMethodScrolled = def.onLongMethodScrolled;
        }

        private static formatScale(actualHeight: number): number {
            var scale = (InputViewModel.greaterHeight - actualHeight)
                        / (InputViewModel.greaterHeight - InputViewModel.lesserHeight);

            return (InputViewModel.lesserHeightCoef - InputViewModel.greaterHeightCoef) * scale
                   + InputViewModel.greaterHeightCoef;
        }

        onOperationSelected = (opt) => {
            this.methodHasChanged = true;

            this.update();
            this.updateResult(this.index());

            var onMethodSelected = this.onMethodSelected();
            if (onMethodSelected && opt !== this.prevSelectedOption()) {
                onMethodSelected();
            }
        };

        scrollWidthInSymbols(index: number) {
            return this.formatArguments().length
                ? _.max<string>(this.formatArguments()[this.mapFormatIndex(index)].map(x => x + ''), 'length').length
                : 1;
        }

        formatLength() {
            return this.format().reduce(
                (acc, x, idx) => acc + (typeof x === 'string' ? x.length : this.scrollWidthInSymbols(idx)),
                0
            );
        }

        areAllInputsValid(): boolean {
            var args = this.args();

            return args.every(x => !_.isString(x))
                   ||  args.filter(_.isString).some(x => this.validLineSegmentNames().indexOf(x) !== -1);
        }

        args(): string[] {
            return this.inputIndexes.length ? this.formatArguments().map((x, idx) => x[this.inputIndexes[idx]]) : [];
        }

        operation(): string {
            return this.selectedOption();
        }

        cleanUp() {
            this.selectedOption(InputViewModel.operations()[0]);
            this.inputIndexes = this.inputIndexes.map(() => 0);
        }

        format(): any[] {
            return RxModel.format(
                this.selectedOption(),
                this.lineSegmentNames,
                this.validLineSegmentNames().length
            );
        }

        isPreviousFormatValueString(index: number): boolean {
            return index && _.isString(this.format()[index - 1]);
        }

        formatConfig() {
            return (el, init, context) => {
                var option = this.selectedOption();
                var argsLength = this.args().length;

                if (this.prevSelectedOption() !== option || this.prevArgsLength() !== argsLength) {
                    context.component.scrollTo(0,0);

                    Framework.Scroller.updateBounds(context.component, x => {
                        return x.getBoundingClientRect().width - (parseFloat(x.style.paddingLeft) || 0);
                    });
                }

                this.prevSelectedOption(option);
                this.prevArgsLength(argsLength);
            };
        }

        formatElementConfig(index: number) {
            return (element: Element, isInitialized: boolean, context) => {
                var scrollerChildCount = element.firstChild.childNodes.length;
                var scroller: IScrollInstance = context.component;

                if (isInitialized && context.size !== scrollerChildCount) {
                    scroller.refresh();
                }

                context.size = scrollerChildCount;

                if (this.methodHasChanged) {
                    var idx = this.mapFormatIndex(index);

                    scroller.goToPage(0, this.inputIndexes[idx] || 0, 0);
                    if (idx === (this.inputIndexes.length - 1)) {
                        this.methodHasChanged = false;
                    }
                }

                if (index <= 1) {
                    var rect: ClientRect = element.getBoundingClientRect();

                    this.firstMethodArgumentCenter({
                        x: rect.left + rect.width / 2,
                        y: rect.top + rect.height / 2
                    });
                }
            };
        }

        updateInput(index: number, selected: number) {
            this.inputIndexes[this.mapFormatIndex(index)] = selected;
            this.updateResult(this.index());

            var onParameterSelected = this.onParameterSelected();
            if (onParameterSelected) {
                onParameterSelected();
            }
        }

        update() {
            var formatArguments = this.formatArguments();
            this.inputIndexes.length = formatArguments.length;
            this.inputIndexes = _.map(this.inputIndexes, x => x ? x : 0);

            formatArguments.forEach((x, i) => {
                if (this.inputIndexes[i] >= x.length) {
                    this.inputIndexes[i] = x.length - 1;
                }
            });
        }

        isValidInput(input: string) {
            return (_.isNaN(+input) ? this.validLineSegmentNames().indexOf(input) !== -1 : true);
        }

        selectCenter(): Point {
            return {
                x: this.selectX() + this.selectWidth() / 2,
                y: this.selectY() + this.selectHeight() / 2
            };
        }

        methodCenter(): Point {
            return {
                x: this.formatX() + this.formatWidth() / 2,
                y: this.formatY() + this.formatHeight() / 2
            };
        }

        longMethodScrollEnd = () => {
            var onLongMethodScrolled = this.onLongMethodScrolled();
            if (onLongMethodScrolled) {
                onLongMethodScrolled();
            }
        };

        private mapFormatIndex(idx: number) {
            return RxModel.mapFormatIndex(this.selectedOption(), idx);
        }

        private formatArguments(): any[] {
            return RxModel.formatArguments(this.format());
        }

        static operations(): string[] {
            return RxModel.operations();
        }

        x: Property<number>;
        y: Property<number>;
        width: Property<number>;
        height: Property<number>;

        selectX: Property<number>;
        selectY: Property<number>;
        selectWidth: Property<number>;
        selectHeight: Property<number>;

        formatX: Property<number>;
        formatY: Property<number>;
        formatWidth: Property<number>;
        formatHeight: Property<number>;
        formatFontSize: Property<number>;
        formatScrollWidth: Property<number>;

        selectedOption: Property<string>;

        prevSelectedOption: Property<string> = m.prop('amb');
        prevArgsLength: Property<number> = m.prop(0);

        firstMethodArgumentCenter: Property<Point> = m.prop<Point>(null);

        onMethodSelected: Property<Function> = m.prop<Function>(null);
        onParameterSelected: Property<Function> = m.prop<Function>(null);
        onLongMethodScrolled: Property<Function> = m.prop<Function>(null);

        private validLineSegmentNames: () => string[];
        private inputIndexes: number[] = [];
        private lineSegmentNames: string[];
        private methodHasChanged = false;

        private static greaterHeight = 768;
        private static lesserHeight = 553;
        private static greaterHeightCoef = 1;
        private static lesserHeightCoef = 1.5;
    }
}
