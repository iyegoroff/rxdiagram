/**
 * Created by iyegoroff on 15/12/14.
 */

///<reference path='../../../../lib/framework.d.ts' />
///<reference path='../../../../lib/lodash.d.ts' />
///<reference path='../../../../lib/rx.interactions.d.ts' />
///<reference path='LineViewModel.ts' />
///<reference path='../../../Constants.ts' />
///<reference path='../../../utils/Geometry.ts' />

module RxDiagram {

    export interface CircleViewModelDef {
        parentLine: LineViewModel;
        x: number;
        y: number;
        radius: number;
        key: number;
        isInteractable?: boolean;
        isPressable?: boolean;
        isAnimated?: boolean;
        value?: string;
        fill?: string;
        textFill?: string;
        onPositionChange?: (x: number, y: number) => void;
        stroke?: string;
        mainViewWidth: number;
        mainViewHeight: number;
        mainViewX?: number;
        mainViewY?: number;
        leftLimit: number;
        rightLimit: number;
        gradientId: string;
        onCircleAnimationFinished?: () => void;
        onTapped?: Property<PointCallback>;
        onDragged?: Property<Function>;
    }

    export interface CircleEditGeckoDef {
        rectWidth: number;
        rectHeight: number;
        scrollX: number;
        scrollY: number;
        buttonX: number;
        buttonY: number;
    }

    export interface CircleViewDef {
        value: any;
        x: number;
        y: number;
        scale: number;
        rotation: number;
        key: number;
        isError: boolean;
        radius: number;
        fill: string;
        stroke: string;
        capsuleOffset: number;
        fontSize: number;
        textY: number;
        textFill: string;
        isTextStroked: boolean;
        mainViewWidth?: number;
        mainViewHeight?: number;
        editRectSide?: number;
        gradientId?: string;
        isEdited?: Property<boolean>;
        isDeleteClicked?: Property<boolean>;
        config?: Function;
        remove?: Function;
        editAttributesForGecko?: Function;
        editConfig?: Function;
        finishScroll?: Function;
        groupConfig?: Function;
        editCloseConfig?: Function;
    }

    export class CircleViewModel {

        constructor(def: CircleViewModelDef) {
            this.x = Framework.prop(def.x, this.update);
            this.y = m.prop(def.y);
            this.radius = m.prop(def.radius);
            this.key = m.prop(def.key);
            this.mainViewWidth = m.prop(def.mainViewWidth);
            this.mainViewHeight = m.prop(def.mainViewHeight);
            this.leftLimit = def.leftLimit;
            this.rightLimit = def.rightLimit;
            this.gradientId = m.prop(def.gradientId);

            this.value = Framework.prop(
                def.value === undefined ? (_.random(99) + '') : def.value, () => this.updateResult()
            );

            this.textFill = m.prop(def.textFill || 'white');
            this.fontSize = m.prop(def.radius * 1.25);
            this.textY = m.prop(this.fontSize() / 2.8);

            this.parentLine = def.parentLine;

            var colors = CircleViewModel.fillColors;
            this.fill = m.prop(def.fill || colors[_.random(colors.length - 1)]);

            this.editRectSide = m.prop(def.radius * 4.8);

            this.isInteractable = m.prop(def.isInteractable !== undefined ? def.isInteractable : true);
            this.isAnimated = m.prop(def.isAnimated !== undefined ? def.isAnimated : true);
            this.isPressable = def.isPressable !== undefined ? def.isPressable : true;

            this.scale = Framework.prop(CircleViewModel.endScale, this.updateTransform);

            this.rotation = Framework.prop(0, this.updateTransform);

            this.isEdited = Framework.prop(false, this.updateEditing);
            this.isDragged = Framework.prop(false, this.updateDragging);

            this.onPositionChange = m.prop(def.onPositionChange);

            this.stroke = m.prop(def.stroke || 'black');

            this.onCircleAnimationFinished = def.onCircleAnimationFinished;

            this.onTapped = def.onTapped;
            this.onDragged = def.onDragged;

            this.mainViewX = def.mainViewX;
            this.mainViewY = def.mainViewY;

            CircleViewModel.activeKey(def.key);
        }

        static timeToX(time: number, leftLimit: number, rightLimit: number): number {
            return Math.round(time * (rightLimit - leftLimit) / timeScale + leftLimit);
        }

        static xToTime(x: number, leftLimit: number, rightLimit: number): number {
            return Math.round(timeScale * (x - leftLimit) / (rightLimit - leftLimit));
        }

        static maxValue(): number {
            return 100;
        }

        viewDef(): CircleViewDef {
            return {
                value: this.value(),
                x: this.x(),
                y: this.y(),
                scale: this.scale(),
                rotation: this.rotation(),
                key: this.key(),
                isError: this.isError(),
                radius: this.radius(),
                fill: this.fill(),
                stroke: this.stroke(),
                capsuleOffset: this.capsuleOffset(),
                fontSize: this.fontSize(),
                textY: this.textY(),
                textFill: this.textFill(),
                isTextStroked: this.mainViewWidth() === screenWidth,
                mainViewWidth: this.mainViewWidth(),
                mainViewHeight: this.mainViewHeight(),
                gradientId: this.gradientId(),
                editRectSide: this.editRectSide(),
                isEdited: this.isEdited,
                isDeleteClicked: this.isDeleteClicked,
                remove: this.remove,
                editAttributesForGecko: this.editAttributesForGecko,
                finishScroll: this.finishScroll,
                editConfig: this.editConfig,
                config: this.config,
                groupConfig: this.groupConfig,
                editCloseConfig: this.editCloseConfig
            };
        }

        isError() {
            return this.value() === RxModel.errorValue();
        }

        remove = () => {
            this.parentLine.remove(this);
            this.updateResult();
            this.cleanUp();

            var onEditMenuInteraction = this.onEditMenuInteraction();
            if (onEditMenuInteraction) {
                onEditMenuInteraction();
            }
        };

        cleanUp() {
            //this.drag(null);
            //this.press(null);
            this.element = null;
            this.groupElement = null;
        }

        canInteract(): boolean {
            return this.parentLine.canEditCircles();
        }

        getTimestamp(): Rx.Timestamp<any> {
            return {
                value: {
                    value: this.isError() ? <any>this.value() : +this.value(),
                    color: +this.fill().replace('#', '0x')
                },
                timestamp: CircleViewModel.xToTime(this.x(), this.leftLimit, this.rightLimit)
            };
        }

        update = () => {
            var cb = this.onPositionChange();
            if (cb) {
                cb(this.x(), this.y());
            }

            this.updateResult(true);
            this.updateTransform();
        };

        updateResult = (isDelayed?: boolean) => {
            var parent = this.parentLine;
            parent.updateResult(parent.index(), isDelayed);
        };

        updateGroupTransform = (y) => {
            this.groupElement.setAttribute(
                'style',
                CircleViewModel.circleStyle(0, y, 1, 0)
            );
        };

        updateTransform = () => {
            this.element.setAttribute(
                'style',
                CircleViewModel.circleStyle(this.x(), this.y(), this.scale(), this.rotation())
            );
        };

        updateDragging = (isDragging: boolean) => {
            if (isDragging) {
                this.makeActive();
            }
        };

        updateEditing = (isEditing: boolean) => {
            if (isEditing) {
                this.makeActive();
                //this.drag().pause();
                //this.press().pause();
            } else {
                //this.drag().resume();
                //this.press().resume();
            }

            var onEditMenuInteraction = this.onEditMenuInteraction();
            if (onEditMenuInteraction) {
                onEditMenuInteraction();
            }
        };

        static circleStyle(x, y, scale, rotation) {
            return prefixedTransform + ': ' + CircleViewModel.circleTransform(x, y, scale, rotation) + ';';
        }

        private static circleTransform(x, y, scale, rotation) {
            return Framework.matrix3d(x, y, scale, scale, rotation);
        }

        finishScroll = () => {
            return (scroller) => {
                var curPage = scroller.currentPage.pageY;
                this.value((curPage < CircleViewModel.maxValue()) ? <any>curPage : RxModel.errorValue());

                var onEditMenuInteraction = this.onEditMenuInteraction();
                if (onEditMenuInteraction) {
                    onEditMenuInteraction();
                }
            }
        };

        editConfig = () => {
            return (elem, isInitialized, context) => {
                if (!isInitialized) {
                    var val = this.value();

                    context.component.goToPage(
                        0,
                        val === RxModel.errorValue() ? CircleViewModel.maxValue() + 1 : +val,
                        0
                    );
                }
            };
        };

        editCloseConfig = () => {
            return (elem, init) => {
                if (!init) {
                    Rx.Observable.merge(
                        rxia.tap(elem),
                        rxia.press(elem),
                        rxia.swipe(elem),
                        rxia.pan(elem).move
                    )
                    .take(1)
                    .subscribe(() => {
                        m.startComputation();
                        this.isEdited(this.isDeleteClicked());
                        m.endComputation();
                    });
                }
            };
        };

        editAttributesForGecko = (def: CircleEditGeckoDef): Object => {
            var width = def.rectWidth, height = def.rectHeight;
            var isInsideDeleteButton = (x, y) => Geometry.isInsideRect(x, y, def.buttonX, def.buttonY, width, height);
            var x = this.mainViewX;
            var y = this.mainViewY;

            return {
                onmousedown: (ev) => {
                    if (isInsideDeleteButton(ev.pageX - x, ev.pageY - y)) this.isDeleteClicked(true);
                },
                onmouseup: (ev) => {
                    if (isInsideDeleteButton(ev.pageX - x, ev.pageY - y)) this.isDeleteClicked(false);
                },
                onmousemove: (ev) => {
                    if (isInsideDeleteButton(ev.pageX - x, ev.pageY - y)) this.isDeleteClicked(false);
                },
                onclick: (ev: MouseEvent) => {
                    var ex = ev.pageX - x, ey = ev.pageY - y;

                    if (isInsideDeleteButton(ex, ey)) {

                        this.remove();

                    } else if (!Geometry.isInsideRect(ex, ey, def.scrollX, def.scrollY, width, height)) {

                        this.isEdited(this.isDeleteClicked());

                    }
                }
            };
        };

        groupConfig = (maxDistance: number) => {
            return (element: any, isInitialized) => {
                this.config()(element, isInitialized);

                this.startGroupY = 0;

                if (!isInitialized) {
                    this.initDragGroup(element);

                    this.groupElement = element;
                    this.maxDragGroupDistance = maxDistance;
                }
            };
        };

        config = () => {
            return (element: any, isInitialized) => {
                if (!isInitialized) {
                    this.initTap(element);

                    if (this.isInteractable()) {
                        this.initDrag(element);

                        if (this.isPressable) {
                            this.initPress(element);
                        }
                        //this.initGestures();
                    }

                    this.element = element;
                }
            };
        };

        domWasCreated(): boolean {
            return this.element != undefined;
        }

        capsuleOffset(): number {
            var value = this.value();
            return _.isArray(value) ? value.reduce((acc, x) => acc + (x + '').length * this.radius() * 0.34, 0) : 0;
        }

        //private initGestures(element: Element) {
        //    var prevRotation = 0;
        //    var prevScale = 1;
        //
        //    var rotate = rxia.rotate(element);
        //
        //    rotate.start.subscribe(() => {
        //        prevRotation = 0;
        //
        //        if (!this.isEdited()) {
        //            this.drag().pause();
        //            this.press().pause();
        //        }
        //    });
        //
        //    rotate.move.subscribe((event: rxia.Event) => {
        //        if (!this.isEdited()) {
        //            var rotation = event.rotation * Math.PI / 180;
        //            this.rotation(this.rotation() + rotation - prevRotation);
        //            prevRotation = rotation;
        //        }
        //    });
        //
        //    rotate.end.subscribe(() => {
        //        if (!this.isEdited()) {
        //            this.drag().resume();
        //            this.press().resume();
        //        }
        //    });
        //
        //    var pinch = rxia.pinch(element);
        //
        //    pinch.start.subscribe(() => {
        //        prevScale = 1;
        //    });
        //
        //    pinch.move.subscribe((event: rxia.Event) => {
        //        if (!this.isEdited()) {
        //            var scale = event.scale;
        //            this.scale(this.scale() * scale / prevScale);
        //            prevScale = scale;
        //        }
        //    });
        //}

        private makeActive() {
            m.startComputation();
            CircleViewModel.activeKey(this.key());
            m.endComputation();
        }

        private initTap(element: Element) {
            rxia.tap(element).subscribe(this.onTap);
        }

        private onTap = (ev: rxia.Event) => {
            this.makeActive();

            var onTapped = this.onTapped && this.onTapped();
            if (onTapped) {
                onTapped(ev.centerX, ev.centerY);
            }
        };

        private initDragGroup(element) {
            var panGroup = rxia.pan(element);

            panGroup.start.subscribe(this.onDragGroupStart);
            panGroup.move.subscribe(this.onDragGroupMove);
            panGroup.end.delay(100).subscribe(this.onDragGroupEnd);
        }

        private onDragGroupStart = (ev: rxia.Event) => {
            this.startDragGroupY = ev.centerY - this.startGroupY;
            this.makeActive();
        };

        private onDragGroupMove = (ev: rxia.Event) => {
            var dist = this.maxDragGroupDistance;
            var offset = rxia.yOf(ev) - this.startDragGroupY;

            if (offset <= 0) {
                this.updateGroupTransform(Math.abs(offset) <= dist ? offset : -dist);
            }
        };

        private onDragGroupEnd = (ev: rxia.Event) => {
            var distance = Math.abs(this.startDragGroupY - rxia.yOf(ev));
            var groupElement = this.groupElement;

            groupElement.style[cssTransform] = CircleViewModel.circleTransform(0, 0, 1, 0);
            groupElement.style[cssTransition] = prefixedTransform + ' ' + (distance / screenHeight * 350) + 'ms linear';

            var onDragged = this.onDragged && this.onDragged();
            if (onDragged) {
                onDragged();
            }
        };

        private initDrag(element) {
            var makeElementDraggable = () => {
                var pan = rxia.pan(element);
                var drag = pan.move.pausable();

                pan.start.subscribe(this.onDragStart);
                pan.end.subscribe(this.onDragEnd);
                drag.subscribe(this.onDragMove);

                drag.resume();
                //this.drag(drag);

                if (this.onCircleAnimationFinished) {
                    this.onCircleAnimationFinished();
                }
            };

            if (this.isAnimated()) {
                this.isAnimated(false);

                var style = element.style;
                var scaleDelay = CircleViewModel.scaleDelay;

                style[cssTransform] = CircleViewModel.circleTransform(this.x(), this.y(), CircleViewModel.startScale, 0);

                _.delay(() => {
                    style[cssTransform] = CircleViewModel.circleTransform(this.x(), this.y(), CircleViewModel.endScale, 0);
                    style[cssTransition] = `${prefixedTransform} ${scaleDelay}ms linear`;

                    Rx.Observable.fromEvent(element, transitionEndEvent)
                        .timeout(scaleDelay * 2, Rx.Observable.just(null))
                        .take(1)
                        .subscribe(() => {
                            makeElementDraggable();
                            style[cssTransition]= '';
                        });
                }, 10);

            } else {
                makeElementDraggable();
            }
        }

        private realX = (x) => {
            return (x < this.leftLimit)
                ? this.leftLimit
                : (x > this.rightLimit)
                ? this.rightLimit
                : x;
        };

        private onDragStart = (ev: rxia.Event) => {
            this.startDragX = ev.centerX;
            this.startCircleX = this.x();

            this.isDragged(true);
        };

        private onDragMove = (ev: rxia.Event) => {
            if (!this.isEdited()) {
                var newX = this.realX(rxia.xOf(ev) - this.startDragX + this.startCircleX);

                if (this.x() !== newX) {
                    this.x(newX);
                }
            }
        };

        private onDragEnd = () => {
            this.isDragged(false);
        };

        private initPress(element: Element) {
            var onPress = (ev: rxia.Event) => {
                var target: Element = <Element>(ev.target);
                var canEditCircle = target.tagName.toLowerCase() !== 'g' && this.canInteract() && target.parentNode;

                if (canEditCircle) {
                    this.isEdited(true);

                    var onEditMenuCreated = this.onEditMenuCreated();
                    if (onEditMenuCreated) {
                        onEditMenuCreated(this.x(), this.y());
                    }
                }
            };

            var press = rxia.press(element)
                .pausable();

            press.subscribe(onPress);
            press.resume();

            //this.press(press);
        }

        static redFiller(): string {
            return CircleViewModel.fillColors[0];
        }

        static greenFiller(): string {
            return CircleViewModel.fillColors[1];
        }

        static blueFiller(): string {
            return CircleViewModel.fillColors[2];
        }

        static grayFiller(): string {
            return CircleViewModel.fillColors[0].substr(0, 3)
                + CircleViewModel.fillColors[1].substr(3, 2)
                + CircleViewModel.fillColors[2].substr(5);
        }

        static purpleFiller(): string {
            return CircleViewModel.fillColors[0].substr(0, 5)
                + CircleViewModel.fillColors[2].substr(5);
        }

        x: Property<number>;
        y: Property<number>;
        radius: Property<number>;
        fill: Property<string>;
        key: Property<number>;
        value: Property<any>;
        scale: Property<number>;
        rotation: Property<number>;
        stroke: Property<string>;
        //press: Property<any> = m.prop(null);
        //drag: Property<any> = m.prop(null);
        mainViewWidth: Property<number>;
        mainViewHeight: Property<number>;
        gradientId: Property<string>;

        textFill: Property<string>;
        textY: Property<number>;
        fontSize: Property<number>;

        isInteractable: Property<boolean>;
        isAnimated: Property<boolean>;
        isEdited: Property<boolean>;
        isDragged: Property<boolean>;
        editRectSide: Property<number>;

        isDeleteClicked: Property<boolean> = m.prop(false);

        onPositionChange: Property<PointCallback>;

        onEditMenuCreated: Property<PointCallback> = m.prop<PointCallback>(null);
        onEditMenuInteraction: Property<Function> = m.prop<Function>(null);
        onTapped: Property<PointCallback> = m.prop<PointCallback>(null);
        onDragged: Property<Function> = m.prop<Function>(null);

        private onCircleAnimationFinished: () => void;
        private parentLine: LineViewModel = null;
        private isPressable: boolean;
        private startDragX = 0;
        private startCircleX = 0;
        private startDragGroupY = 0;
        private startGroupY = 0;
        private maxDragGroupDistance: number;
        private element = null;
        private groupElement = null;
        private leftLimit: number;
        private rightLimit: number;
        private mainViewX: number;
        private mainViewY: number;

        static activeKey: Property<number> = m.prop(0);

        private static fillColors = ['#B00000', '#00B000', '#0000B0'];
        private static scaleDelay = 125;
        private static startScale = 0.1;
        private static endScale = 1;
    }
}
