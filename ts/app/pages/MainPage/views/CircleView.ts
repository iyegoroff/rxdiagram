/**
 * Created by iyegoroff on 15/12/14.
 */

///<reference path='../views/CircleEditView.ts' />
///<reference path='../../../utils/Geometry.ts' />

module RxDiagram {

    export class CircleView {

        static draw(vm: CircleViewModel): Mithril.VirtualElement {
            return vm.isEdited()
                ? CircleView.group(vm.viewDef(), CircleView.shape(vm.value()), CircleView.text, CircleEditView.draw)
                : CircleView.drawFromDef(vm.viewDef());
        }

        static drawFromDef(vd: CircleViewDef): Mithril.VirtualElement {
            return CircleView.group(vd, CircleView.shape(vd.value), CircleView.text);
        }

        private static shape(value: any): Function {
            return _.isArray(value) ? CircleView.capsule : CircleView.circle;
        }

        static group(vd: CircleViewDef, ...elements: Function[]): Mithril.VirtualElement {
            return m('g', {
                style: CircleViewModel.circleStyle(vd.x, vd.y, vd.scale, vd.rotation),
                key: vd.key,
                config: vd.config()
            }, elements.map(f => f(vd)));
        }

        static circle(vd: CircleViewDef): Mithril.VirtualElement {
            return vd.isError
                ? CircleView.errorCircle(vd)
                : m('circle', {
                    r: vd.radius,
                    fill: vd.fill,
                    stroke: vd.stroke,
                    style: 'stroke-width: 2;'
                });
        }

        private static capsule(vd: CircleViewDef): Mithril.VirtualElement {
            return m('path', {
                d: Geometry.capsule(vd.radius, vd.capsuleOffset),
                fill: vd.fill,
                stroke: vd.stroke,
                style: 'stroke-width: 2;'
            });
        }

        private static text(vd: CircleViewDef): Mithril.VirtualElement {
            var fontSize = vd.fontSize;
            var textY = vd.textY;
            var textStroke = 1;
            var value: any = vd.value;
            var scale;

            if (_.isArray(value)) {

                value = '[' + value + ']';

                scale = CircleView.textScale[0];
                fontSize *= scale;
                textY *= scale;
                textStroke = 0.5;

            } else {

                value += '';
                value = +value
                    ? +(+value).toFixed(2)
                    : value.length > 1 ? value[0].toUpperCase() : value;

                var valueLength = (value + '').length;

                if (valueLength > 2) {
                    scale = CircleView.textScale[valueLength - 3];

                    fontSize *= scale;
                    textY *= scale;
                    textStroke = 0.5;
                }
            }

            var textElement = m('text', {
                style: prefixedTransform + ': ' + Framework.matrix3d(0, textY, 1, 1, 0) + ';',
                'font-size': fontSize,
                'text-anchor': 'middle',
                fill: vd.textFill
            }, value);

            if (vd.isTextStroked) {
                (<any>textElement.attrs).stroke = 'black';
                (<any>textElement.attrs).style += 'stroke-width: ' + textStroke + ';';
            }

            return textElement;
        }

        private static errorCircle(vd: CircleViewDef): Mithril.VirtualElement {
            var radius = vd.radius;
            var innerRadius = radius * 0.9;
            var outerRadius = radius * 1.1;
            var step = Math.PI / 10;
            var offset = step / 2;

            var innerPoints = _.times(20, n => Geometry.pointOnCircle(0, 0, innerRadius, step * n - offset));
            var outerPoints = _.times(20, n => Geometry.pointOnCircle(0, 0, outerRadius, step * n));

            var points = _.flatten(_.zip(innerPoints, outerPoints))
                .concat(innerPoints[0])
                .reduce((str, p: any) => str + p.x + ',' + p.y + ' ','');

            return m('polyline', {
                points: points,
                stroke: 'black',
                style: 'stroke-width: 2;',
                fill: 'red'
            });
        }

        private static textScale = [0.81, 0.66, 0.52];
    }
}
