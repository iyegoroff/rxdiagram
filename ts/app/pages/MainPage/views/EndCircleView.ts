/**
 * Created by iyegoroff on 25/02/15.
 */

///<reference path='CircleView.ts' />

module RxDiagram {

    export class EndCircleView {

        static draw(vm: CircleViewModel): Mithril.VirtualElement {
            return CircleView.group(vm.viewDef(), CircleView.circle, EndCircleView.line);
        }

        private static line(vd: CircleViewDef): Mithril.VirtualElement {
            var halfLength = vd.radius * 1.2;

            return m('line', {
                x1: 0,
                y1: halfLength,
                x2: 0,
                y2: -halfLength,
                stroke: 'black',
                style: 'stroke-width: 5; stroke-linecap: round;'
            });
        }
    }
}
