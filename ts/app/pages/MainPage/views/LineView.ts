/**
 * Created by iyegoroff on 15/12/14.
 */

///<reference path='../../../../lib/framework.d.ts' />
///<reference path='../../../../lib/lodash.d.ts' />
///<reference path='CircleView.ts' />
///<reference path='EndCircleView.ts' />
///<reference path='../viewmodels/LineViewModel.ts' />

module RxDiagram {

    export class LineView {

        static draw(vm: LineViewModel): Mithril.VirtualElement[] {
            var line = LineView.line(vm);
            var name = vm.nameCircle();
            var elements = name ? [CircleView.draw(name), line] : [line];

            return LineView.appendEndCircle(elements, vm).concat(LineView.circles(vm));
        }

        static appendEndCircle(elements: Mithril.VirtualElement[], vm: LineViewModel): Mithril.VirtualElement[] {
            var end = vm.endCircle();

            if (end) {
                elements.push(vm.isFinite() ? EndCircleView.draw(end) : LineView.arrow(end));
            }

            return elements;
        }

        private static circles(vm: LineViewModel): Mithril.VirtualElement[] {
            return vm.circles().map(cvm => CircleView.draw(cvm));
        }

        static line(vm: LineViewModel): Mithril.VirtualElement {
            return m('line', {
                x1: vm.x1(),
                y1: vm.y1(),
                x2: vm.x2(),
                y2: vm.y2(),
                stroke: 'black',
                style: 'stroke-width: 5; stroke-linecap: round;',
                key: vm.key(),
                config: vm.config()
            });
        }

        private static arrow(vm: CircleViewModel): Mithril.VirtualElement {
            var x = vm.x(), y = vm.y(), radius = vm.radius();

            return m('polyline', {
                points: x + ',' + (y - radius * 0.5) + ' '
                        + (x + radius * 1.5) + ',' + y + ' '
                        + x + ',' + (y + radius * 0.5),
                stroke: 'black'
            });
        }
    }
}
