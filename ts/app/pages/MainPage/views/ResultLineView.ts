/**
 * Created by iyegoroff on 29/03/15.
 */

///<reference path='LineView.ts' />

module RxDiagram {

    export class ResultLineView {

        static draw(vm: LineViewModel): Mithril.VirtualElement[] {
            var groups = vm.groups().map(
                g => g.length > 1
                    ? ResultLineView.group(g.map(c => c.viewDef()), vm.upperBorder())
                    : CircleView.draw(g[0])
            );

            return LineView.appendEndCircle([LineView.line(vm)], vm).concat(groups);
        }

        static group(cvms: CircleViewDef[], upperBorder: number): Mithril.VirtualElement {
            var cvm = cvms[0];
            var r = cvm.radius;
            var x = cvm.x;
            var y = cvm.y;
            var scale = cvm.scale;
            var radius = 1.14 * r;
            var vertOffset = (cvms.length - 1) * 1.07 * r;
            var horizOffset = _.max(cvms, cvm => cvm.capsuleOffset).capsuleOffset;
            var rectWidth = (horizOffset + radius) * 2;
            var rectHeight = (vertOffset + radius) * 2;

            var borderOffset = (upperBorder - (y - vertOffset - radius));
            if (borderOffset > 0) {
                borderOffset += r * 0.2;
                y += borderOffset;
            } else {
                borderOffset = 0;
            }

            var shape = [
                horizOffset
                ? m('rect', {
                    x: -rectWidth / 2 + x,
                    y: -rectHeight / 2 + y,
                    width: rectWidth,
                    height: rectHeight,
                    rx: radius,
                    ry: radius,
                    fill: 'white',
                    stroke: 'black',
                    style: 'stroke-width: 2;'
                })
                : m('path', {
                    d: Geometry.capsule(radius, vertOffset),
                    fill: 'white',
                    stroke: 'black',
                    style: 'stroke-width: 2;',
                    transform: Framework.matrix(x, y, scale, scale, Math.PI / 2)
                })
            ];

            vertOffset += borderOffset;

            return m('g', {
                'class': 'noselect',
                key: cvm.key,
                config: borderOffset ? cvm.groupConfig(vertOffset) : cvm.config()
            }, shape.concat(cvms.map((cvm, idx) => ResultLineView.circleWithOffset(cvm, vertOffset - 2.14 * idx * r))));
        }

        static circleWithOffset(vd: CircleViewDef, yOffset: number): Mithril.VirtualElement {
            var circle = CircleView.drawFromDef(vd);
            var y = vd.y + yOffset;

            circle.attrs['style'] = CircleViewModel.circleStyle(vd.x, y, vd.scale, vd.rotation);

            return circle;
        }
    }
}
