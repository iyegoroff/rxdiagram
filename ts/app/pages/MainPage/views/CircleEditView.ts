/**
 * Created by iyegoroff on 06/01/15.
 */

///<reference path='../../../../lib/framework.d.ts' />
///<reference path='../../../Constants.ts' />
///<reference path='../../../components/SVGButtonWithText.ts' />
///<reference path='../viewmodels/CircleViewModel.ts' />

module RxDiagram {

    export class CircleEditView {

        static draw(vd: CircleViewDef): Mithril.VirtualElement[] {
            var side = vd.editRectSide;
            var buttonX = side * 0.25;
            var buttonY = -side * 0.45;
            var buttonWidth = side * 0.45;
            var buttonHeight = side * 0.9;
            var offsetX = side * 0.05;
            var stroke = 1;
            var liHeight = buttonHeight - stroke * 2;
            var x = vd.x;
            var y = vd.y;
            var maxValue = CircleViewModel.maxValue();
            var mainViewWidth = vd.mainViewWidth;
            var mainViewHeight = vd.mainViewHeight;
            var rxy = uiCornerRoundingCoef * vd.mainViewWidth;
            var fontSize = buttonWidth * 0.5;
            var isGecko = Framework.isGecko();

            var positionOffset = side / 2 - y;
            positionOffset = (positionOffset < 0) ? 0 : (positionOffset + stroke * 2);

            var rect = m('rect', {
                x: -side * 0.25,
                y: -side * 0.5 + positionOffset,
                width: side * 1.05,
                height: side,
                rx: rxy,
                ry: rxy,
                stroke: '#000000',
                fill: 'url(#' + vd.gradientId + ')',
                'stroke-width': stroke
            });

            var cancelArea = m('rect', {
                x: -x,
                y: -y,
                width: mainViewWidth,
                height: mainViewHeight,
                'fill-opacity': 0,
                config: vd.editCloseConfig()
            });

            var selectRect = m('rect', {
                x: -buttonX + offsetX,
                y: buttonY + positionOffset,
                width: buttonWidth,
                height: buttonHeight,
                rx: rxy,
                ry: rxy,
                stroke: '#000000',
                fill:'white',
                'stroke-width': 1
            });

            var button = SVGButtonWithText.view({
                x: buttonX + offsetX,
                y: buttonY + positionOffset,
                width: buttonWidth,
                height: buttonHeight,
                radius: rxy,
                fill: 0xFF6666,
                press: vd.isDeleteClicked,
                onClick: vd.remove,
                text: 'X',
                fontSize: fontSize,
                fontColor: 0x7F3333
            });

            var valueSelectAttrs = {
                width: mainViewWidth,
                height: mainViewHeight,
                x: -x,
                y: -y + (isGecko ? 0 : positionOffset)
            };

            var scrollElemHeight = (buttonHeight * 0.5 + (isGecko ? 0 : positionOffset));

            var valueSelect = m('foreignObject', isGecko
                    ? _.extend(valueSelectAttrs, vd.editAttributesForGecko({
                          rectWidth: buttonWidth,
                          rectHeight: buttonHeight,
                          scrollX: x - buttonX + offsetX,
                          scrollY: y + buttonY,
                          buttonX: x + buttonX + offsetX,
                          buttonY: y + buttonY
                      }))
                    : valueSelectAttrs,
                Framework.Scroller.view('div', {
                   xmlns: xmlns,
                   style: 'top: ' + (buttonY + y + (isGecko ? positionOffset : 0)) + 'px; '
                       + 'left: ' + (-buttonX + x + offsetX + rxy) + 'px; '
                       + 'width: ' + (buttonWidth - rxy * 2) + 'px; '
                       + 'height: ' + (liHeight + stroke) + 'px;',
                   config: vd.editConfig(),
                   options: {
                       freeScroll: true,
                       scrollX: false,
                       snap: 'li',
                       scrollEnd: vd.finishScroll(),
                       scrollCancel: vd.finishScroll()
                   }
                }, m('ul', {
                    style: 'list-style: none; padding-left: 0; margin-top: ' + scrollElemHeight + 'px;'
                }, (<any[]>_.range(maxValue)).concat('E').map(x => m('li', {
                    style: 'height: ' + liHeight + 'px; padding-left: 50%;'
                }, m('div', {
                    style: 'position: absolute; ' + prefixedTransform + ': translate(-50%,-50%); font-size: '
                           + fontSize + 'px;'
                }, x)))))
            );

            return [cancelArea, rect, valueSelect, selectRect, button];
        }

    }
}
