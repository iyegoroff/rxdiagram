/**
 * Created by iyegoroff on 13/12/14.
 */

///<reference path='../../../../lib/framework.d.ts' />
///<reference path='../../../views/GradientView.ts' />
///<reference path='../viewmodels/MainPageViewModel.ts' />
///<reference path='../../../components/SVGButtonWithText.ts' />
///<reference path='../../../Locales.ts' />

module RxDiagram {

    export class MainPageView {

        static draw(vm: MainPageViewModel): Mithril.VirtualElement {
            var mainView = MainPageView.main(vm, 1);

            return vm.isUiPresent()
                ? [mainView, MainPageView.ui(vm, 2)]
                : mainView;
        }

        private static ui(vm: MainPageViewModel, key: number): Mithril.VirtualElement {
            var isMenuShown = vm.isMenuShown();

            var elements = isMenuShown
                ? [MainPageView.hideArea(vm), MainPageView.menu(vm)]
                : [];

            return m('svg', {
                width: vm.width(),
                height: vm.height(),
                style: 'position: absolute; pointer-events: ' + (isMenuShown ? 'auto;' : 'none;'),
                'class': 'noselect',
                key: key
            }, elements);
        }

        private static main(vm: MainPageViewModel, key: number): Mithril.VirtualElement {
            var elements = [
                GradientView.draw(vm.gradientId()),
                MainPageView.bottomSegment(vm),
                MainPageView.groupedSegments(vm, 1),
                vm.isHighlighted() ? MainPageView.highlightedAreas(vm, 2) : null,
                vm.isUiPresent() ? MainPageView.menuButton(vm, 3) : null,
                vm.pointer() ? MainPageView.pointer(vm, 4) : null
            ];

            return m('svg', {
                width: vm.width(),
                height: vm.height(),
                style: 'position: absolute; ' +
                        prefixedTransform + ': translate(' + vm.x() + 'px,' + vm.y() + 'px);' +
                       'background-color: white;',
                'class': 'noselect',
                config: vm.config(),
                key: key
            }, elements);
        }

        private static pointer(vm: MainPageViewModel, key: number): Mithril.VirtualElement {
            return vm.pointer().view(
                m('circle', {
                    r: vm.width() / 40,
                    fill: 'red'
                }),
                key
            );
        }

        private static highlightedAreas(vm: MainPageViewModel, key: number): Mithril.VirtualElement {
            var width = vm.width(), height = vm.height(), segmentHeight = vm.segmentHeight();

            var strokeWidth = width * 0.025;
            var observablesHeight = (segmentHeight * 3);
            var inputHeight = segmentHeight;
            var resultHeight = (segmentHeight * 2);
            var offset = width * 0.025;
            var rectWidth = width * 0.95;

            return m('g', {
                key: key,
                style: 'stroke-opacity: 0.5; opacity: 0.25; stroke-width: ' + strokeWidth + 'px;'
            }, [
                m('rect', {
                    x: 0,
                    y: 0,
                    width: width,
                    height: height,
                    style: 'fill: transparent;'
                }),
                m('rect', {
                    x: offset,
                    y: offset,
                    width: rectWidth,
                    height: observablesHeight - offset * 2,
                    style: 'fill: red; stroke: red;'
                }),
                m('rect', {
                    x: offset,
                    y: observablesHeight + offset,
                    width: rectWidth,
                    height: inputHeight - offset * 2,
                    style: 'fill: green; stroke: green;'
                }),
                m('rect', {
                    x: offset,
                    y: observablesHeight + inputHeight + offset,
                    width: rectWidth,
                    height: resultHeight - offset * 2,
                    style: 'fill: blue; stroke: blue;'
                })
            ]);
        }

        private static bottomSegment(vm: MainPageViewModel): Mithril.VirtualElement {
            return vm.segments().filter(s => !s.drawInsideGroup()).map(s => s.drawView())[0];
        }

        private static groupedSegments(vm: MainPageViewModel, key: number): Mithril.VirtualElement {
            return m('g', { key: key }, vm.segments().filter(s => s.drawInsideGroup()).map(s => s.drawView()));
        }

        private static menuButton(vm: MainPageViewModel, key: number): Mithril.VirtualElement {
            var width = vm.width();

            var x  = width * 0.97;
            var y = width * 0.03;
            var radius = width * 0.0045;
            var offset = width * 0.012;

            return m('g', {
                transform: Framework.matrix(x, y, 1, 1, 0),
                onclick: () => vm.isMenuShown(true),
                key: key
            }, [
                m('rect', {
                    x: -y,
                    y: -y,
                    width: 2 * y,
                    height: 2 * y,
                    fill: 'transparent'
                }),
                m('path', {
                    d: Geometry.capsule(radius, offset),
                    fill: 'gray',
                    transform: Framework.matrix(0, -2.5 * radius, 1, 1, 0)
                }),
                m('path', {
                    d: Geometry.capsule(radius, offset),
                    fill: 'gray'
                }),
                m('path', {
                    d: Geometry.capsule(radius, offset),
                    fill: 'gray',
                    transform: Framework.matrix(0, 2.5 * radius, 1, 1, 0)
                })
            ]);
        }

        private static hideArea(vm: MainPageViewModel): Mithril.VirtualElement {
            return m('rect', {
                x: 0,
                y: 0,
                width: vm.width(),
                height: vm.height(),
                fill: 'transparent',
                config: vm.hideAreaConfig()
            });
        }

        private static menu(vm: MainPageViewModel): Mithril.VirtualElement {
            var width = vm.width(), height = vm.height();

            var menuWidth = width * 0.25;
            var menuHeight = height * 0.2;
            var offset = width * 0.01;
            var rxy = uiCornerRoundingCoef * width;
            var x = width + offset;
            var buttonX = menuWidth * 0.05 + x;
            var buttonY = menuHeight * 0.05 + offset;
            var fontSize = width * 0.02;
            var animOffset = -(menuWidth + 2 * offset);

            return m('g', {
                config: vm.menuConfig(animOffset),
                key: 4
            }, [
                m('rect', {
                    x: x,
                    y: offset,
                    rx: rxy,
                    ry: rxy,
                    width: menuWidth,
                    height: menuHeight,
                    stroke: '#cccccc',
                    style: 'fill: url(#' + vm.gradientId() + '); stroke-width: 2;'
                }),
                SVGButtonWithText.view({
                    x: buttonX,
                    y: buttonY,
                    width: menuWidth * 0.9,
                    height: menuHeight * 0.4,
                    fill: buttonColor,
                    press: vm.isTutorialButtonPressed,
                    onClick: vm.transitToTutorial,
                    text: l('Tutorial'),
                    fontSize: fontSize,
                    radius: rxy
                }),
                SVGButtonWithText.view({
                    x: buttonX,
                    y: buttonY + menuHeight * 0.5,
                    width: menuWidth * 0.9,
                    height: menuHeight * 0.4,
                    fill: buttonColor,
                    press: vm.isAboutButtonPressed,
                    onClick: vm.transitToAbout,
                    text: l('About'),
                    fontSize: fontSize,
                    radius: rxy
                })
            ]);
        }
    }
}
