/**
 * Created by iyegoroff on 22/12/14.
 */

///<reference path='../viewmodels/InputViewModel.ts' />
///<reference path='../../../../lib/cordova.d.ts' />

module RxDiagram {

    export class InputView {

        static draw(vm: InputViewModel): Mithril.VirtualElement[] {
            var rxy = uiCornerRoundingCoef * vm.mainViewWidth();
            var selectedOption = vm.selectedOption();

            var rect = m('rect', {
                x: vm.x(),
                y: vm.y(),
                width: vm.width(),
                height: vm.height(),
                rx: rxy,
                ry: rxy,
                stroke: '#cccccc',
                style: 'fill: url(#' + vm.gradientId() + '); stroke-width: 2;'
            });

            var select = m('foreignObject', {
                width: vm.selectWidth(),
                height: vm.selectHeight(),
                x: vm.selectX(),
                y: vm.selectY()
            }, m('select', {
                xmlns: xmlns,
                'class': 'select-operation noselect',
                style: 'height: ' + vm.selectHeight() + 'px;',
                value: selectedOption,
                onchange: function () { vm.selectedOption(this.options[this.selectedIndex].value); }
            }, InputViewModel.operations().map(option => {
                return m(
                    'option',
                    selectedOption === option
                        ? { value: option, selected: true }
                        : { value: option },
                    option
                );
            })));

            var fontSize = vm.formatFontSize();
            var formatTextHeight = fontSize * (cordova.platformId === 'android' ? 1.1 : 1);
            var formatHeight = vm.formatHeight();

            var format = m('foreignObject', {
                width: vm.formatWidth(),
                height: formatHeight,
                x: vm.formatX(),
                y: vm.formatY()
            }, m('div', {
                xmlns: xmlns,
                style: 'font-size: ' + fontSize + 'px;',
                'class': 'noselect'
            }, Framework.Scroller.view('div', {
                style: 'position: static; -webkit-backface-visibility: visible;',
                options: {
                    freeScroll: true,
                    scrollY: false,
                    scrollX: true,
                    scrollEnd: vm.longMethodScrollEnd
                },
                config: vm.formatConfig()
            }, m('div', {
                style: 'overflow: hidden;' +
                       'white-space: nowrap;' +
                       'height: ' + formatHeight + 'px;' +
                       'width: 100%;' +
                       'position: relative;' +
                       'top: ' + ((formatHeight - fontSize) / 2) + 'px;'
            }, vm.format().map(InputView.formatElement(vm, fontSize, formatTextHeight))))));

            return [rect, select, format];
        }

        private static formatElement(vm: InputViewModel, fontSize: number, height: number) {
            return (value: any, index: number): Mithril.VirtualElement => {
                if (_.isString(value)) {
                    var padding = index && !vm.isPreviousFormatValueString(index)
                        ? vm.formatScrollWidth() * InputView.scrollWidthMultipliers[vm.scrollWidthInSymbols(index - 1) - 1]
                        : 0;

                    return m('span', {
                        style: 'padding-left: ' + padding + 'px;',
                        key: index
                    }, value);
                } else {
                    var scrollWidth = vm.formatScrollWidth()
                                      * InputView.scrollWidthMultipliers[vm.scrollWidthInSymbols(index) - 1];

                    return Framework.Scroller.view('span', {
                        style: 'top: 0%; width: ' + scrollWidth + 'px; height: ' + height + 'px;',
                        key: index,
                        options: {
                            freeScroll: true,
                            scrollX: false,
                            snap: true,
                            scrollEnd: (scroller) => vm.updateInput(index, scroller.currentPage.pageY)
                        },
                        config: vm.formatElementConfig(index)
                    }, m('ul', {
                        style: 'list-style: none; padding-left: 0; margin-top: 0px;'
                    }, value.map(x => m('li', {
                        style: 'height: ' + height + 'px; padding-left: 50%;'
                    }, m('div', {
                        style: 'position: absolute; ' + prefixedTransform + ': translate(-50%,0%); font-size: '
                        + fontSize
                        + 'px; color: ' + (vm.isValidInput(x) ? 'green;' : 'red;')
                    }, x)))));
                }
            };
        }

        private static scrollWidthMultipliers = [0.9, 1.3, 1.8];
    }
}
