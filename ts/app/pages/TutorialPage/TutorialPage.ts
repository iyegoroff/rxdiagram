/**
 * Created by iyegoroff on 27/04/15.
 */

///<reference path='views/TutorialPageView.ts' />
///<reference path='viewmodels/TutorialPageViewModel.ts' />

module RxDiagram {

    export class TutorialPage {

        static create(): Framework.Page {
            var vm = new TutorialPageViewModel();

            return () => TutorialPageView.draw(vm);
        }
    }
}
