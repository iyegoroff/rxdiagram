/**
 * Created by iyegoroff on 27/04/15.
 */

///<reference path='../../MainPage/viewmodels/MainPageViewModel.ts' />

module RxDiagram {

    export class TutorialPageViewModel {

        constructor() {
            this.pageModel = m.prop(
                new MainPageViewModel(
                    TutorialPageViewModel.x(),
                    TutorialPageViewModel.y(),
                    TutorialPageViewModel.width(),
                    TutorialPageViewModel.height(),
                    {
                        isUiPresent: false,
                        isHighlighted: true
                    }
                )
            );

            this.selectedPage = Framework.prop(0, this.onPageSelect);
        }

        transitBack = () => {
            app.transitBackToMain(() => {
                this.pageModel().cleanUp();
                this.pageModel(null);

                clearTimeout(this.currentTimerId);
            });
        };

        onPrevClicked = () => {
            var page = this.selectedPage();

            if (page > 0) {
                this.selectedPage(page - 1);
            }
        };

        onNextClicked = () => {
            var page = this.selectedPage();

            if (page < (this.pageCount() - 1)) {
                this.selectedPage(page + 1);
            }
        };

        infoConfig() {
            return (elem, init, context: any) => {
                if (init && context.page !== this.selectedPage()) {
                    context.component.scrollTo(0, 0);
                    Framework.Scroller.updateBounds(context.component, undefined, x => {
                        return x.getBoundingClientRect().height;
                    });
                }

                context.page = this.selectedPage();
            };
        }

        isGeneralInfoPage(): boolean {
            return this.selectedPage() === 0;
        }

        isCreatingAndDeletingObservablesPage(): boolean {
            return this.selectedPage() === 1;
        }

        isCreatingValuesPage(): boolean {
            return this.selectedPage() === 2;
        }

        isEditingAndDeletingValuesPage(): boolean {
            return this.selectedPage() === 3;
        }

        isChoosingMethodsPage(): boolean {
            return this.selectedPage() === 4;
        }

        isInteractingWithResultPage(): boolean {
            return this.selectedPage() === 5;
        }

        isValueTypesPage(): boolean {
            return this.selectedPage() === 6;
        }

        private onPageSelect = () => {
            this.pageModel().clearAll();

            clearTimeout(this.currentTimerId);

            if (this.isGeneralInfoPage()) {

                this.startGeneralInfoScenario();

            } else if (this.isCreatingAndDeletingObservablesPage()) {

                this.startCreatingAndDeletingObservablesScenario();

            } else if (this.isCreatingValuesPage()) {

                this.startCreatingValuesScenario();

            } else if (this.isEditingAndDeletingValuesPage()) {

                this.startEditingAndDeletingValuesScenario();

            } else if (this.isChoosingMethodsPage()) {

                this.startChoosingMethodsScenario();

            } else if (this.isInteractingWithResultPage()) {

                this.startInteractingWithResultScenario();
            }
        };

        private startGeneralInfoScenario(): void {
            var pageModel = this.pageModel();

            m.startComputation();

            pageModel.isHighlighted(true);

            m.endComputation();
        }

        private startCreatingAndDeletingObservablesScenario(): void {
            var pageModel = this.pageModel();
            var halfWidth = TutorialPageViewModel.width() / 2;
            var quarterWidth = halfWidth / 2;

            m.startComputation();

            this.highlightRed(0);

            pageModel.pointer(
                new SVGPointer(halfWidth, TutorialPageViewModel.height() / 4, 1)
                    .tap(1500)
                    .infinite()
            );

            pageModel.isHighlighted(false);

            m.endComputation();

            pageModel.onSegmentCreated((segY: number) => {
                pageModel.onSegmentCreated(null);

                m.startComputation();

                this.highlightRed(1);

                pageModel.pointer(
                    new SVGPointer(pageModel.defaultEndCircleX(), segY, 2)
                        .press(750)
                        .moveBy(2000, -quarterWidth, 0).release(750)
                        .infinite()
                );

                m.endComputation();

                pageModel.onEndCircleMoved(_.debounce((x, y) => {
                    if (y === segY) {
                        pageModel.onEndCircleMoved(null);

                        var offset = TutorialPageViewModel.height() / 24;

                        m.startComputation();

                        this.highlightRed(2);

                        pageModel.pointer(
                            new SVGPointer(quarterWidth, y + offset, 3)
                                .press(750)
                                .moveBy(1000, halfWidth, -2 * offset)
                                .release(750)
                                .infinite()
                        );

                        m.endComputation();
                    }
                }, TutorialPageViewModel.debounceInterval));

                pageModel.onSegmentCleanUp(y => {
                    if (y === segY) {
                        this.removePointer();
                        pageModel.onEndCircleMoved(null);
                    }
                });
            });
        }

        private startCreatingValuesScenario(): void {
            var pageModel = this.pageModel();
            var halfWidth = TutorialPageViewModel.width() / 2;
            var quarterWidth = halfWidth / 2;

            this.highlightRed(0);

            pageModel.onSegmentCreated(segY => {
                pageModel.onSegmentCreated(null);

                m.startComputation();

                pageModel.pointer(
                    new SVGPointer(quarterWidth, segY, 1)
                        .tap(1500)
                        .infinite()
                );

                m.endComputation();

                pageModel.onCircleCreated((x, y) => {
                    if (y === segY) {
                        pageModel.onCircleCreated(null);

                        m.endComputation();

                        this.highlightRed(1);

                        pageModel.pointer(
                            new SVGPointer(x, y, 2)
                                .press(750)
                                .moveBy(2000, (x > halfWidth) ? -quarterWidth : quarterWidth, 0)
                                .release(750)
                                .infinite()
                        );

                        m.endComputation();
                    }
                });

                pageModel.onSegmentCleanUp(y => {
                    if (y === segY) {
                        this.removePointer();
                        pageModel.onCircleMoved(null);
                        pageModel.onCircleCreated(null);
                    }
                });
            });

            pageModel.onCircleMoved(_.debounce(() => {
                pageModel.onCircleMoved(null);
                this.removePointer();
            }, TutorialPageViewModel.debounceInterval));

            pageModel.createTestLine(0);
        }

        private startEditingAndDeletingValuesScenario(): void {
            var pageModel = this.pageModel();
            var circleX = TutorialPageViewModel.width() / 2;

            this.highlightRed(0);

            pageModel.onInputMethodSelected(null);

            pageModel.onSegmentCreated((circleY: number) => {
                pageModel.onSegmentCreated(null);

                m.startComputation();

                pageModel.pointer(
                    new SVGPointer(circleX, circleY, 1)
                        .tapAndHold(1500, 2500)
                        .infinite()
                );

                m.endComputation();

                pageModel.onSegmentCleanUp(this.removePointer);
            });

            var line = pageModel.createTestLine(1);
            var circle = line.createTestCircle(circleX, '99');

            circle.onEditMenuCreated((x, y) => {
                circle.onEditMenuCreated(null);
                m.startComputation();

                this.highlights(['black', 'red', 'green', 'blue']);

                pageModel.pointer(
                    new SVGPointer(x, y, 2)
                        .press(750)
                        .moveBy(500, 0, -TutorialPageViewModel.height() / 6)
                        .release(750)
                        .infinite()
                );

                m.endComputation();

                circle.onEditMenuInteraction(() => {
                    pageModel.onCircleMoved(null);
                    this.removePointer();
                });
            });
        }

        private startChoosingMethodsScenario(): void {
            var pageModel = this.pageModel();
            var circleOffset = TutorialPageViewModel.width() / 8;

            var firstLine = pageModel.createTestLine(0);
            var secondLine = pageModel.createTestLine(1);
            var thirdLine = pageModel.createTestLine(2);

            pageModel.onResultCircleTapped(null);
            pageModel.onResultCircleDragged(null);

            firstLine.createTestCircle(circleOffset * 6, '1');
            secondLine.createTestCircle(circleOffset * 4, '2');
            thirdLine.createTestCircle(circleOffset * 5, '3');

            var selectCenter = pageModel.inputSelectCenter();

            m.startComputation();

            this.highlightRed(0);

            pageModel.pointer(
                new SVGPointer(selectCenter.x, selectCenter.y, 1)
                    .tap(1500)
                    .infinite()
            );

            m.endComputation();

            pageModel.onInputMethodSelected(() => {
                pageModel.onInputMethodSelected(null);

                this.currentTimerId = _.delay(() => {
                    var firstArgumentCenter = pageModel.inputMethodFirstArgumentCenter();

                    m.startComputation();

                    this.highlightRed(1);

                    pageModel.pointer(
                        new SVGPointer(firstArgumentCenter.x, firstArgumentCenter.y, 2)
                            .press(750)
                            .moveBy(500, 0, -TutorialPageViewModel.height() / 10)
                            .release(750)
                            .infinite()
                    );

                    m.endComputation();
                }, TutorialPageViewModel.inputUpdateDelay);

                pageModel.onInputParameterSelected(() => {
                    pageModel.onInputParameterSelected(null);

                    m.startComputation();

                    this.highlightRed();
                    pageModel.pointer(null);

                    m.endComputation();

                    this.currentTimerId = _.delay(() => {
                        var methodCenter = pageModel.inputMethodCenter();

                        m.startComputation();

                        pageModel.selectInputOption(TutorialPageViewModel.longMethodName);

                        this.highlightRed(2);

                        pageModel.pointer(
                            new SVGPointer(methodCenter.x, methodCenter.y, 3)
                                .press(750)
                                .moveBy(1000, -TutorialPageViewModel.width() / 4, 0)
                                .release(750)
                                .infinite()
                        );

                        m.endComputation();

                        pageModel.onInputMethodScrolled(() => {
                            pageModel.onInputMethodScrolled(null);

                            this.removePointer();
                        });
                    }, TutorialPageViewModel.reactionDelay);
                });

            });
        }

        private startInteractingWithResultScenario() {
            var pageModel = this.pageModel();
            var line = pageModel.createTestLine(0);
            var circleOffset = TutorialPageViewModel.width() / 38;
            var startX = line.x1();

            pageModel.onInputMethodSelected(null);

            for (var i = 0; i < 8; i += 1) {
                line.createTestCircle(i * circleOffset + startX, (i * 10) + '');
            }

            pageModel.onResultCircleTapped(() => {
                pageModel.onResultCircleTapped(null);

                m.startComputation();

                this.highlightRed();

                pageModel.pointer(null);

                m.endComputation();

                this.currentTimerId = _.delay(() => {
                    m.startComputation();

                    this.highlightRed(1);

                    line.endCircle().x(line.x1());

                    pageModel.selectInputOption(TutorialPageViewModel.capsuleResultMethodName);

                    pageModel.pointer(
                        new SVGPointer(startX, pageModel.resultLineY(), 2)
                            .press(750)
                            .moveBy(1000, 0, -TutorialPageViewModel.height() / 2)
                            .release(750)
                            .infinite()
                    );

                    m.endComputation();

                    pageModel.onResultCircleDragged(() => {
                        pageModel.onResultCircleDragged(null);

                        this.removePointer();
                    });

                }, TutorialPageViewModel.reactionDelay);
            });

            m.startComputation();

            this.highlightRed(0);

            pageModel.selectInputOption(TutorialPageViewModel.arrayResultMethodName);

            pageModel.pointer(
                new SVGPointer(startX + 2 * circleOffset, pageModel.resultLineY(), 1)
                    .tap(1500)
                    .infinite()
            );

            m.endComputation();

            pageModel.onSegmentCleanUp(this.removePointer);
        }

        private removePointer = () => {
            var pageModel = this.pageModel();

            m.startComputation();

            this.highlightRed();

            pageModel.pointer(null);

            m.endComputation();

            pageModel.onSegmentCleanUp(null);
        };

        private highlightRed(index?: number): void {
            this.highlights(this.highlights().map(() => 'black'));

            if (index !== undefined) {
                this.highlights()[index] = 'red';
            }
        }

        static standaloneValue(value: any, fill: string, x: number, y: number, offest?: number): CircleViewDef {
            var width = TutorialPageViewModel.width();
            var radius = width / 12;
            var fontSize = radius * 1.25;

            return {
                value: value,
                x: x,
                y: y,
                scale: 1,
                rotation: 0,
                key: +_.uniqueId(),
                isError: value === RxModel.errorValue(),
                radius: radius,
                fill: fill,
                stroke: 'black',
                capsuleOffset: offest === undefined ? radius : offest,
                fontSize: fontSize,
                textY: fontSize / 2.8,
                textFill: 'white',
                isTextStroked: true,
                config: () => {}
            };
        }

        static standaloneValueCaptionFontSize(def: CircleViewDef): number {
            return def.fontSize / 3;
        }

        static x: Property<number> = m.prop(screenWidth / 5);
        static y: Property<number> = m.prop(screenHeight / 4);
        static width: Property<number> = m.prop(screenWidth * 0.6);
        static height: Property<number> = m.prop(screenHeight * 0.6);

        private static debounceInterval = 100;
        private static inputUpdateDelay = 100;
        private static reactionDelay = 2000;
        private static longMethodName = 'combineLatest';
        private static arrayResultMethodName = 'buffer';
        private static capsuleResultMethodName = 'amb';

        menuWidth: Property<number> = m.prop(TutorialPageViewModel.width());
        selectedPage: Property<number>;
        isPrevPressed: Property<boolean> = m.prop(false);
        isNextPressed: Property<boolean> = m.prop(false);
        pageModel: Property<MainPageViewModel>;
        pageCount: Property<number> = m.prop(7);
        highlights: Property<string[]> = m.prop(['black', 'black', 'black', 'black']);

        private currentTimerId: number;
    }
}
