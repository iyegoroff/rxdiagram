/**
 * Created by iyegoroff on 27/04/15.
 */

///<reference path='../../../../lib/framework.d.ts' />
///<reference path='../../../views/BackButtonView.ts' />
///<reference path='../viewmodels/TutorialPageViewModel.ts' />
///<reference path='../../MainPage/views/MainPageView.ts' />
///<reference path='../../../components/SVGButtonWithText.ts' />


module RxDiagram {

    export class TutorialPageView {

        static draw(vm: TutorialPageViewModel): Mithril.VirtualElement[] {
            var isValueTypesPage = vm.isValueTypesPage();

            var ui = m('svg', {
                width: screenWidth,
                height: screenHeight,
                style: 'background-color: whitesmoke;',
                'class': 'noselect'
            }, [
                TutorialPageView.info(vm),
                BackButtonView.draw(vm.transitBack, 1),
                TutorialPageView.pageControl(vm),
                isValueTypesPage ? TutorialPageView.values() : null
            ]);

            return [
                m('div', {
                    key: vm.selectedPage(),
                }, isValueTypesPage ? null : MainPageView.draw(vm.pageModel())),
                ui
            ];
        }

        private static info(vm: TutorialPageViewModel): Mithril.VirtualElement {
            var text: Mithril.VirtualElement;

            var fontSize = screenHeight * 0.02;
            var padding = screenHeight * 0.01;
            var x = screenWidth * 0.2;
            var y = screenHeight * 0.05;
            var width = vm.menuWidth();
            var height = screenHeight * 0.15;
            var isWebkit = Framework.isWebkit();

            return m('foreignObject', {
                x: x,
                y: y,
                width: width,
                height: height
            }, Framework.Scroller.view('div', {
                xmlns: xmlns,
                style: 'top: ' + (isWebkit ? y : 0) + 'px;' +
                       'left: ' + (isWebkit ? x : 0) + 'px;' +
                       'height: ' + height + 'px;' +
                       'position: fixed;',
                options: {
                    scrollX: false,
                    bounce: false,
                    scrollbars: true
                },
                config: vm.infoConfig()
            }, m('div',
                m('div', {
                    style: 'font-size: ' + fontSize + 'px; ' +
                    'background-color: white; ' +
                    'text-align: justify; ' +
                    'padding-left: ' + padding + 'px; ' +
                    'padding-right: ' + (padding * 1.5) + 'px; ' +
                    'padding-bottom: ' + padding + 'px;'
                }, TutorialPageView.pageDescription(vm))
            )));
        }

        private static pageControl(vm: TutorialPageViewModel): Mithril.VirtualElement[] {
            var pageModel = vm.pageModel();

            var buttonTop = pageModel.y() + pageModel.height();
            var buttonY = (screenHeight - buttonTop) / 2 + buttonTop;
            var buttonX = screenHeight - buttonY;

            var circleOffset = pageModel.x();
            var circleStep = pageModel.width() / (vm.pageCount() + 1);

            return [
                TutorialPageView.pageControlButton(
                    buttonX,
                    buttonY,
                    -1,
                    vm.isPrevPressed,
                    vm.onPrevClicked
                ),
                _.times(
                    vm.pageCount(),
                    idx => TutorialPageView.pageControlCircle(
                        circleOffset + circleStep * (idx + 1),
                        buttonY,
                        idx === vm.selectedPage(),
                        idx * 10
                    )
                ),
                TutorialPageView.pageControlButton(
                    screenWidth - buttonX,
                    buttonY,
                    1,
                    vm.isNextPressed,
                    vm.onNextClicked
                )
            ];
        }

        private static pageControlCircle(x, y, isSelected, key): Mithril.VirtualElement {
            var radius = (screenHeight - y) * 0.2;

            return m('g', {
                key: key
            }, [
                m('circle', {
                    r: radius,
                    fill: '#777777',
                    cx: x,
                    cy: y
                }),
                isSelected
                    ? m('circle', {
                    r: radius * 0.85,
                    fill: 'whitesmoke',
                    cx: x,
                    cy: y
                })
                    : null
            ]);
        }

        private static pageControlButton(x, y, facing, isPressed: Property<boolean>, onClick): Mithril.VirtualElement {
            var side = screenHeight - y;
            var halfSize = side / 2;

            return SVGButton.view({
                x: x - halfSize,
                y: y - halfSize,
                width: side,
                height: side,
                fill: buttonColor,
                press: isPressed,
                onClick: onClick,
                key: facing + 3,
                children: [
                    TutorialPageView.arrow(x, y, side * facing)
                ]
            });
        }

        private static arrow(x, y, buttonSide): Mithril.VirtualElement {
            var offsetX = buttonSide / 4;
            var offsetY = buttonSide / 4;

            return m('polyline', {
                points: `${x - offsetX},${y - offsetY} ${x + offsetX},${y} ${x - offsetX},${y + offsetY}`,
                stroke: 'black'
            });
        }

        private static pageDescription(vm: TutorialPageViewModel): Mithril.VirtualElement[] {
            var highlights = vm.highlights();

            if (vm.isGeneralInfoPage()) {
                return [
                    m('h2', {
                        style: 'margin-top: 0%;'
                    }, l('1. General info.')),
                    m('a', l('Screen is divided into three parts: ')),
                    m('a', {
                        style: 'color: red;'
                    }, l('observables input area')),
                    m('a', ', '),
                    m('a', {
                        style: 'color: green;'
                    }, l('Rx.Observable method selection area')),
                    m('a', l(' and ')),
                    m('a', {
                        style: 'color: blue;'
                    }, l('result area')),
                    m('a',
                        l('. You can create up to three observables, add numeric (0..99) or error values, ') +
                        l('select methods and adjust some of their parameters.')
                    )
                ];
            }

            if (vm.isCreatingAndDeletingObservablesPage()) {
                return [
                    m('h2', {
                        style: 'margin-top: 0%;'
                    }, l('2. Creating and deleting observables.')),
                    m('a', {
                        style: `color: ${highlights[0]};`
                    }, l('To create an observable just tap in input area. ')),
                    m('a', l('Observables are marked as \'A\', \'B\' and \'C\'. ')),
                    m('a', {
                        style: `color: ${highlights[1]};`
                    }, l('Drag the right end of observable to change its length. ')),
                    m('a', {
                        style: `color: ${highlights[2]};`
                    }, l('To delete created observable - swipe near it horizontally.'))
                ];
            }

            if (vm.isCreatingValuesPage()) {
                return [
                    m('h2', {
                        style: 'margin-top: 0%;'
                    }, l('3. Creating values.')),
                    m('a', {
                        style: `color: ${highlights[0]};`
                    }, l('To create a value you need to tap on existing observable. ')),
                    m('a', l('Note that result area is also updated when you add values to \'A\' observable. ')),
                    m('a', {
                        style: `color: ${highlights[1]};`
                    }, l('Also you can drag values to change their positions.'))
                ];
            }

            if (vm.isEditingAndDeletingValuesPage()) {
                return [
                    m('h2', {
                        style: 'margin-top: 0%;'
                    }, l('4. Editing and deleting values.')),
                    m('a', {
                        style: `color: ${highlights[0]};`
                    }, l('To edit or delete some value - press on it and hold until small menu appears. ')),
                    m('a', l('Now it is possible ')),
                    m('a', {
                        style: `color: ${highlights[1]};`
                    }, l('to edit selected value by scrolling vertically the left panel')),
                    m('a', ', '),
                    m('a', {
                        style: `color: ${highlights[2]};`
                    }, l('delete the value by pressing the right \'X\'-button ')),
                    m('a', l('or ')),
                    m('a', {
                        style: `color: ${highlights[3]};`
                    }, l('close this menu by tapping outside of it.')),
                ];
            }

            if (vm.isValueTypesPage()) {
                return [
                    m('h2', {
                        style: 'margin-top: 0%;'
                    }, l('7. Value types.'))
                ];
            }

            if (vm.isChoosingMethodsPage()) {
                return [
                    m('h2', {
                        style: 'margin-top: 0%;'
                    }, l('5. Choosing methods.')),
                    m('a', {
                        style: `color: ${highlights[0]};`
                    },
                        l('You can choose one of the Rx.Observable class or instance methods ') +
                        l('with help of the drop down menu. ')
                    ),
                    m('a', {
                        style: `color: ${highlights[1]};`
                    }, l('Also it is possible to customize colored arguments by vertical scrolling. ')),
                    m('a', {
                        style: `color: ${highlights[2]};`
                    },
                        l('In case when the whole method invocation string is not visible ') +
                        l('you can scroll it horizontally.')
                    ),
                ];
            }

            if (vm.isInteractingWithResultPage()) {
                return [
                    m('h2', {
                        style: 'margin-top: 0%;'
                    }, l('6. Interacting with result.')),
                    m('a', {
                        style: `color: ${highlights[0]};`
                    }, l('If you tap on a partially covered value it will be moved to the front. ')),
                    m('a', {
                        style: `color: ${highlights[1]};`
                    }, l('When vertical \'capsule\' is not fully visible - just drag it up.')),
                ];
            }
        }

        private static values(): Mithril.VirtualElement[] {
            var x = TutorialPageViewModel.x();
            var y = TutorialPageViewModel.y() * 0.7;
            var width = TutorialPageViewModel.width();
            var height = TutorialPageViewModel.height();
            var firstRowY = (y + height / 4) * 0.8;
            var secondRowY = y + height / 4 * 3;
            var firstColX = x + width / 6;
            var secondColX = x + width / 2;
            var thirdColX = x + width / 6 * 5;
            var red = CircleViewModel.redFiller();
            var blue = CircleViewModel.blueFiller();
            var gray = CircleViewModel.grayFiller();
            var purple = CircleViewModel.purpleFiller();

            var groupFirstDef = TutorialPageViewModel.standaloneValue(10, red, thirdColX, secondRowY, 0);
            var groupCaptionSize = TutorialPageViewModel.standaloneValueCaptionFontSize(groupFirstDef);

            return [
                TutorialPageView.drawValue(99, 'Number', red, firstColX, firstRowY),
                TutorialPageView.drawValue(true, l('Boolean (T or F)'), blue, secondColX, firstRowY),
                TutorialPageView.drawValue(RxModel.errorValue(), 'Error', '', thirdColX, firstRowY),
                TutorialPageView.drawValue(undefined, 'undefined', gray, firstColX, secondRowY),
                TutorialPageView.drawValue([1, 2, 3], 'Array', purple, secondColX, secondRowY),

                ResultLineView.group([
                    groupFirstDef,
                    TutorialPageViewModel.standaloneValue(20, CircleViewModel.greenFiller(), thirdColX, secondRowY, 0)
                ], y),
                TutorialPageView.valueCaption(
                    l('Values with equal positions'),
                    groupCaptionSize,
                    thirdColX,
                    secondRowY + groupFirstDef.fontSize * 2.1
                )
            ];
        }

        private static drawValue(value: any, text: string, fill: string, x, y): Mithril.VirtualElement[] {
            var def = TutorialPageViewModel.standaloneValue(value, fill, x, y);

            return [
                CircleView.drawFromDef(def),
                TutorialPageView.valueCaption(
                    text,
                    TutorialPageViewModel.standaloneValueCaptionFontSize(def),
                    x,
                    y + def.radius * 1.5
                )
            ];
        }

        private static valueCaption(text: string, fontSize: number, x, y): Mithril.VirtualElement {
            return m('text', {
                fill: 'black',
                'text-anchor': 'middle',
                'font-size': fontSize,
                style: `${prefixedTransform}: ${Framework.matrix3d(x, y, 1, 1, 0)}`
            }, text);
        }
    }
}
