//Mithril type definitions for Typescript
declare module Mithril {
    export interface Static {

        (selector: string, attributes: Attributes, ...children: Array<string|VirtualElement>): VirtualElement;
        (selector: string, ...children: Array<string|VirtualElement>): VirtualElement;

        prop<T>(promise: Promise<T>) : PromiseProperty<T>;
        prop<T>(value: T): Property<T>;
        prop(): Property<Object>; // might be that this should be Property<any>

        withAttr(property: string, callback: (value: any) => void): (e: Event) => any;

        module<T extends Controller>(rootElement: Node, module: Module<T>): T;
        module<T extends Controller>(rootElement: Node): T;

        trust(html: string): string;

        render(rootElement: Element|HTMLDocument): void;
        render(rootElement: Element|HTMLDocument, children: VirtualElement, forceRecreation?: boolean): void;
        render(rootElement: Element|HTMLDocument, children: VirtualElement[], forceRecreation?: boolean): void;

        redraw: {
            (force?: boolean): void;
            strategy: Property<string>;
        }

        route: {
            <T extends Controller>(rootElement: HTMLDocument, defaultRoute: string, routes: Routes<T>): void;
            <T extends Controller>(rootElement: Element, defaultRoute: string, routes: Routes<T>): void;

            (element: Element, isInitialized: boolean): void;
            (path: string, params?: any, shouldReplaceHistory?: boolean): void;
            (): string;

            param(key: string): string;
            mode: string;
            buildQueryString(data: Object): String
            parseQueryString(data: String): Object
        }

        request<T>(options: XHROptions): Promise<T>;

        deferred: {
            onerror(e: Error): void;
            <T>(): Deferred<T>;
        }

        sync<T>(promises: Promise<T>[]): Promise<T[]>;

        startComputation(): void;
        endComputation(): void;

        // For test suite
        deps: {
            (mockWindow: Window): Window;
            factory: Object;
        }

    }

    export interface VirtualElement {
        key?: number;
        tag?: string;
        attrs?: Attributes;
        children?: any[];
    }

    // Configuration function for an element
    export interface ElementConfig {
        (element: Element, isInitialized: boolean, context?: any): void;
    }

    // Attributes on a virtual element
    export interface Attributes {
        title?: string;
        className?: string;
        class?: string;
        config?: ElementConfig;
    }

    // Defines the subset of Event that  needs
    export interface Event {
        currentTarget: Element;
    }

    export interface Controller {
        onunload?(evt: Event): any;
    }

    export interface ControllerFunction extends Controller {
        (): any;
    }

    export interface View<T extends Controller> {
        (ctrl: T): string|VirtualElement;
    }

    export interface Module<T extends Controller> {
        controller: ControllerFunction|{ new(): T };
        view: View<T>;
    }

    export interface Property<T> {
        (): T;
        (value: T): T;
        toJSON(): T;
    }

    export interface PromiseProperty<T> extends Promise<T> {
        (): T;
        (value: T): T;
        toJSON(): T;
    }

    export interface Routes<T extends Controller> {
        [key: string]: Module<T>;
    }

    export interface Deferred<T> {
        resolve(value?: T): void;
        reject(value?: any): void;
        promise: Promise<T>;
    }

    export interface SuccessCallback<T, U> {
        (value: T): U;
        (value: T): Promise<U>;
    }

    export interface ErrorCallback<U> {
        (value: Error): U;
        (value: string): U;
    }

    export interface Promise<T> {
        (): T;
        (value: T): T;
        then<U>(success: (value: T) => U): Promise<U>;
        then<U>(success: (value: T) => Promise<U>): Promise<U>;
        then<U,V>(success: (value: T) => U, error: (value: Error) => V): Promise<U>|Promise<V>;
        then<U,V>(success: (value: T) => Promise<U>, error: (value: Error) => V): Promise<U>|Promise<V>;
    }

    export interface XHROptions {
        method?: string;
        url: string;
        user?: string;
        password?: string;
        data?: any;
        background?: boolean;
        unwrapSuccess?(data: any): any;
        unwrapError?(data: any): any;
        serialize?(dataToSerialize: any): string;
        deserialize?(dataToDeserialize: string): any;
        extract?(xhr: XMLHttpRequest, options: XHROptions): string;
        type?(data: Object): void;
        config?(xhr: XMLHttpRequest, options: XHROptions): XMLHttpRequest;
        dataType?: string;
    }
}

declare var m: Mithril.Static;