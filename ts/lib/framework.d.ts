///<reference path="mithril.d.ts" />
///<reference path="iscroll.d.ts" />

declare module Framework {
    export interface ComponentAttrs<T> {
        options?: T
        config?: Function;
    }


    export class App {
        constructor(page: Page, onInit?: () => void);
        popBack(transitionOptions?: TransitionOptions, onFinish?: () => void);
        replacePage(page: Page, transitionOptions?: TransitionOptions, onFinish?: () => void);
        pushPage(page: Page, transitionOptions?: TransitionOptions, onFinish?: () => void);
        transitBack(transitionOptions?: TransitionOptions, onFinish?: () => void);
        transitToPage(page: Page, transitionOptions?: TransitionOptions, onFinish?: () => void);
    }

    export interface Page {
        (): Mithril.VirtualElement | Mithril.VirtualElement[];
    }

    export interface TransitionOptions {
        newPageTransition: Transition;
        oldPageTransition: Transition;
    }

    export class Transition {
        constructor(startStyle: Object, finishStyle: Object, duration: number, delay?: number, easing?: number);

        symmetric(): Transition;
        codirectional(): Transition;

        static flipX(duration: number, delay?: number, easing?: string): TransitionOptions;
        static flipY(duration: number, delay?: number, easing?: string): TransitionOptions;
        static slideFromRight(duration: number, delay?: number, easing?: string): TransitionOptions;
        static slideFromLeft(duration: number, delay?: number, easing?: string): TransitionOptions;
        static slideFromTop(duration: number, delay?: number, easing?: string): TransitionOptions;
        static slideFromBottom(duration: number, delay?: number, easing?: string): TransitionOptions;
        static moveFromRight(duration: number, delay?: number, easing?: string): TransitionOptions;
        static moveFromLeft(duration: number, delay?: number, easing?: string): TransitionOptions;
        static moveFromTop(duration: number, delay?: number, easing?: string): TransitionOptions;
        static moveFromBottom(duration: number, delay?: number, easing?: string): TransitionOptions;
    }

    export interface Property<T> {
        (value?: T): T;
    }

    export function isWebkit(): boolean;
    export function isGecko(): boolean;

    export function prop<T>(initial: T, onSet?: (value?: T) => void, onGet?: (value?: T) => void): Property<T>;

    export function matrix(x: number, y: number, scaleX: number, scaleY: number, rotation: number): string;
    export function matrix3d(x: number, y: number, scaleX: number, scaleY: number, rotationOverAxisZ: number): string;

    export class Scroller {
        static view(selector: string, attrs: ComponentAttrs<ScrollerOptions>, children?: any): Mithril.VirtualElement;

        static updateBounds(
            component: IScrollInstance,
            widthFun?: (x: HTMLElement) => number,
            heightFun?: (x: HTMLElement) => number
        );
    }

    export interface ScrollerOptions extends IScrollOptions {
        beforeScrollStart?: Function;
        scrollCancel?: Function;
        scrollStart?: Function;
        scrollEnd?: Function;
        flick?: Function;
        refresh?: Function;
        destroy?: Function;
    }

}

interface Property<T> extends Framework.Property<T> {}
