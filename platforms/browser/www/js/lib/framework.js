'use strict';

var Framework;
(function (Framework) {
    Framework.Component = (function () {

        function Component() {}

        Component.viewFun = function (comp, attrsMutator, childrenMutator) {
            attrsMutator = attrsMutator || function (attrs) {
                return attrs;
            };

            childrenMutator = childrenMutator || function (children) {
                return children;
            };

            return function (selector, attrs, children) {
                return m(
                    selector,
                    attrsMutator(_.omit(_.extend(attrs, { config: comp.config(attrs, attrs.config) }), 'options')),
                    childrenMutator(children)
                );
            };
        };

        Component.config = function (userConfig, element, isInitialized, context) {
            if (userConfig) {
                userConfig(element, isInitialized, context);
            }
        };

        return Component;
    })();
})(Framework || (Framework = {}));


var Framework;
(function (Framework) {
    Framework.App = (function () {
        
        function App(page, onInit) {
            this._pages = [];
            this._history = [];
            this._cacheFlags = [];

            var _this = this;

            var init = function () {
                if (onInit) {
                    onInit();
                }

                FastClick.attach(document.body);

                page.id = _this._createNewPageId();
                _this._pages.push(page);

                m.module(document.body, {
                    view: function () {
                        return _this._pages.map(function (p) {
                            return m('div.page', { key: p.id, config: p.animation }, p());
                        });
                    }
                });
            };

            document.addEventListener('deviceready', init, false);
        }

        App.prototype.popBack = function (transitionOptions, onFinish) {
            this._transitToPage(
                this._cacheFlags.pop() ? this._pages.splice(-2, 1)[0] : this._history.pop(),
                transitionOptions,
                onFinish
            );
        };

        App.prototype.replacePage = function (page, transitionOptions, onFinish) {
            this._transitToPage(page, transitionOptions, onFinish);
        };

        App.prototype.pushPage = function (page, transitionOptions, onFinish) {
            this._transitToPage(page, transitionOptions, onFinish, true);
        };

        App.prototype.transitBack = function (transitionOptions, onFinish) {
            this._transitToPage(
                this._cacheFlags.pop() ? this._pages.splice(-2, 1)[0] : this._history.pop(),
                transitionOptions,
                onFinish,
                false,
                false
            );
        };

        App.prototype.transitToPage = function (page, transitionOptions, onFinish) {
            this._transitToPage(page, transitionOptions, onFinish, false, false);
        };

        App.prototype._transitToPage = function (page, transitionOptions, onFinish, rememberPrevPage, removePrevPage) {
            if (typeof removePrevPage === 'undefined') removePrevPage = true;
            var pages = this._pages;
            var oldPage = pages[pages.length - 1];
            var newPage = page;
            var transition = transitionOptions && App._createTransition(
                transitionOptions.newPageTransition,
                transitionOptions.oldPageTransition
            );
            var _this = this;

            if (newPage.id === undefined) {
                newPage.id = this._createNewPageId();
            }

            pages.push(newPage);

            if (transition) {

                App._switchableEvents.forEach(function (event) {
                    window.addEventListener(event, App._disableEvent, true);
                });

                var body = document.body;
                body.className = body.className + ' pointer-disabled';

                oldPage.animation = transition.oldPageAnimation;
                newPage.animation = transition.newPageAnimation;

                newPage.animation.onComplete = function () {
                    m.startComputation();

                    _this._completeTransition(oldPage, onFinish, rememberPrevPage, removePrevPage);

                    App._switchableEvents.forEach(function (event) {
                        window.removeEventListener(event, App._disableEvent, true);
                    });

                    body.className = body.className.replace(' pointer-disabled', '');

                    newPage.animation = null;
                    oldPage.animation = null;

                    m.endComputation();
                };
            } else {
                _this._completeTransition(oldPage, onFinish, rememberPrevPage, removePrevPage);
            }
        };

        App.prototype._completeTransition = function (oldPage, onFinish, rememberPrevPage, removePrevPage) {
            if (removePrevPage) {
                this._removePage(oldPage, rememberPrevPage);

                if (rememberPrevPage) {
                    this._cacheFlags.push(false);
                }
            } else {
                this._cacheFlags.push(true);
            }

            if (typeof onFinish === 'function') {
                onFinish();
            }
        };
        
        App.prototype._removePage = function (page, shouldRemember) {
            var pages = this._pages;
            var pageIndex = pages.indexOf(page);

            if (pageIndex >= 0) {
                pages.splice(pageIndex, 1);
            }

            if (shouldRemember) {
                this._history.push(page);
            }
        };

        App.prototype._createNewPageId = function () {
            var newId = 0;
            var ids = this._pages.concat(this._history).map(function (p) { return p.id; });

            while (ids.indexOf(newId) !== -1) {
                newId += 1;
            }

            return newId;
        };

        App._disableEvent = function (event) {
            event.stopPropagation();
            event.preventDefault();
        };

        App._createTransition = function (newPageTransition, oldPageTransition) {
            return newPageTransition ? {
                newPageAnimation: App._createPageAnimation(newPageTransition),
                oldPageAnimation: App._createPageAnimation(oldPageTransition)
            } : null;
        };

        App._createPageAnimation = function (transition) {
            return transition.isIdle()
                ? function animation() {

                    _.delay(function () {
                        if (animation.onComplete) {
                            animation.onComplete();
                            animation.onComplete = null;
                        }
                    }, transition.idleDelay());
                }
                : function animation(element) {

                    App._extendStyle(element, transition.startStyle());
                    _.delay(App._extendStyle, 0, element, transition.finishStyle());

                    var event = Framework.isWebkit() ? 'webkitTransitionEnd' : 'transitionend';
                    element.addEventListener(event, function listener() {
                        element.removeEventListener(event, listener);

                        if (animation.onComplete) {
                            _.delay(function () {
                                animation.onComplete();
                                animation.onComplete = null;
                            }, App._animationCompleteDelay);
                        }

                    }, false);
                };
        };

        App._extendStyle = function (element, finishStyle) {
            var isWebkit = Framework.isWebkit();

            for (var property in finishStyle) {
                if (finishStyle.hasOwnProperty(property)) {
                    var match = property.match(isWebkit ? App._webkitRegex : App._geckoRegex);
                    var p = match
                        ? ((isWebkit ? 'webkit' : 'Moz') + match[1].toUpperCase() + match[2])
                        : property;

                    element.style[p] = finishStyle[property];
                }
            }
        };

        App._switchableEvents = ['click', 'touchstart', 'touchmove', 'touchend', 'touchcancel'];
        App._webkitRegex = /-webkit-(\w)(.*)/;
        App._geckoRegex = /-moz-(\w)(.*)/;
        App._animationCompleteDelay = 100;

        return App;
    })();
})(Framework || (Framework = {}));


var Framework;
(function (Framework) {

    Framework.Scroller = (function () {

        function Scroller() {}

        Scroller.extendCssClass = function (targetClass, classToAdd) {
            if (targetClass) {
                if (targetClass.indexOf(classToAdd) === -1) {
                    targetClass += ' ' + classToAdd;
                }
            } else {
                targetClass = classToAdd;
            }

            return targetClass;
        };

        Scroller.config = function (attrs, userConfig) {
            return function (element, isInitialized, context) {
                if (!isInitialized) {
                    var options = attrs.options || {};

                    context.component = new IScroll(element, options);

                    var scroll = context.component;

                    Scroller.addEventHandler(scroll, options, 'beforeScrollStart');
                    Scroller.addEventHandler(scroll, options, 'scrollCancel');
                    Scroller.addEventHandler(scroll, options, 'scrollStart');
                    Scroller.addEventHandler(scroll, options, 'scrollEnd');
                    Scroller.addEventHandler(scroll, options, 'flick');
                }

                Framework.Component.config(userConfig, element, isInitialized, context);

                if (!isInitialized) {
                    var userUnload = context.onunload;

                    context.onunload = function () {
                        context.component.destroy();
                        context.component = null;

                        if (userUnload) {
                            userUnload();
                        }
                    }
                }
            };
        };

        Scroller.addEventHandler = function (target, options, event) {
            target.on(event, function () {
                var handler = options[event];

                if (handler) {
                    m.startComputation();
                    handler(target);
                    m.endComputation();
                }
            });
        };

        Scroller.view = Framework.Component.viewFun(
            Scroller,

            function (attrs) {
                attrs.class = Scroller.extendCssClass(attrs.class, 'scroller-wrapper');

                return attrs;
            },

            function (children) {
                var addClass = function (cl) {
                    return Scroller.extendCssClass(cl, 'scroller-scroller');
                };

                if (children instanceof Array) {
                    children.forEach(function (child) {
                        child.attrs.class = addClass(child.attrs.class);
                    });
                } else if (children instanceof Object) {
                    children.attrs.class = addClass(children.attrs.class);
                }

                return children;
            }
        );

        Scroller.updateBounds = function (component, widthFun, heightFun) {
            var scrollerStyle = component.scroller.style;
            var children = component.scroller.children;
            var scrollerWidth = scrollerStyle.width;
            var scrollerHeight = scrollerStyle.height;
            var width = 0;
            var height = 0;
            var needsRefresh = false;

            if (widthFun) {
                width = _.sum(_.map(children, widthFun));
            }

            if (heightFun) {
                height = _.sum(_.map(children, heightFun));
            }

            if (widthFun && (Math.floor(+scrollerWidth.replace('px', '')) !== Math.floor(width))) {
                scrollerStyle.width = width + 'px';
                needsRefresh = true;
            }

            if (heightFun && (Math.floor(+scrollerHeight.replace('px', '')) !== Math.floor(height))) {
                scrollerStyle.height = height + 'px';
                needsRefresh = true;
            }

            if (needsRefresh) {
                component.refresh();
            }
        };

        return Scroller;
    })();
})(Framework || (Framework = {}));


var Framework;
(function (Framework) {
    Framework.Transition = (function () {

        function Transition(startStyle, finishStyle, duration, delay, easing) {
            this._startStyle = startStyle;
            this._finishStyle = finishStyle;
            this._duration = duration;
            this._easing = easing || 'linear';
            this._delay = delay || '0';
        }

        Transition.prototype.isIdle = function () {
            return _.isEmpty(this._startStyle) && _.isEmpty(this._finishStyle);
        };

        Transition.prototype.idleDelay = function () {
            return this.isIdle() ? this._duration : 0;
        };

        Transition.prototype.startStyle = function () {
            return this._startStyle;
        };

        Transition.prototype.finishStyle = function () {
            var transition = _.keys(this._finishStyle).join(' ') + ' '
                             + this._duration + 'ms '
                             + this._easing + ' '
                             + this._delay + 'ms';

            return _.extend(
                _.clone(this._finishStyle),
                Framework.isWebkit()
                    ? { 'webkitTransition': transition }
                    : { 'MozTransition': transition }
            );
        };

        Transition.prototype.symmetric = function () {
            return new Transition(
                this._finishStyle,
                this._startStyle,
                this._duration,
                this._delay,
                this._easing
            );
        };

        Transition.prototype.codirectional = function () {
            return new Transition(
                this._finishStyle,
                this.isIdle() ? {} : Transition._changeSigns(this._startStyle),
                this._duration,
                this._delay,
                this._easing
            );
        };

        Transition._changeSigns = function (style) {
            return _.mapValues(style, function (v) {
                var parts = v.match(/([^\(]+\()([^\)]+)/);

                return parts[1]
                    + parts[2]
                      .split(',')
                      .map(_.trim)
                      .map(function (x) {
                          return x[0] === '-'
                              ? x.slice(1)
                              : '-' + x;
                      })
                      .join(', ')
                    + ')';
            });
        };

        Transition.flipX = function (duration, delay, easing) {
            var isWebkit = Framework.isWebkit();

            var transition = new Transition(
                isWebkit
                    ? { '-webkit-transform': 'rotateX(180deg)' }
                    : { '-moz-transform': 'rotateX(180deg)' },
                isWebkit
                    ? { '-webkit-transform': 'rotateX(0)' }
                    : { '-moz-transform': 'rotateX(0)' },
                duration,
                delay,
                easing
            );

            return {
                newPageTransition: transition,
                oldPageTransition: transition.codirectional()
            };
        };

        Transition.flipY = function (duration, delay, easing) {
            var isWebkit = Framework.isWebkit();

            var transition = new Transition(
                isWebkit
                    ? { '-webkit-transform': 'rotateY(180deg)' }
                    : { '-moz-transform': 'rotateY(180deg)' },
                isWebkit
                    ? { '-webkit-transform': 'rotateY(0)' }
                    : { '-moz-transform': 'rotateY(0)' },
                duration,
                delay,
                easing
            );

            return {
                newPageTransition: transition,
                oldPageTransition: transition.codirectional()
            };
        };

        Transition.slideFromRight = function (duration, delay, easing) {
            var isWebkit = Framework.isWebkit();

            var transition = new Transition(
                isWebkit
                    ? { '-webkit-transform': 'translate3d(100%, 0, 0)' }
                    : { '-moz-transform': 'translate3d(100%, 0, 0)' },
                isWebkit
                    ? { '-webkit-transform': 'translate3d(0, 0, 0)' }
                    : { '-moz-transform': 'translate3d(0, 0, 0)' },
                duration,
                delay,
                easing
            );

            return {
                newPageTransition: transition,
                oldPageTransition: transition.codirectional()
            };
        };

        Transition.slideFromLeft = function (duration, delay, easing) {
            var isWebkit = Framework.isWebkit();

            var transition = new Transition(
                isWebkit
                    ? { '-webkit-transform': 'translate3d(-100%, 0, 0)' }
                    : { '-moz-transform': 'translate3d(-100%, 0, 0)' },
                isWebkit
                    ? { '-webkit-transform': 'translate3d(0, 0, 0)' }
                    : { '-moz-transform': 'translate3d(0, 0, 0)' },
                duration,
                delay,
                easing
            );

            return {
                newPageTransition: transition,
                oldPageTransition: transition.codirectional()
            };
        };

        Transition.slideFromTop = function (duration, delay, easing) {
            var isWebkit = Framework.isWebkit();

            var transition = new Transition(
                isWebkit
                    ? { '-webkit-transform': 'translate3d(0, -100%, 0)' }
                    : { '-moz-transform': 'translate3d(0, -100%, 0)' },
                isWebkit
                    ? { '-webkit-transform': 'translate3d(0, 0, 0)' }
                    : { '-moz-transform': 'translate3d(0, 0, 0)' },
                duration,
                delay,
                easing
            );

            return {
                newPageTransition: transition,
                oldPageTransition: transition.codirectional()
            };
        };

        Transition.slideFromBottom = function (duration, delay, easing) {
            var isWebkit = Framework.isWebkit();

            var transition = new Transition(
                isWebkit
                    ? { '-webkit-transform': 'translate3d(0, 100%, 0)' }
                    : { '-moz-transform': 'translate3d(0, 100%, 0)' },
                isWebkit
                    ? { '-webkit-transform': 'translate3d(0, 0, 0)' }
                    : { '-moz-transform': 'translate3d(0, 0, 0)' },
                duration,
                delay,
                easing
            );

            return {
                newPageTransition: transition,
                oldPageTransition: transition.codirectional()
            };
        };

        Transition.moveFromRight = function (duration, delay, easing) {
            var isWebkit = Framework.isWebkit();

            return {
                newPageTransition: new Transition(
                    isWebkit
                        ? { '-webkit-transform': 'translate3d(100%, 0, 0)' }
                        : { '-moz-transform': 'translate3d(100%, 0, 0)' },
                    isWebkit
                        ? { '-webkit-transform': 'translate3d(0, 0, 0)' }
                        : { '-moz-transform': 'translate3d(0, 0, 0)' },
                    duration,
                    delay,
                    easing
                ),
                oldPageTransition: new Transition({}, {}, duration + delay)
            };
        };

        Transition.moveFromLeft = function (duration, delay, easing) {
            var isWebkit = Framework.isWebkit();

            return {
                newPageTransition: new Transition(
                    isWebkit
                        ? { '-webkit-transform': 'translate3d(-100%, 0, 0)' }
                        : { '-moz-transform': 'translate3d(-100%, 0, 0)' },
                    isWebkit
                        ? { '-webkit-transform': 'translate3d(0, 0, 0)' }
                        : { '-moz-transform': 'translate3d(0, 0, 0)' },
                    duration,
                    delay,
                    easing
                ),
                oldPageTransition: new Transition({}, {}, duration + delay)
            };
        };

        Transition.moveFromTop = function (duration, delay, easing) {
            var isWebkit = Framework.isWebkit();

            return {
                newPageTransition: new Transition(
                    isWebkit
                        ? { '-webkit-transform': 'translate3d(0, -100%, 0)' }
                        : { '-moz-transform': 'translate3d(0, -100%, 0)' },
                    isWebkit
                        ? { '-webkit-transform': 'translate3d(0, 0, 0)' }
                        : { '-moz-transform': 'translate3d(0, 0, 0)' },
                    duration,
                    delay,
                    easing
                ),
                oldPageTransition: new Transition({}, {}, duration + delay)
            };
        };

        Transition.moveFromBottom = function (duration, delay, easing) {
            var isWebkit = Framework.isWebkit();
            return {
                newPageTransition: new Transition(
                    isWebkit
                        ? { '-webkit-transform': 'translate3d(0, 100%, 0)' }
                        : { '-moz-transform': 'translate3d(0, 100%, 0)' },
                    isWebkit
                        ? { '-webkit-transform': 'translate3d(0, 0, 0)' }
                        : { '-moz-transform': 'translate3d(0, 0, 0)' },
                    duration,
                    delay,
                    easing
                ),
                oldPageTransition: new Transition({}, {}, duration + delay)
            };
        };

        return Transition;
    })();
})(Framework || (Framework = {}));


var Framework;
(function (Framework) {
    Framework._engine = function () {
        return /webkit/i.test(navigator.userAgent) ? 'webkit' : 'gecko';
    };

    Framework._isWebkit = Framework._engine() === 'webkit';
    Framework._isGecko = Framework._engine() === 'gecko';

    Framework.isWebkit = function () {
        return Framework._isWebkit;
    };

    Framework.isGecko = function () {
        return Framework._isGecko;
    };

    Framework.prop = function (initial, onSet, onGet) {
        var value = initial;

        if (onSet !== undefined && onGet !== undefined) {
            return function (v) {
                if (v !== undefined) {
                    value = v;
                    onSet(value);
                } else {
                    onGet(value);
                }
                return value;
            };
        } else if (onSet !== undefined) {
            return function (v) {
                if (v !== undefined) {
                    value = v;
                    onSet(value);
                }
                return value;
            };
        } else if (onGet !== undefined) {
            return function (v) {
                if (v !== undefined) {
                    value = v;
                } else {
                    onGet(value);
                }
                return value;
            };
        } else {
            return function (v) {
                if (v !== undefined) {
                    value = v;
                }
                return value;
            };
        }
    };

    Framework.matrix = function (x, y, scaleX, scaleY, rotation) {
        var cosR = Math.cos(rotation);
        var sinR = Math.sin(rotation);

        return 'matrix('
            + scaleX * cosR + ','
            + scaleY * sinR + ','
            + scaleX * -sinR + ','
            + scaleY * cosR + ','
            + x + ','
            + y + ')';
    };

    Framework.matrix3d = function (x, y, scaleX, scaleY, rotationOverAxisZ) {
        var cosR = Math.cos(rotationOverAxisZ);
        var sinR = Math.sin(rotationOverAxisZ);

        return 'matrix3d('
            + scaleX * cosR + ','
            + scaleY * sinR + ',0,0,'
            + scaleX * -sinR + ','
            + scaleY * cosR + ',0,0,0,0,1,0,'
            + x + ','
            + y + ',0,1)';
    };

})(Framework || (Framework = {}));
