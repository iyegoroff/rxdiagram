'use strict';
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var RxDiagram;
(function (RxDiagram) {

    var GradientView = (function () {
        function GradientView() {
        }
        GradientView.draw = function (gradientId) {
            return m('defs', [
                m('linearGradient', {
                    id: gradientId,
                    x1: '0%',
                    y1: '100%',
                    x2: '0%',
                    y2: '0%',
                    spreadMethod: 'pad'
                }, [
                    m('stop', {
                        offset: '0%',
                        'stop-color': '#ffffff',
                        'stop-opacity': 1
                    }),
                    m('stop', {
                        offset: '100%',
                        'stop-color': '#dddddd',
                        'stop-opacity': 1
                    })
                ])
            ]);
        };
        return GradientView;
    })();
    RxDiagram.GradientView = GradientView;



    var Geometry = (function () {
        function Geometry() {
        }
        Geometry.degToRad = function (degrees) {
            return degrees * Math.PI / 180;
        };
        Geometry.radToDeg = function (radiens) {
            return radiens * 180 / Math.PI;
        };
        Geometry.distance = function (x1, y1, x2, y2) {
            return Math.sqrt(Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2));
        };
        Geometry.angle = function (x1, y1, x2, y2) {
            return (360 - Math.atan2(y2 - y1, x2 - x1) * 180 / Math.PI) % 360;
        };
        Geometry.isInsideRange = function (value, startRange, endRange) {
            return value >= startRange && value < endRange;
        };
        Geometry.isInsideRect = function (x, y, rectX, rectY, rectWidth, rectHeight) {
            return Geometry.isInsideRange(x, rectX, rectX + rectWidth) && Geometry.isInsideRange(y, rectY, rectY + rectHeight);
        };
        Geometry.pointOnCircle = function (cx, cy, radius, angle) {
            return { x: cx + radius * Math.cos(angle), y: cy + radius * Math.sin(angle) };
        };
        Geometry.arc = function (x, y, radius, startAngle, endAngle) {
            var start = Geometry.pointOnCircle(x, y, radius, endAngle);
            var end = Geometry.pointOnCircle(x, y, radius, startAngle);
            var arcSweep = endAngle - startAngle <= Math.PI ? "0" : "1";
            return [
                "M",
                start.x,
                start.y,
                "A",
                radius,
                radius,
                0,
                arcSweep,
                0,
                end.x,
                end.y
            ].join(" ");
        };
        Geometry.capsule = function (radius, offset) {
            var PI_2 = Math.PI / 2;
            return Geometry.arc(offset, 0, radius, -PI_2, PI_2) + 'L-' + offset + ',-' + radius + Geometry.arc(-offset, 0, radius, PI_2, -PI_2) + 'L' + offset + ',' + radius;
        };
        return Geometry;
    })();
    RxDiagram.Geometry = Geometry;



    RxDiagram.screenWidth = window.innerWidth;
    RxDiagram.screenHeight = window.innerHeight;
    RxDiagram.standardGradientId = 'gradient';
    RxDiagram.timeScale = 1000;
    RxDiagram.xmlns = 'http://www.w3.org/1999/xhtml';
    RxDiagram.rxVersion = '2.5.3';
    RxDiagram.buttonColor = 0xEBEBEB;
    RxDiagram.cssTransform = Framework.isWebkit() ? 'webkitTransform' : 'MozTransform';
    RxDiagram.cssTransition = Framework.isWebkit() ? 'webkitTransition' : 'MozTransition';
    RxDiagram.prefixedTransform = Framework.isWebkit() ? '-webkit-transform' : '-moz-transform';
    RxDiagram.prefixedTransition = Framework.isWebkit() ? '-webkit-transition' : '-moz-transition';
    RxDiagram.transitionEndEvent = Framework.isWebkit() ? 'webkitTransitionEnd' : 'transitionend';
    RxDiagram.uiCornerRoundingCoef = 0.01;



    var SVGButton = (function () {
        function SVGButton() {
        }
        SVGButton.view = function (def) {
            var x = def.x, y = def.y, width = def.width, height = def.height, radius = def.radius || 10, fill = def.fill || 0xF0F0F0, stroke = def.stroke || 2, isPressed = def.press(), onPointerStart = function () { return def.press(true); }, onPointerEnd = function () { return def.press(false); }, onClick = def.onClick, children = def.children, klass = def.class || '', style = def.style || '';
            var pi = Math.PI;
            if (stroke > radius) {
                stroke = radius;
            }
            var s = stroke / 2;
            var r = radius - s;
            var up1 = (fill & 0xFF0000) + 0x0A0000;
            var up2 = (fill & 0x00FF00) + 0x000A00;
            var up3 = (fill & 0x0000FF) + 0x00000A;
            var upStroke = SVGButton.colorNumberToString((up1 > 0xFF0000 ? 0xFF0000 : up1) + (up2 > 0x00FF00 ? 0x00FF00 : up2) + (up3 > 0x0000FF ? 0x0000FF : up3));
            var downStroke = SVGButton.colorNumberToString(((fill & 0xFF0000) / 2 & 0xFF0000) + ((fill & 0x00FF00) / 2 & 0x00FF00) + ((fill & 0x0000FF) / 2 & 0x0000FF));
            var f = SVGButton.colorNumberToString(fill);
            var centers = [
                x + radius,
                y + height - radius,
                x + width - radius,
                y + height - radius,
                x + width - radius,
                y + radius,
                x + radius,
                y + radius
            ];
            var elements = [
                m('rect', {
                    x: x,
                    y: y,
                    width: width,
                    height: height,
                    rx: radius,
                    ry: radius,
                    fill: f
                }),
                m('path', {
                    d: RxDiagram.Geometry.arc(centers[0], centers[1], r, 0.5 * pi, 0.75 * pi) + 'L' + centers[2] + ',' + (centers[3] + r) + RxDiagram.Geometry.arc(centers[2], centers[3], r, 0, 0.5 * pi) + 'L' + (centers[4] + r) + ',' + centers[5] + RxDiagram.Geometry.arc(centers[4], centers[5], r, -0.25 * pi, 0),
                    stroke: isPressed ? upStroke : downStroke,
                    fill: 'transparent',
                    'stroke-width': stroke
                }),
                m('path', {
                    d: RxDiagram.Geometry.arc(centers[4], centers[5], r, -0.5 * pi, -0.25 * pi) + 'L' + centers[6] + ',' + (centers[7] - r) + RxDiagram.Geometry.arc(centers[6], centers[7], r, -pi, -0.5 * pi) + 'L' + (centers[0] - r) + ',' + centers[1] + RxDiagram.Geometry.arc(centers[0], centers[1], r, 0.75 * pi, pi),
                    stroke: isPressed ? downStroke : upStroke,
                    fill: 'transparent',
                    'stroke-width': stroke
                })
            ];
            return m('g', {
                ontouchstart: onPointerStart,
                ontouchend: onPointerEnd,
                onmousedown: onPointerStart,
                onmouseup: onPointerEnd,
                onmouseout: isPressed ? onPointerEnd : null,
                'class': klass,
                style: style,
                config: SVGButton.config(onClick)
            }, children ? elements.concat(m('g', {
                style: ("" + RxDiagram.prefixedTransform + ": " + Framework.matrix3d(0, isPressed ? 2 : 0, 1, 1, 0) + ";") + 'pointer-events: none;'
            }, children)) : elements);
        };
        SVGButton.colorNumberToString = function (color) {
            var c = color.toString(16);
            return '#' + _.repeat('0', 6 - c.length) + c;
        };
        SVGButton.config = function (onClick) {
            return function (elem, init) {
                if (!init) {
                    rxia.tap(elem, {
                        time: 1000,
                        threshold: 50,
                        preventDefault: false
                    }).subscribe(function () {
                        m.startComputation();
                        onClick();
                        m.endComputation();
                    });
                }
            };
        };
        return SVGButton;
    })();
    RxDiagram.SVGButton = SVGButton;



    var SVGButtonWithText = (function () {
        function SVGButtonWithText() {
        }
        SVGButtonWithText.view = function (def) {
            var width = def.width, height = def.height, fontSize = def.fontSize, fontColor = def.fontColor || 0;
            var text = m('foreignObject', {
                width: width,
                height: height,
                x: def.x,
                y: def.y
            }, m('div', {
                xmlns: 'http://www.w3.org/1999/xhtml',
                style: 'display: table; height: ' + height + 'px; width: 100%; overflow: hidden; ' + 'color: ' + RxDiagram.SVGButton.colorNumberToString(fontColor) + ' ;'
            }, m('div', {
                style: 'display: table-cell; vertical-align: middle; text-align: center; font-size: ' + fontSize + 'px;'
            }, def.text)));
            if (def.children) {
                def.children.push(text);
            }
            else {
                def.children = [text];
            }
            return RxDiagram.SVGButton.view(def);
        };
        return SVGButtonWithText;
    })();
    RxDiagram.SVGButtonWithText = SVGButtonWithText;



    var RxModel = (function () {
        function RxModel() {
        }
        RxModel.errorValue = function () {
            return 'E';
        };
        RxModel.operations = function () {
            return Object.keys(RxModel.reactives).sort(function (x, y) { return x > y ? 1 : -1; });
        };
        RxModel.format = function (operation, obsNames, obsAmount) {
            return RxModel.reactives[operation].format(obsNames, obsAmount).filter(function (v) { return v.length; });
        };
        RxModel.computeWithColors = function (operation, inputs, onComplete) {
            RxModel.compute(RxModel.reactives[operation].call, inputs, function (result, isFinishedOnTime, endTime) {
                if (result.length && result[0].value && !_.isUndefined(result[0].value.interval)) {
                    result = result.map(function (x) { return ({ value: x.value.interval, timestamp: x.timestamp }); });
                }
                var shouldComputeColorSeparately = RxModel.reactives[operation].colorCall;
                var valueIsArray = result.length && _.isArray(result[0].value);
                if (valueIsArray) {
                    onComplete(result.map(function (arr) { return ({
                        timestamp: arr.timestamp,
                        value: {
                            value: _.isArray(arr.value) ? arr.value.map(function (v) { return v.value; }) : arr.value,
                            color: (_.isArray(arr.value) ? arr.value.reduce(function (acc, v) { return acc | v.color; }, 0) : RxModel.defaultColor) || RxModel.defaultColor
                        }
                    }); }), isFinishedOnTime, endTime);
                }
                else if (shouldComputeColorSeparately) {
                    RxModel.compute(RxModel.reactives[operation].colorCall, inputs.map(function (arr) {
                        if (!arr.length) {
                            return [];
                        }
                        var result = arr.filter(function (x) { return x.value ? x.value.value !== RxModel.errorValue() : true; });
                        result.endTime = arr.endTime;
                        return result;
                    }), function (colorResult) {
                        var combined = _.zipWith(result, colorResult.slice(0, result.length), function (num, col) {
                            var color = _.isUndefined(col && col.value) ? RxModel.defaultColor : col.value;
                            return {
                                value: {
                                    value: num.value,
                                    color: _.isUndefined(color.color) ? color : color.color
                                },
                                timestamp: num.timestamp
                            };
                        });
                        onComplete(combined, isFinishedOnTime, endTime);
                    });
                }
                else {
                    onComplete(result.map(function (o) { return _.isObject(o.value) ? o : _.extend(o, { value: { value: o.value, color: RxModel.defaultColor } }); }), isFinishedOnTime, endTime);
                }
            });
        };
        RxModel.compute = function (operation, inputs, onComplete) {
            var subscription = RxModel.subscription;
            if (subscription) {
                subscription.dispose();
            }
            var scheduler = new Rx.HistoricalScheduler();
            RxModel.virtualScheduler = scheduler;
            var args = inputs.map(function (input) { return (_.isString(input) || _.isNumber(input) || !_.isUndefined(input.color)) ? input : RxModel.createObservable(input, scheduler); });
            var result = [];
            var fixedEndTime = RxDiagram.timeScale + RxModel.timeScaleFix;
            var source = operation(args, scheduler).timestamp(scheduler);
            var finiteSource = source.takeUntilWithTime(fixedEndTime, scheduler);
            RxModel.subscription = finiteSource.finally(function () {
                var now = scheduler.now();
                var endTime = now > RxDiagram.timeScale ? fixedEndTime : now;
                onComplete(result.filter(function (x) { return x.timestamp <= RxDiagram.timeScale; }), endTime <= RxDiagram.timeScale, endTime);
            }).subscribe(function (x) { return result.push(x); }, function () { return result.push({ value: RxModel.errorValue(), timestamp: scheduler.now() }); });
            scheduler.start();
        };
        RxModel.mapFormatIndex = function (operation, idx) {
            return RxModel.reactives[operation].mapFormatIndex(idx);
        };
        RxModel.formatArguments = function (format) {
            return format.filter(function (x) { return typeof x !== 'string'; });
        };
        RxModel.createObservable = function (sequence, scheduler) {
            var endTime = sequence.endTime;
            var observable = Rx.Observable.merge(sequence.map(function (x) {
                var value = x.value, timestamp = x.timestamp;
                var delay = timestamp === endTime ? endTime - 1 : timestamp;
                return value.value === RxModel.errorValue() ? Rx.Observable.empty().delay(delay, scheduler).doOnCompleted(function () {
                    throw new Error();
                }) : Rx.Observable.just(value).delay(delay, scheduler);
            }));
            return observable.concat(Rx.Observable.never()).takeUntilWithTime(endTime, scheduler);
        };
        RxModel.staticMapFormatIndex = function () {
            return function (idx) { return (idx - 1) / 2; };
        };
        RxModel.prototypeMapFormatIndex = function () {
            return function (idx) { return idx / 2; };
        };
        RxModel.methodInfo = function (options) {
            var methodInfo = {
                call: RxModel.methodCall(options.method, options.params),
                format: RxModel.methodFormat(options.method, options.params),
                mapFormatIndex: RxModel.methodMapFormatIndex(options.params[0])
            };
            return options.colorMethod ? _.extend(methodInfo, { colorCall: RxModel.methodCall(options.colorMethod, options.colorParams) }) : methodInfo;
        };
        RxModel.methodCall = function (method, params) {
            return function (inputs, scheduler) {
                var inpIndex = 0;
                var args = params.reduce(function (result, param) {
                    if (param === RxModel.observablePlaceholder || _.isArray(param)) {
                        result.push(inputs[inpIndex++]);
                    }
                    else if (param === RxModel.hotObservablePlaceholder) {
                        var hot = inputs[inpIndex++].publish();
                        result.push(hot);
                        hot.connect();
                    }
                    else if (param === RxModel.colorlessObservablePlaceholder) {
                        result.push(inputs[inpIndex++].map(function (x) { return x.value; }));
                    }
                    else if (param === RxModel.schedulerPlaceholder) {
                        result.push(scheduler);
                    }
                    else if (param === RxModel.restObservablesPlaceholder) {
                        for (var len = inputs.length; inpIndex < len; inpIndex += 1) {
                            result.push(inputs[inpIndex]);
                        }
                        if (!len) {
                            result.push(Rx.Observable.empty());
                        }
                    }
                    else {
                        result.push(RxModel.isInvisible(param) ? param.invisible : param);
                    }
                    return result;
                }, []);
                return args[0][method].apply(args[0], args.slice(1));
            };
        };
        RxModel.methodFormat = function (method, params) {
            return function (argNames, argAmount) {
                var restAmount = argAmount;
                var format = params.reduce(function (result, param) {
                    var isObservableInstance = param === RxModel.observablePlaceholder || param == RxModel.hotObservablePlaceholder || param == RxModel.colorlessObservablePlaceholder;
                    if (param === Rx.Observable) {
                        result.push('Rx.Observable.' + method + '(');
                    }
                    else if (_.isFunction(param)) {
                        result[result.length - 1] += ((param + '').replace(/\{\s*?value:\s*([^,]*)[^\}]*\}/, '$1').replace(/\.value/g, '') + ',').replace(/(?:[,\s]*RxModel\.scheduler\(\))|(?:\"use strict\";\s)/g, '');
                    }
                    else if (isObservableInstance) {
                        result.push(argNames);
                        if (result.length > 1) {
                            result.push(',');
                        }
                        else {
                            result.push('.' + method + '(');
                        }
                        restAmount -= 1;
                    }
                    else if (param === RxModel.restObservablesPlaceholder) {
                        for (var i = 0; i < restAmount; i += 1) {
                            result.push(argNames);
                            result.push(',');
                        }
                    }
                    else if (param !== RxModel.schedulerPlaceholder && !RxModel.isInvisible(param)) {
                        result.push(_.isString(param) ? '"' + param + '"' : param);
                        result.push(',');
                    }
                    return result;
                }, []);
                var lastIndex = format.length - 1;
                format[lastIndex] = format[lastIndex].replace(/,$/, ')').replace(/\($/, '()');
                if (method === 'timeInterval') {
                    format[lastIndex] = format[lastIndex] + '.interval';
                }
                return format;
            };
        };
        RxModel.methodMapFormatIndex = function (thisParam) {
            return thisParam === Rx.Observable ? RxModel.staticMapFormatIndex() : RxModel.prototypeMapFormatIndex();
        };
        RxModel.scheduler = function () {
            return RxModel.virtualScheduler;
        };
        RxModel.createReactives = function (methods) {
            return Object.keys(methods).reduce(function (result, x) {
                var curMethodInfo = methods[x];
                curMethodInfo.method = x.replace(/ .*/, '');
                var colorMethod = curMethodInfo.colorMethod;
                var generateColors = colorMethod && colorMethod.generateColors;
                var returnColor = colorMethod && colorMethod.returnColor;
                if (generateColors !== undefined) {
                    curMethodInfo.colorMethod = 'take';
                    curMethodInfo.colorParams = [
                        Rx.Observable.generate(0xA0, function (x) { return true; }, function (x) { return (x === 0xA00000) ? 0xA0 : (x * 0x100); }, function (x) { return x; }, RxModel.scheduler()),
                        generateColors
                    ];
                }
                else if (returnColor !== undefined) {
                    curMethodInfo.colorMethod = 'return';
                    curMethodInfo.colorParams = [
                        Rx.Observable,
                        returnColor,
                        RxModel.schedulerPlaceholder
                    ];
                }
                else if (curMethodInfo.colorMethod === RxModel.sumOfColors) {
                    curMethodInfo.colorMethod = 'reduce';
                    curMethodInfo.colorParams = [
                        RxModel.observablePlaceholder,
                        (function (acc, x) { return acc | x.color; }),
                        [0]
                    ];
                }
                else if (curMethodInfo.colorMethod === RxModel.mapColors) {
                    curMethodInfo.colorMethod = 'map';
                    curMethodInfo.colorParams = [
                        RxModel.observablePlaceholder,
                        (function (x) { return x.color; })
                    ];
                }
                result[x] = RxModel.methodInfo(curMethodInfo);
                return result;
            }, {});
        };
        RxModel.isInvisible = function (param) {
            return !_.isUndefined(param.invisible);
        };
        RxModel.subscription = null;
        RxModel.virtualScheduler = null;
        RxModel.timeScaleFix = 50;
        RxModel.defaultColor = 0xB0B0B0;
        RxModel.observablePlaceholder = {};
        RxModel.colorlessObservablePlaceholder = {};
        RxModel.restObservablesPlaceholder = {};
        RxModel.schedulerPlaceholder = {};
        RxModel.hotObservablePlaceholder = {};
        RxModel.invisible = function (x) { return ({ invisible: x }); };
        RxModel.generateColors = function (n) { return ({ generateColors: n }); };
        RxModel.returnColor = function (c) {
            if (c === void 0) { c = RxModel.defaultColor; }
            return ({ returnColor: c });
        };
        RxModel.sumOfColors = {};
        RxModel.mapColors = {};
        RxModel.methods = {
            'amb *': {
                params: [
                    Rx.Observable,
                    RxModel.restObservablesPlaceholder
                ]
            },
            'concat *': {
                params: [
                    Rx.Observable,
                    RxModel.restObservablesPlaceholder
                ]
            },
            'merge': {
                params: [
                    Rx.Observable,
                    RxModel.restObservablesPlaceholder
                ]
            },
            'count': {
                params: [
                    RxModel.observablePlaceholder
                ],
                colorMethod: RxModel.sumOfColors
            },
            'average': {
                params: [
                    RxModel.observablePlaceholder,
                    RxModel.invisible(function (x) { return x.value; })
                ],
                colorMethod: RxModel.sumOfColors
            },
            'delay': {
                params: [
                    RxModel.observablePlaceholder,
                    [0, 100, 250, 500],
                    RxModel.schedulerPlaceholder
                ]
            },
            'debounce': {
                params: [
                    RxModel.observablePlaceholder,
                    [0, 100, 250, 500],
                    RxModel.schedulerPlaceholder
                ]
            },
            'distinct': {
                params: [
                    RxModel.observablePlaceholder,
                    RxModel.invisible(function (x) { return x.value; })
                ]
            },
            'distinctUntilChanged': {
                params: [
                    RxModel.observablePlaceholder,
                    RxModel.invisible(function (x) { return x.value; })
                ]
            },
            'filter': {
                params: [
                    RxModel.observablePlaceholder,
                    (function (x) { return x.value % 2; })
                ]
            },
            'map': {
                params: [
                    RxModel.observablePlaceholder,
                    (function (x) { return x.value / 2; })
                ],
                colorMethod: RxModel.mapColors
            },
            'max': {
                params: [
                    RxModel.observablePlaceholder,
                    RxModel.invisible(function (x, y) {
                        var xv = x.value, yv = y.value;
                        return (xv > yv) ? 1 : ((xv < yv) ? -1 : 0);
                    })
                ]
            },
            'min': {
                params: [
                    RxModel.observablePlaceholder,
                    RxModel.invisible(function (x, y) {
                        var xv = x.value, yv = y.value;
                        return (xv > yv) ? 1 : ((xv < yv) ? -1 : 0);
                    })
                ]
            },
            'skip': {
                params: [
                    RxModel.observablePlaceholder,
                    [0, 1, 2, 3]
                ]
            },
            'skipLast': {
                params: [
                    RxModel.observablePlaceholder,
                    [0, 1, 2, 3]
                ]
            },
            'take': {
                params: [
                    RxModel.observablePlaceholder,
                    [0, 1, 2, 3]
                ]
            },
            'takeLast': {
                params: [
                    RxModel.observablePlaceholder,
                    [0, 1, 2, 3]
                ]
            },
            'skipUntilWithTime': {
                params: [
                    RxModel.observablePlaceholder,
                    [0, 100, 250, 500],
                    RxModel.schedulerPlaceholder
                ]
            },
            'takeUntilWithTime': {
                params: [
                    RxModel.observablePlaceholder,
                    [0, 100, 250, 500],
                    RxModel.schedulerPlaceholder
                ]
            },
            'takeWhile': {
                params: [
                    RxModel.observablePlaceholder,
                    (function (x) { return x.value < 50; })
                ]
            },
            'delayWithSelector': {
                params: [
                    RxModel.observablePlaceholder,
                    (function (x) { return Rx.Observable.timer(x.value * 10, RxModel.scheduler()); })
                ]
            },
            'throttleFirst': {
                params: [
                    RxModel.observablePlaceholder,
                    [1, 100, 250, 500],
                    RxModel.schedulerPlaceholder
                ]
            },
            'findIndex': {
                params: [
                    RxModel.observablePlaceholder,
                    (function (x) { return x.value > 50; })
                ],
                colorMethod: 'find',
                colorParams: [
                    RxModel.observablePlaceholder,
                    (function (x) { return x.value > 50; })
                ]
            },
            'first': {
                params: [
                    RxModel.observablePlaceholder
                ]
            },
            'scan': {
                params: [
                    RxModel.observablePlaceholder,
                    RxModel.invisible(0),
                    (function (acc, x) { return acc + x.value; })
                ],
                colorMethod: 'scan',
                colorParams: [
                    RxModel.observablePlaceholder,
                    [0],
                    (function (acc, x) { return acc | x.color; })
                ]
            },
            'debounceWithSelector': {
                params: [
                    RxModel.observablePlaceholder,
                    (function (x) { return Rx.Observable.timer(x.value * 10, RxModel.scheduler()); })
                ]
            },
            'last': {
                params: [
                    RxModel.observablePlaceholder
                ]
            },
            'repeat': {
                params: [
                    RxModel.observablePlaceholder,
                    [0, 1, 2, 3]
                ]
            },
            'combineLatest': {
                params: [
                    Rx.Observable,
                    RxModel.restObservablesPlaceholder,
                    (function () { return Array.prototype.reduce.call(arguments, function (acc, x) { return acc + x.value; }, 0); })
                ],
                colorMethod: 'combineLatest',
                colorParams: [
                    Rx.Observable,
                    RxModel.restObservablesPlaceholder,
                    (function () { return Array.prototype.reduce.call(arguments, function (acc, x) { return acc | x.color; }, 0); })
                ]
            },
            'create': {
                params: [
                    Rx.Observable,
                    (function (o) {
                        o.onNext(24);
                        o.onCompleted();
                    })
                ],
                colorMethod: RxModel.returnColor()
            },
            'empty': {
                params: [
                    Rx.Observable,
                    RxModel.schedulerPlaceholder
                ]
            },
            'return': {
                params: [
                    Rx.Observable,
                    [0, 10, 25],
                    RxModel.schedulerPlaceholder
                ],
                colorMethod: RxModel.returnColor()
            },
            'interval': {
                params: [
                    Rx.Observable,
                    [50, 100, 250],
                    RxModel.schedulerPlaceholder
                ],
                colorMethod: RxModel.generateColors(20)
            },
            'never': {
                params: [
                    Rx.Observable
                ]
            },
            'repeat *': {
                params: [
                    Rx.Observable,
                    [0, 10, 25],
                    [0, 1, 2, 3],
                    RxModel.schedulerPlaceholder
                ],
                colorMethod: RxModel.generateColors(3)
            },
            'timer': {
                params: [
                    Rx.Observable,
                    [0, 100, 250, 500],
                    [50, 100, 250],
                    RxModel.schedulerPlaceholder
                ],
                colorMethod: RxModel.generateColors(21)
            },
            'amb': {
                params: [
                    RxModel.observablePlaceholder,
                    RxModel.observablePlaceholder
                ]
            },
            'includes': {
                params: [
                    RxModel.colorlessObservablePlaceholder,
                    [0, 10, 20, 30, 40, 50, 60, 70, 80, 90]
                ]
            },
            'concat': {
                params: [
                    RxModel.observablePlaceholder,
                    RxModel.restObservablesPlaceholder
                ]
            },
            'every': {
                params: [
                    RxModel.observablePlaceholder,
                    (function (x) { return x.value % 2 === 0; })
                ],
                colorMethod: RxModel.sumOfColors
            },
            'flatMap': {
                params: [
                    RxModel.observablePlaceholder,
                    (function (x) { return Rx.Observable.range(x.value, 2); })
                ],
                colorMethod: 'flatMap',
                colorParams: [
                    RxModel.observablePlaceholder,
                    (function (x) { return Rx.Observable.from([x.color, x.color]); })
                ]
            },
            'sum': {
                params: [
                    RxModel.observablePlaceholder,
                    function (x) { return x.value; }
                ],
                colorMethod: RxModel.sumOfColors
            },
            'takeUntil': {
                params: [
                    RxModel.observablePlaceholder,
                    RxModel.observablePlaceholder
                ]
            },
            'takeLastWithTime': {
                params: [
                    RxModel.observablePlaceholder,
                    [0, 100, 250, 500, 900],
                    RxModel.schedulerPlaceholder
                ]
            },
            'timeInterval': {
                params: [
                    RxModel.observablePlaceholder,
                    RxModel.schedulerPlaceholder
                ],
                colorMethod: RxModel.mapColors
            },
            'defaultIfEmpty': {
                params: [
                    RxModel.observablePlaceholder,
                    [10, 25, 50]
                ]
            },
            'throw': {
                params: [
                    Rx.Observable
                ]
            },
            'mergeDelayError': {
                params: [
                    Rx.Observable,
                    RxModel.restObservablesPlaceholder
                ]
            },
            'catch *': {
                params: [
                    Rx.Observable,
                    RxModel.restObservablesPlaceholder
                ]
            },
            'from': {
                params: [
                    Rx.Observable,
                    "hello"
                ]
            },
            'just': {
                params: [
                    Rx.Observable,
                    [0, 10, 25],
                    RxModel.schedulerPlaceholder
                ],
                colorMethod: RxModel.returnColor()
            },
            'of': {
                params: [
                    Rx.Observable,
                    [1, 2, 3],
                    [4, 5, 6],
                    [7, 8, 9]
                ]
            },
            'onErrorResumeNext': {
                params: [
                    Rx.Observable,
                    RxModel.restObservablesPlaceholder
                ]
            },
            'range': {
                params: [
                    Rx.Observable,
                    [0, 10, 25],
                    [0, 1, 2, 3],
                    RxModel.schedulerPlaceholder
                ],
                colorMethod: RxModel.generateColors(3)
            },
            'ignoreElements': {
                params: [
                    RxModel.observablePlaceholder
                ]
            },
            'sample': {
                params: [
                    RxModel.observablePlaceholder,
                    RxModel.observablePlaceholder,
                    RxModel.schedulerPlaceholder
                ]
            },
            'where': {
                params: [
                    RxModel.observablePlaceholder,
                    (function (x) { return x.value % 3; })
                ]
            },
            'catch': {
                params: [
                    RxModel.observablePlaceholder,
                    RxModel.observablePlaceholder
                ]
            },
            'elementAt': {
                params: [
                    RxModel.observablePlaceholder,
                    [0, 1, 2, 3]
                ]
            },
            'elementAtOrDefault': {
                params: [
                    RxModel.observablePlaceholder,
                    [0, 1, 2, 3],
                    [0, 99]
                ]
            },
            'expand': {
                params: [
                    RxModel.observablePlaceholder,
                    (function (x) { return Rx.Observable.just({ value: x.value + 5, color: x.color }).delay(200, RxModel.scheduler()); }),
                    RxModel.schedulerPlaceholder
                ]
            },
            'find': {
                params: [
                    RxModel.observablePlaceholder,
                    (function (x, i) { return x.value < 25 && i % 2; })
                ]
            },
            'firstOrDefault': {
                params: [
                    RxModel.observablePlaceholder,
                    (function (x) { return x.value % 2; }),
                    [1, 51, 99]
                ]
            },
            'selectMany': {
                params: [
                    RxModel.observablePlaceholder,
                    (function (x) { return Rx.Observable.interval((x.value + 1) * 2, RxModel.scheduler()); })
                ],
                colorMethod: 'selectMany',
                colorParams: [
                    RxModel.observablePlaceholder,
                    (function (x) { return Rx.Observable.interval((x.value + 1) * 2, RxModel.scheduler()).map(function () { return x.color; }); })
                ]
            },
            'forkJoin': {
                params: [
                    RxModel.observablePlaceholder,
                    RxModel.observablePlaceholder,
                    (function (x, y) { return x.value + y.value; })
                ],
                colorMethod: 'forkJoin',
                colorParams: [
                    RxModel.observablePlaceholder,
                    RxModel.observablePlaceholder,
                    (function (x, y) { return x.color | y.color; })
                ]
            },
            'lastOrDefault': {
                params: [
                    RxModel.observablePlaceholder,
                    (function (x) { return x.value % 2 === 0; }),
                    [0, 50, 100]
                ]
            },
            'isEmpty': {
                params: [
                    RxModel.observablePlaceholder
                ],
                colorMethod: RxModel.returnColor()
            },
            'indexOf': {
                params: [
                    RxModel.colorlessObservablePlaceholder,
                    [0, 10, 20, 30, 40, 50, 60, 70, 80, 90]
                ]
            },
            'select': {
                params: [
                    RxModel.observablePlaceholder,
                    (function (x, i) { return x.value * i; })
                ],
                colorMethod: RxModel.mapColors
            },
            'single': {
                params: [
                    RxModel.observablePlaceholder,
                    (function (x) { return x.value % 2; })
                ]
            },
            'singleOrDefault': {
                params: [
                    RxModel.observablePlaceholder,
                    (function (x) { return x.value % 2 === 0; }),
                    [0, 10, 20]
                ]
            },
            'forkJoin *': {
                params: [
                    Rx.Observable,
                    RxModel.restObservablesPlaceholder
                ]
            },
            'zipArray': {
                params: [
                    Rx.Observable,
                    RxModel.restObservablesPlaceholder
                ]
            },
            'zip *': {
                params: [
                    Rx.Observable,
                    RxModel.restObservablesPlaceholder,
                    (function () { return Array.prototype.reduce.call(arguments, function (acc, x) { return acc + x.value; }, 0); })
                ],
                colorMethod: 'zip',
                colorParams: [
                    Rx.Observable,
                    RxModel.restObservablesPlaceholder,
                    (function () { return Array.prototype.reduce.call(arguments, function (acc, x) { return acc | x.color; }, 0); })
                ]
            },
            'zip': {
                params: [
                    RxModel.observablePlaceholder,
                    RxModel.restObservablesPlaceholder,
                    (function () { return Array.prototype.reduce.call(arguments, function (acc, x) { return acc + x.value; }, 0); })
                ],
                colorMethod: 'zip',
                colorParams: [
                    RxModel.observablePlaceholder,
                    RxModel.restObservablesPlaceholder,
                    (function () { return Array.prototype.reduce.call(arguments, function (acc, x) { return acc | x.color; }, 0); })
                ]
            },
            'buffer': {
                params: [
                    RxModel.observablePlaceholder,
                    (function () { return Rx.Observable.timer(125, RxModel.scheduler()); })
                ]
            },
            'bufferWithCount': {
                params: [
                    RxModel.observablePlaceholder,
                    [1, 2, 3]
                ]
            },
            'bufferWithTime': {
                params: [
                    RxModel.observablePlaceholder,
                    [100, 250, 500],
                    RxModel.schedulerPlaceholder
                ]
            },
            'bufferWithTimeOrCount': {
                params: [
                    RxModel.observablePlaceholder,
                    [100, 250, 500],
                    [1, 2, 3],
                    RxModel.schedulerPlaceholder
                ]
            },
            'maxBy': {
                params: [
                    RxModel.observablePlaceholder,
                    (function (x) { return x.value; })
                ]
            },
            'minBy': {
                params: [
                    RxModel.observablePlaceholder,
                    (function (x) { return x.value; })
                ]
            },
            'pairwise': {
                params: [
                    RxModel.observablePlaceholder
                ]
            },
            'reduce': {
                params: [
                    RxModel.observablePlaceholder,
                    (function (acc, x) { return acc + x.value; }),
                    RxModel.invisible(0)
                ],
                colorMethod: RxModel.sumOfColors
            },
            'sequenceEqual': {
                params: [
                    RxModel.colorlessObservablePlaceholder,
                    RxModel.colorlessObservablePlaceholder
                ]
            },
            'skipUntil': {
                params: [
                    RxModel.observablePlaceholder,
                    RxModel.observablePlaceholder
                ]
            },
            'skipLastWithTime': {
                params: [
                    RxModel.observablePlaceholder,
                    [100, 250, 500],
                    RxModel.schedulerPlaceholder
                ]
            },
            'skipWhile': {
                params: [
                    RxModel.observablePlaceholder,
                    (function (x) { return x.value > 50; })
                ]
            },
            'some': {
                params: [
                    RxModel.observablePlaceholder,
                    (function (x) { return x.value % 2 === 0; })
                ]
            },
            'startWith': {
                params: [
                    RxModel.observablePlaceholder,
                    RxModel.schedulerPlaceholder,
                    [1, 10, 25]
                ]
            },
            'takeLastBuffer': {
                params: [
                    RxModel.observablePlaceholder,
                    [0, 1, 2, 3]
                ]
            },
            'takeLastBufferWithTime': {
                params: [
                    RxModel.observablePlaceholder,
                    [0, 100, 250, 500],
                    RxModel.schedulerPlaceholder
                ]
            },
            'timeout': {
                params: [
                    RxModel.observablePlaceholder,
                    [100, 250, 500],
                    RxModel.observablePlaceholder,
                    RxModel.schedulerPlaceholder
                ]
            },
            'timeoutWithSelector': {
                params: [
                    RxModel.observablePlaceholder,
                    (function () { return Rx.Observable.timer(100, RxModel.scheduler()); })
                ]
            },
            'toArray': {
                params: [
                    RxModel.observablePlaceholder
                ]
            },
            'pausable': {
                params: [
                    RxModel.hotObservablePlaceholder,
                    RxModel.colorlessObservablePlaceholder
                ]
            },
            'pausableBuffered': {
                params: [
                    RxModel.hotObservablePlaceholder,
                    RxModel.colorlessObservablePlaceholder
                ]
            },
            'jortSort': {
                params: [
                    RxModel.colorlessObservablePlaceholder
                ]
            },
            'jortSortUntil': {
                params: [
                    RxModel.colorlessObservablePlaceholder,
                    RxModel.observablePlaceholder
                ]
            },
            'for': {
                params: [
                    Rx.Observable,
                    "RxJS rules!",
                    (function (x) { return Rx.Observable.just(x).delay(90, RxModel.scheduler()); })
                ],
                colorMethod: RxModel.generateColors(11)
            }
        };
        RxModel.reactives = RxModel.createReactives(RxModel.methods);
        return RxModel;
    })();
    RxDiagram.RxModel = RxModel;



    var SegmentViewModel = (function () {
        function SegmentViewModel(def) {
            this.index = m.prop(def.index);
            this.viewClass = def.viewClass;
            this.updateResult = def.updateResult;
            this.gradientId = m.prop(def.gradientId);
            this.mainViewHeight = m.prop(def.mainViewHeight);
            this.mainViewWidth = m.prop(def.mainViewWidth);
        }
        SegmentViewModel.prototype.createCircle = function (x, isAnimated, isInteractable, value) {
            return false;
        };
        SegmentViewModel.prototype.drawInsideGroup = function () {
            return false;
        };
        SegmentViewModel.prototype.drawView = function () {
            return this.viewClass.draw(this);
        };
        SegmentViewModel.prototype.isAnyCircleEdited = function () {
            return false;
        };
        SegmentViewModel.prototype.isAnyCircleDragged = function () {
            return false;
        };
        SegmentViewModel.prototype.getTimestamps = function () {
            return {};
        };
        SegmentViewModel.prototype.operation = function () {
            return '';
        };
        SegmentViewModel.prototype.args = function () {
            return [];
        };
        SegmentViewModel.prototype.name = function () {
            return '';
        };
        SegmentViewModel.prototype.areAllInputsValid = function () {
            return false;
        };
        SegmentViewModel.prototype.update = function () {
        };
        SegmentViewModel.prototype.cleanUp = function () {
        };
        SegmentViewModel.prototype.isActive = function () {
            return false;
        };
        return SegmentViewModel;
    })();
    RxDiagram.SegmentViewModel = SegmentViewModel;




    var LineViewModel = (function (_super) {
        __extends(LineViewModel, _super);
        function LineViewModel(def) {
            _super.call(this, def);
            this.lineCircles = [];
            this.leftLimit = def.leftLimit;
            this.rightLimit = def.rightLimit;
            this.onEndCircleMoved = def.onEndCircleMoved;
            this.onCircleMoved = def.onCircleMoved;
            this.onCircleTapped = def.onCircleTapped;
            this.onCircleDragged = def.onCircleDragged;
            this.x1 = m.prop(def.x1);
            this.x2 = m.prop(def.x2);
            this.y1 = m.prop(def.y1);
            this.y2 = m.prop(def.y2);
            this.key = m.prop(def.key);
            this.canEditCircles = def.canEditCircles;
            this.circleRadius = def.circleRadius;
            this.isFinite = m.prop(def.isFinite);
            this.endCircle(def.x2);
            this.nameCircle(def.name);
            this.mainViewX = def.mainViewX;
            this.mainViewY = def.mainViewY;
            if (def.bounds) {
                this.upperBorder = m.prop(def.bounds.y - def.bounds.height);
            }
        }
        LineViewModel.prototype.drawInsideGroup = function () {
            return true;
        };
        LineViewModel.prototype.circles = function () {
            return this.activeToTop(this.lineCircles);
        };
        LineViewModel.prototype.groups = function () {
            return this.activeToTop(this.circles().reduce(function (acc, circle) {
                var groupWithSameX = _.find(acc, function (group) { return group[0].x() === circle.x(); });
                if (acc.length && groupWithSameX) {
                    groupWithSameX.push(circle);
                }
                else {
                    acc.push([circle]);
                }
                return acc;
            }, []).map(function (g) { return g.some(function (c) { return !_.isNaN(+c.value()); }) ? _.sortBy(g, function (c) { return +c.fill() + c.value(); }) : g; }));
        };
        LineViewModel.prototype.activeToTop = function (circles) {
            var activeKey = RxDiagram.CircleViewModel.activeKey();
            var active = _.find(circles, function (cvm) {
                if (_.isArray(cvm)) {
                    return cvm.some(function (c) { return c.key() === activeKey; });
                }
                else {
                    return cvm.key() === activeKey;
                }
            });
            if (active) {
                if (_.isArray(active)) {
                    _.pull(circles, active).push(active);
                }
                else {
                    var activeGroup = circles.filter(function (c) { return c.x() === active.x(); });
                    activeGroup.forEach(function (g) { return _.pull(circles, g).push(g); });
                }
            }
            return circles;
        };
        LineViewModel.prototype.cleanUp = function () {
            this.line = null;
            this.lineCircles.forEach(function (x) { return x.cleanUp(); });
            if (this.lineEndCircle) {
                this.lineEndCircle.cleanUp();
            }
            if (this.lineNameCircle) {
                this.lineNameCircle.cleanUp();
            }
        };
        LineViewModel.prototype.endCircle = function (x) {
            if (arguments.length !== 0) {
                this.lineEndCircle = new RxDiagram.CircleViewModel({
                    parentLine: this,
                    x: x,
                    y: this.y2(),
                    radius: this.circleRadius,
                    key: +_.uniqueId(),
                    isPressable: false,
                    isInteractable: true,
                    isAnimated: false,
                    fill: 'transparent',
                    stroke: 'transparent',
                    onPositionChange: this.onEndCirclePositionChange(),
                    mainViewWidth: this.mainViewWidth(),
                    mainViewHeight: this.mainViewHeight(),
                    leftLimit: this.leftLimit,
                    rightLimit: this.rightLimit,
                    gradientId: this.gradientId()
                });
            }
            return this.lineEndCircle;
        };
        LineViewModel.prototype.removeEndCircle = function () {
            if (this.lineEndCircle) {
                this.lineEndCircle.cleanUp();
                this.lineEndCircle = null;
            }
        };
        LineViewModel.prototype.nameCircle = function (value) {
            if (arguments.length !== 0) {
                this.lineNameCircle = new RxDiagram.CircleViewModel({
                    parentLine: this,
                    x: this.x1() / 2,
                    y: this.y1(),
                    radius: this.circleRadius,
                    key: +_.uniqueId(),
                    isInteractable: false,
                    value: value,
                    fill: 'white',
                    textFill: 'black',
                    mainViewWidth: this.mainViewWidth(),
                    mainViewHeight: this.mainViewHeight(),
                    leftLimit: this.leftLimit,
                    rightLimit: this.rightLimit,
                    gradientId: this.gradientId()
                });
            }
            return this.lineNameCircle;
        };
        LineViewModel.prototype.name = function () {
            return this.lineNameCircle.value();
        };
        LineViewModel.prototype.createTestCircle = function (x, value) {
            var endCirlePos = this.endCircle().x();
            this.createCircle((x <= endCirlePos) ? x : endCirlePos, false, true, value);
            var circle = _.last(this.lineCircles);
            var cb = function () {
                if (circle.domWasCreated()) {
                    circle.x(circle.x() + 0.001);
                }
                else {
                    requestAnimationFrame(cb);
                }
            };
            cb();
            return circle;
        };
        LineViewModel.prototype.createCircle = function (x, isAnimated, isInteractable, value, color) {
            if (x <= this.endCircle().x()) {
                var fill;
                if (typeof color === 'number') {
                    var colorString = color.toString(16);
                    fill = '#' + _.repeat('0', 6 - colorString.length) + colorString;
                }
                else {
                    fill = color;
                }
                var circle = new RxDiagram.CircleViewModel({
                    parentLine: this,
                    x: x,
                    y: this.y1(),
                    radius: this.circleRadius,
                    key: +_.uniqueId(),
                    isInteractable: isInteractable,
                    isAnimated: isAnimated,
                    value: value,
                    onPositionChange: this.onCirclePositionChange(),
                    mainViewWidth: this.mainViewWidth(),
                    mainViewHeight: this.mainViewHeight(),
                    mainViewX: this.mainViewX,
                    mainViewY: this.mainViewY,
                    leftLimit: this.leftLimit,
                    rightLimit: this.rightLimit,
                    gradientId: this.gradientId(),
                    onCircleAnimationFinished: this.onCircleAnimationFinished(),
                    fill: fill,
                    onTapped: this.onCircleTapped,
                    onDragged: this.onCircleDragged
                });
                this.lineCircles.push(circle);
                return true;
            }
            return false;
        };
        LineViewModel.prototype.remove = function (circle) {
            _.pull(this.lineCircles, circle);
        };
        LineViewModel.prototype.isAnyCircleEdited = function () {
            return this.lineCircles.some(function (circle) { return circle.isEdited(); });
        };
        LineViewModel.prototype.isAnyCircleDragged = function () {
            return this.lineCircles.some(function (circle) { return circle.isDragged(); });
        };
        LineViewModel.prototype.getTimestamps = function () {
            var input = this.lineCircles.map(function (x) { return x.getTimestamp(); });
            input.endTime = this.endCircle().getTimestamp().timestamp;
            return input;
        };
        LineViewModel.prototype.config = function () {
            var _this = this;
            return function (element, isInitialized) {
                if (!isInitialized) {
                    _this.line = element;
                }
            };
        };
        LineViewModel.prototype.onCirclePositionChange = function () {
            var _this = this;
            return function (x) {
                var endCircle = _this.endCircle();
                if (x > endCircle.x()) {
                    endCircle.x(x);
                }
                var onCircleMoved = _this.onCircleMoved();
                if (onCircleMoved) {
                    onCircleMoved(x, _this.y2());
                }
            };
        };
        LineViewModel.prototype.onEndCirclePositionChange = function () {
            var _this = this;
            return function (x) {
                _this.lineCircles.forEach(function (circle) {
                    if (circle.x() > x) {
                        circle.x(x);
                    }
                });
                _this.line.setAttribute('x2', _this.x2(x) + '');
                var onEndMoved = _this.onEndCircleMoved();
                if (onEndMoved) {
                    onEndMoved(_this.x2(), _this.y1());
                }
            };
        };
        LineViewModel.prototype.onCircleAnimationFinished = function () {
            var _this = this;
            return function () {
                _this.updateResult(_this.index(), true);
            };
        };
        LineViewModel.prototype.isActive = function () {
            return this.lineCircles.some(function (c) { return c.key() === RxDiagram.CircleViewModel.activeKey(); });
        };
        return LineViewModel;
    })(RxDiagram.SegmentViewModel);
    RxDiagram.LineViewModel = LineViewModel;



    var CircleViewModel = (function () {
        function CircleViewModel(def) {
            var _this = this;
            this.remove = function () {
                _this.parentLine.remove(_this);
                _this.updateResult();
                _this.cleanUp();
                var onEditMenuInteraction = _this.onEditMenuInteraction();
                if (onEditMenuInteraction) {
                    onEditMenuInteraction();
                }
            };
            this.update = function () {
                var cb = _this.onPositionChange();
                if (cb) {
                    cb(_this.x(), _this.y());
                }
                _this.updateResult(true);
                _this.updateTransform();
            };
            this.updateResult = function (isDelayed) {
                var parent = _this.parentLine;
                parent.updateResult(parent.index(), isDelayed);
            };
            this.updateGroupTransform = function (y) {
                _this.groupElement.setAttribute('style', CircleViewModel.circleStyle(0, y, 1, 0));
            };
            this.updateTransform = function () {
                _this.element.setAttribute('style', CircleViewModel.circleStyle(_this.x(), _this.y(), _this.scale(), _this.rotation()));
            };
            this.updateDragging = function (isDragging) {
                if (isDragging) {
                    _this.makeActive();
                }
            };
            this.updateEditing = function (isEditing) {
                if (isEditing) {
                    _this.makeActive();
                }
                else {
                }
                var onEditMenuInteraction = _this.onEditMenuInteraction();
                if (onEditMenuInteraction) {
                    onEditMenuInteraction();
                }
            };
            this.finishScroll = function () {
                return function (scroller) {
                    var curPage = scroller.currentPage.pageY;
                    _this.value((curPage < CircleViewModel.maxValue()) ? curPage : RxDiagram.RxModel.errorValue());
                    var onEditMenuInteraction = _this.onEditMenuInteraction();
                    if (onEditMenuInteraction) {
                        onEditMenuInteraction();
                    }
                };
            };
            this.editConfig = function () {
                return function (elem, isInitialized, context) {
                    if (!isInitialized) {
                        var val = _this.value();
                        context.component.goToPage(0, val === RxDiagram.RxModel.errorValue() ? CircleViewModel.maxValue() + 1 : +val, 0);
                    }
                };
            };
            this.editCloseConfig = function () {
                return function (elem, init) {
                    if (!init) {
                        Rx.Observable.merge(rxia.tap(elem), rxia.press(elem), rxia.swipe(elem), rxia.pan(elem).move).take(1).subscribe(function () {
                            m.startComputation();
                            _this.isEdited(_this.isDeleteClicked());
                            m.endComputation();
                        });
                    }
                };
            };
            this.editAttributesForGecko = function (def) {
                var width = def.rectWidth, height = def.rectHeight;
                var isInsideDeleteButton = function (x, y) { return RxDiagram.Geometry.isInsideRect(x, y, def.buttonX, def.buttonY, width, height); };
                var x = _this.mainViewX;
                var y = _this.mainViewY;
                return {
                    onmousedown: function (ev) {
                        if (isInsideDeleteButton(ev.pageX - x, ev.pageY - y))
                            _this.isDeleteClicked(true);
                    },
                    onmouseup: function (ev) {
                        if (isInsideDeleteButton(ev.pageX - x, ev.pageY - y))
                            _this.isDeleteClicked(false);
                    },
                    onmousemove: function (ev) {
                        if (isInsideDeleteButton(ev.pageX - x, ev.pageY - y))
                            _this.isDeleteClicked(false);
                    },
                    onclick: function (ev) {
                        var ex = ev.pageX - x, ey = ev.pageY - y;
                        if (isInsideDeleteButton(ex, ey)) {
                            _this.remove();
                        }
                        else if (!RxDiagram.Geometry.isInsideRect(ex, ey, def.scrollX, def.scrollY, width, height)) {
                            _this.isEdited(_this.isDeleteClicked());
                        }
                    }
                };
            };
            this.groupConfig = function (maxDistance) {
                return function (element, isInitialized) {
                    _this.config()(element, isInitialized);
                    _this.startGroupY = 0;
                    if (!isInitialized) {
                        _this.initDragGroup(element);
                        _this.groupElement = element;
                        _this.maxDragGroupDistance = maxDistance;
                    }
                };
            };
            this.config = function () {
                return function (element, isInitialized) {
                    if (!isInitialized) {
                        _this.initTap(element);
                        if (_this.isInteractable()) {
                            _this.initDrag(element);
                            if (_this.isPressable) {
                                _this.initPress(element);
                            }
                        }
                        _this.element = element;
                    }
                };
            };
            this.onTap = function (ev) {
                _this.makeActive();
                var onTapped = _this.onTapped && _this.onTapped();
                if (onTapped) {
                    onTapped(ev.centerX, ev.centerY);
                }
            };
            this.onDragGroupStart = function (ev) {
                _this.startDragGroupY = ev.centerY - _this.startGroupY;
                _this.makeActive();
            };
            this.onDragGroupMove = function (ev) {
                var dist = _this.maxDragGroupDistance;
                var offset = rxia.yOf(ev) - _this.startDragGroupY;
                if (offset <= 0) {
                    _this.updateGroupTransform(Math.abs(offset) <= dist ? offset : -dist);
                }
            };
            this.onDragGroupEnd = function (ev) {
                var distance = Math.abs(_this.startDragGroupY - rxia.yOf(ev));
                var groupElement = _this.groupElement;
                groupElement.style[RxDiagram.cssTransform] = CircleViewModel.circleTransform(0, 0, 1, 0);
                groupElement.style[RxDiagram.cssTransition] = RxDiagram.prefixedTransform + ' ' + (distance / RxDiagram.screenHeight * 350) + 'ms linear';
                var onDragged = _this.onDragged && _this.onDragged();
                if (onDragged) {
                    onDragged();
                }
            };
            this.realX = function (x) {
                return (x < _this.leftLimit) ? _this.leftLimit : (x > _this.rightLimit) ? _this.rightLimit : x;
            };
            this.onDragStart = function (ev) {
                _this.startDragX = ev.centerX;
                _this.startCircleX = _this.x();
                _this.isDragged(true);
            };
            this.onDragMove = function (ev) {
                if (!_this.isEdited()) {
                    var newX = _this.realX(rxia.xOf(ev) - _this.startDragX + _this.startCircleX);
                    if (_this.x() !== newX) {
                        _this.x(newX);
                    }
                }
            };
            this.onDragEnd = function () {
                _this.isDragged(false);
            };
            this.isDeleteClicked = m.prop(false);
            this.onEditMenuCreated = m.prop(null);
            this.onEditMenuInteraction = m.prop(null);
            this.onTapped = m.prop(null);
            this.onDragged = m.prop(null);
            this.parentLine = null;
            this.startDragX = 0;
            this.startCircleX = 0;
            this.startDragGroupY = 0;
            this.startGroupY = 0;
            this.element = null;
            this.groupElement = null;
            this.x = Framework.prop(def.x, this.update);
            this.y = m.prop(def.y);
            this.radius = m.prop(def.radius);
            this.key = m.prop(def.key);
            this.mainViewWidth = m.prop(def.mainViewWidth);
            this.mainViewHeight = m.prop(def.mainViewHeight);
            this.leftLimit = def.leftLimit;
            this.rightLimit = def.rightLimit;
            this.gradientId = m.prop(def.gradientId);
            this.value = Framework.prop(def.value === undefined ? (_.random(99) + '') : def.value, function () { return _this.updateResult(); });
            this.textFill = m.prop(def.textFill || 'white');
            this.fontSize = m.prop(def.radius * 1.25);
            this.textY = m.prop(this.fontSize() / 2.8);
            this.parentLine = def.parentLine;
            var colors = CircleViewModel.fillColors;
            this.fill = m.prop(def.fill || colors[_.random(colors.length - 1)]);
            this.editRectSide = m.prop(def.radius * 4.8);
            this.isInteractable = m.prop(def.isInteractable !== undefined ? def.isInteractable : true);
            this.isAnimated = m.prop(def.isAnimated !== undefined ? def.isAnimated : true);
            this.isPressable = def.isPressable !== undefined ? def.isPressable : true;
            this.scale = Framework.prop(CircleViewModel.endScale, this.updateTransform);
            this.rotation = Framework.prop(0, this.updateTransform);
            this.isEdited = Framework.prop(false, this.updateEditing);
            this.isDragged = Framework.prop(false, this.updateDragging);
            this.onPositionChange = m.prop(def.onPositionChange);
            this.stroke = m.prop(def.stroke || 'black');
            this.onCircleAnimationFinished = def.onCircleAnimationFinished;
            this.onTapped = def.onTapped;
            this.onDragged = def.onDragged;
            this.mainViewX = def.mainViewX;
            this.mainViewY = def.mainViewY;
            CircleViewModel.activeKey(def.key);
        }
        CircleViewModel.timeToX = function (time, leftLimit, rightLimit) {
            return Math.round(time * (rightLimit - leftLimit) / RxDiagram.timeScale + leftLimit);
        };
        CircleViewModel.xToTime = function (x, leftLimit, rightLimit) {
            return Math.round(RxDiagram.timeScale * (x - leftLimit) / (rightLimit - leftLimit));
        };
        CircleViewModel.maxValue = function () {
            return 100;
        };
        CircleViewModel.prototype.viewDef = function () {
            return {
                value: this.value(),
                x: this.x(),
                y: this.y(),
                scale: this.scale(),
                rotation: this.rotation(),
                key: this.key(),
                isError: this.isError(),
                radius: this.radius(),
                fill: this.fill(),
                stroke: this.stroke(),
                capsuleOffset: this.capsuleOffset(),
                fontSize: this.fontSize(),
                textY: this.textY(),
                textFill: this.textFill(),
                isTextStroked: this.mainViewWidth() === RxDiagram.screenWidth,
                mainViewWidth: this.mainViewWidth(),
                mainViewHeight: this.mainViewHeight(),
                gradientId: this.gradientId(),
                editRectSide: this.editRectSide(),
                isEdited: this.isEdited,
                isDeleteClicked: this.isDeleteClicked,
                remove: this.remove,
                editAttributesForGecko: this.editAttributesForGecko,
                finishScroll: this.finishScroll,
                editConfig: this.editConfig,
                config: this.config,
                groupConfig: this.groupConfig,
                editCloseConfig: this.editCloseConfig
            };
        };
        CircleViewModel.prototype.isError = function () {
            return this.value() === RxDiagram.RxModel.errorValue();
        };
        CircleViewModel.prototype.cleanUp = function () {
            this.element = null;
            this.groupElement = null;
        };
        CircleViewModel.prototype.canInteract = function () {
            return this.parentLine.canEditCircles();
        };
        CircleViewModel.prototype.getTimestamp = function () {
            return {
                value: {
                    value: this.isError() ? this.value() : +this.value(),
                    color: +this.fill().replace('#', '0x')
                },
                timestamp: CircleViewModel.xToTime(this.x(), this.leftLimit, this.rightLimit)
            };
        };
        CircleViewModel.circleStyle = function (x, y, scale, rotation) {
            return RxDiagram.prefixedTransform + ': ' + CircleViewModel.circleTransform(x, y, scale, rotation) + ';';
        };
        CircleViewModel.circleTransform = function (x, y, scale, rotation) {
            return Framework.matrix3d(x, y, scale, scale, rotation);
        };
        CircleViewModel.prototype.domWasCreated = function () {
            return this.element != undefined;
        };
        CircleViewModel.prototype.capsuleOffset = function () {
            var _this = this;
            var value = this.value();
            return _.isArray(value) ? value.reduce(function (acc, x) { return acc + (x + '').length * _this.radius() * 0.34; }, 0) : 0;
        };
        CircleViewModel.prototype.makeActive = function () {
            m.startComputation();
            CircleViewModel.activeKey(this.key());
            m.endComputation();
        };
        CircleViewModel.prototype.initTap = function (element) {
            rxia.tap(element).subscribe(this.onTap);
        };
        CircleViewModel.prototype.initDragGroup = function (element) {
            var panGroup = rxia.pan(element);
            panGroup.start.subscribe(this.onDragGroupStart);
            panGroup.move.subscribe(this.onDragGroupMove);
            panGroup.end.delay(100).subscribe(this.onDragGroupEnd);
        };
        CircleViewModel.prototype.initDrag = function (element) {
            var _this = this;
            var makeElementDraggable = function () {
                var pan = rxia.pan(element);
                var drag = pan.move.pausable();
                pan.start.subscribe(_this.onDragStart);
                pan.end.subscribe(_this.onDragEnd);
                drag.subscribe(_this.onDragMove);
                drag.resume();
                if (_this.onCircleAnimationFinished) {
                    _this.onCircleAnimationFinished();
                }
            };
            if (this.isAnimated()) {
                this.isAnimated(false);
                var style = element.style;
                var scaleDelay = CircleViewModel.scaleDelay;
                style[RxDiagram.cssTransform] = CircleViewModel.circleTransform(this.x(), this.y(), CircleViewModel.startScale, 0);
                _.delay(function () {
                    style[RxDiagram.cssTransform] = CircleViewModel.circleTransform(_this.x(), _this.y(), CircleViewModel.endScale, 0);
                    style[RxDiagram.cssTransition] = "" + RxDiagram.prefixedTransform + " " + scaleDelay + "ms linear";
                    Rx.Observable.fromEvent(element, RxDiagram.transitionEndEvent).timeout(scaleDelay * 2, Rx.Observable.just(null)).take(1).subscribe(function () {
                        makeElementDraggable();
                        style[RxDiagram.cssTransition] = '';
                    });
                }, 10);
            }
            else {
                makeElementDraggable();
            }
        };
        CircleViewModel.prototype.initPress = function (element) {
            var _this = this;
            var onPress = function (ev) {
                var target = (ev.target);
                var canEditCircle = target.tagName.toLowerCase() !== 'g' && _this.canInteract() && target.parentNode;
                if (canEditCircle) {
                    _this.isEdited(true);
                    var onEditMenuCreated = _this.onEditMenuCreated();
                    if (onEditMenuCreated) {
                        onEditMenuCreated(_this.x(), _this.y());
                    }
                }
            };
            var press = rxia.press(element).pausable();
            press.subscribe(onPress);
            press.resume();
        };
        CircleViewModel.redFiller = function () {
            return CircleViewModel.fillColors[0];
        };
        CircleViewModel.greenFiller = function () {
            return CircleViewModel.fillColors[1];
        };
        CircleViewModel.blueFiller = function () {
            return CircleViewModel.fillColors[2];
        };
        CircleViewModel.grayFiller = function () {
            return CircleViewModel.fillColors[0].substr(0, 3) + CircleViewModel.fillColors[1].substr(3, 2) + CircleViewModel.fillColors[2].substr(5);
        };
        CircleViewModel.purpleFiller = function () {
            return CircleViewModel.fillColors[0].substr(0, 5) + CircleViewModel.fillColors[2].substr(5);
        };
        CircleViewModel.activeKey = m.prop(0);
        CircleViewModel.fillColors = ['#B00000', '#00B000', '#0000B0'];
        CircleViewModel.scaleDelay = 125;
        CircleViewModel.startScale = 0.1;
        CircleViewModel.endScale = 1;
        return CircleViewModel;
    })();
    RxDiagram.CircleViewModel = CircleViewModel;



    var CircleEditView = (function () {
        function CircleEditView() {
        }
        CircleEditView.draw = function (vd) {
            var side = vd.editRectSide;
            var buttonX = side * 0.25;
            var buttonY = -side * 0.45;
            var buttonWidth = side * 0.45;
            var buttonHeight = side * 0.9;
            var offsetX = side * 0.05;
            var stroke = 1;
            var liHeight = buttonHeight - stroke * 2;
            var x = vd.x;
            var y = vd.y;
            var maxValue = RxDiagram.CircleViewModel.maxValue();
            var mainViewWidth = vd.mainViewWidth;
            var mainViewHeight = vd.mainViewHeight;
            var rxy = RxDiagram.uiCornerRoundingCoef * vd.mainViewWidth;
            var fontSize = buttonWidth * 0.5;
            var isGecko = Framework.isGecko();
            var positionOffset = side / 2 - y;
            positionOffset = (positionOffset < 0) ? 0 : (positionOffset + stroke * 2);
            var rect = m('rect', {
                x: -side * 0.25,
                y: -side * 0.5 + positionOffset,
                width: side * 1.05,
                height: side,
                rx: rxy,
                ry: rxy,
                stroke: '#000000',
                fill: 'url(#' + vd.gradientId + ')',
                'stroke-width': stroke
            });
            var cancelArea = m('rect', {
                x: -x,
                y: -y,
                width: mainViewWidth,
                height: mainViewHeight,
                'fill-opacity': 0,
                config: vd.editCloseConfig()
            });
            var selectRect = m('rect', {
                x: -buttonX + offsetX,
                y: buttonY + positionOffset,
                width: buttonWidth,
                height: buttonHeight,
                rx: rxy,
                ry: rxy,
                stroke: '#000000',
                fill: 'white',
                'stroke-width': 1
            });
            var button = RxDiagram.SVGButtonWithText.view({
                x: buttonX + offsetX,
                y: buttonY + positionOffset,
                width: buttonWidth,
                height: buttonHeight,
                radius: rxy,
                fill: 0xFF6666,
                press: vd.isDeleteClicked,
                onClick: vd.remove,
                text: 'X',
                fontSize: fontSize,
                fontColor: 0x7F3333
            });
            var valueSelectAttrs = {
                width: mainViewWidth,
                height: mainViewHeight,
                x: -x,
                y: -y + (isGecko ? 0 : positionOffset)
            };
            var scrollElemHeight = (buttonHeight * 0.5 + (isGecko ? 0 : positionOffset));
            var valueSelect = m('foreignObject', isGecko ? _.extend(valueSelectAttrs, vd.editAttributesForGecko({
                rectWidth: buttonWidth,
                rectHeight: buttonHeight,
                scrollX: x - buttonX + offsetX,
                scrollY: y + buttonY,
                buttonX: x + buttonX + offsetX,
                buttonY: y + buttonY
            })) : valueSelectAttrs, Framework.Scroller.view('div', {
                xmlns: RxDiagram.xmlns,
                style: 'top: ' + (buttonY + y + (isGecko ? positionOffset : 0)) + 'px; ' + 'left: ' + (-buttonX + x + offsetX + rxy) + 'px; ' + 'width: ' + (buttonWidth - rxy * 2) + 'px; ' + 'height: ' + (liHeight + stroke) + 'px;',
                config: vd.editConfig(),
                options: {
                    freeScroll: true,
                    scrollX: false,
                    snap: 'li',
                    scrollEnd: vd.finishScroll(),
                    scrollCancel: vd.finishScroll()
                }
            }, m('ul', {
                style: 'list-style: none; padding-left: 0; margin-top: ' + scrollElemHeight + 'px;'
            }, _.range(maxValue).concat('E').map(function (x) { return m('li', {
                style: 'height: ' + liHeight + 'px; padding-left: 50%;'
            }, m('div', {
                style: 'position: absolute; ' + RxDiagram.prefixedTransform + ': translate(-50%,-50%); font-size: ' + fontSize + 'px;'
            }, x)); }))));
            return [cancelArea, rect, valueSelect, selectRect, button];
        };
        return CircleEditView;
    })();
    RxDiagram.CircleEditView = CircleEditView;



    var CircleView = (function () {
        function CircleView() {
        }
        CircleView.draw = function (vm) {
            return vm.isEdited() ? CircleView.group(vm.viewDef(), CircleView.shape(vm.value()), CircleView.text, RxDiagram.CircleEditView.draw) : CircleView.drawFromDef(vm.viewDef());
        };
        CircleView.drawFromDef = function (vd) {
            return CircleView.group(vd, CircleView.shape(vd.value), CircleView.text);
        };
        CircleView.shape = function (value) {
            return _.isArray(value) ? CircleView.capsule : CircleView.circle;
        };
        CircleView.group = function (vd) {
            var elements = [];
            for (var _i = 1; _i < arguments.length; _i++) {
                elements[_i - 1] = arguments[_i];
            }
            return m('g', {
                style: RxDiagram.CircleViewModel.circleStyle(vd.x, vd.y, vd.scale, vd.rotation),
                key: vd.key,
                config: vd.config()
            }, elements.map(function (f) { return f(vd); }));
        };
        CircleView.circle = function (vd) {
            return vd.isError ? CircleView.errorCircle(vd) : m('circle', {
                r: vd.radius,
                fill: vd.fill,
                stroke: vd.stroke,
                style: 'stroke-width: 2;'
            });
        };
        CircleView.capsule = function (vd) {
            return m('path', {
                d: RxDiagram.Geometry.capsule(vd.radius, vd.capsuleOffset),
                fill: vd.fill,
                stroke: vd.stroke,
                style: 'stroke-width: 2;'
            });
        };
        CircleView.text = function (vd) {
            var fontSize = vd.fontSize;
            var textY = vd.textY;
            var textStroke = 1;
            var value = vd.value;
            var scale;
            if (_.isArray(value)) {
                value = '[' + value + ']';
                scale = CircleView.textScale[0];
                fontSize *= scale;
                textY *= scale;
                textStroke = 0.5;
            }
            else {
                value += '';
                value = +value ? +(+value).toFixed(2) : value.length > 1 ? value[0].toUpperCase() : value;
                var valueLength = (value + '').length;
                if (valueLength > 2) {
                    scale = CircleView.textScale[valueLength - 3];
                    fontSize *= scale;
                    textY *= scale;
                    textStroke = 0.5;
                }
            }
            var textElement = m('text', {
                style: RxDiagram.prefixedTransform + ': ' + Framework.matrix3d(0, textY, 1, 1, 0) + ';',
                'font-size': fontSize,
                'text-anchor': 'middle',
                fill: vd.textFill
            }, value);
            if (vd.isTextStroked) {
                textElement.attrs.stroke = 'black';
                textElement.attrs.style += 'stroke-width: ' + textStroke + ';';
            }
            return textElement;
        };
        CircleView.errorCircle = function (vd) {
            var radius = vd.radius;
            var innerRadius = radius * 0.9;
            var outerRadius = radius * 1.1;
            var step = Math.PI / 10;
            var offset = step / 2;
            var innerPoints = _.times(20, function (n) { return RxDiagram.Geometry.pointOnCircle(0, 0, innerRadius, step * n - offset); });
            var outerPoints = _.times(20, function (n) { return RxDiagram.Geometry.pointOnCircle(0, 0, outerRadius, step * n); });
            var points = _.flatten(_.zip(innerPoints, outerPoints)).concat(innerPoints[0]).reduce(function (str, p) { return str + p.x + ',' + p.y + ' '; }, '');
            return m('polyline', {
                points: points,
                stroke: 'black',
                style: 'stroke-width: 2;',
                fill: 'red'
            });
        };
        CircleView.textScale = [0.81, 0.66, 0.52];
        return CircleView;
    })();
    RxDiagram.CircleView = CircleView;



    var EndCircleView = (function () {
        function EndCircleView() {
        }
        EndCircleView.draw = function (vm) {
            return RxDiagram.CircleView.group(vm.viewDef(), RxDiagram.CircleView.circle, EndCircleView.line);
        };
        EndCircleView.line = function (vd) {
            var halfLength = vd.radius * 1.2;
            return m('line', {
                x1: 0,
                y1: halfLength,
                x2: 0,
                y2: -halfLength,
                stroke: 'black',
                style: 'stroke-width: 5; stroke-linecap: round;'
            });
        };
        return EndCircleView;
    })();
    RxDiagram.EndCircleView = EndCircleView;



    var LineView = (function () {
        function LineView() {
        }
        LineView.draw = function (vm) {
            var line = LineView.line(vm);
            var name = vm.nameCircle();
            var elements = name ? [RxDiagram.CircleView.draw(name), line] : [line];
            return LineView.appendEndCircle(elements, vm).concat(LineView.circles(vm));
        };
        LineView.appendEndCircle = function (elements, vm) {
            var end = vm.endCircle();
            if (end) {
                elements.push(vm.isFinite() ? RxDiagram.EndCircleView.draw(end) : LineView.arrow(end));
            }
            return elements;
        };
        LineView.circles = function (vm) {
            return vm.circles().map(function (cvm) { return RxDiagram.CircleView.draw(cvm); });
        };
        LineView.line = function (vm) {
            return m('line', {
                x1: vm.x1(),
                y1: vm.y1(),
                x2: vm.x2(),
                y2: vm.y2(),
                stroke: 'black',
                style: 'stroke-width: 5; stroke-linecap: round;',
                key: vm.key(),
                config: vm.config()
            });
        };
        LineView.arrow = function (vm) {
            var x = vm.x(), y = vm.y(), radius = vm.radius();
            return m('polyline', {
                points: x + ',' + (y - radius * 0.5) + ' ' + (x + radius * 1.5) + ',' + y + ' ' + x + ',' + (y + radius * 0.5),
                stroke: 'black'
            });
        };
        return LineView;
    })();
    RxDiagram.LineView = LineView;



    var ResultLineView = (function () {
        function ResultLineView() {
        }
        ResultLineView.draw = function (vm) {
            var groups = vm.groups().map(function (g) { return g.length > 1 ? ResultLineView.group(g.map(function (c) { return c.viewDef(); }), vm.upperBorder()) : RxDiagram.CircleView.draw(g[0]); });
            return RxDiagram.LineView.appendEndCircle([RxDiagram.LineView.line(vm)], vm).concat(groups);
        };
        ResultLineView.group = function (cvms, upperBorder) {
            var cvm = cvms[0];
            var r = cvm.radius;
            var x = cvm.x;
            var y = cvm.y;
            var scale = cvm.scale;
            var radius = 1.14 * r;
            var vertOffset = (cvms.length - 1) * 1.07 * r;
            var horizOffset = _.max(cvms, function (cvm) { return cvm.capsuleOffset; }).capsuleOffset;
            var rectWidth = (horizOffset + radius) * 2;
            var rectHeight = (vertOffset + radius) * 2;
            var borderOffset = (upperBorder - (y - vertOffset - radius));
            if (borderOffset > 0) {
                borderOffset += r * 0.2;
                y += borderOffset;
            }
            else {
                borderOffset = 0;
            }
            var shape = [
                horizOffset ? m('rect', {
                    x: -rectWidth / 2 + x,
                    y: -rectHeight / 2 + y,
                    width: rectWidth,
                    height: rectHeight,
                    rx: radius,
                    ry: radius,
                    fill: 'white',
                    stroke: 'black',
                    style: 'stroke-width: 2;'
                }) : m('path', {
                    d: RxDiagram.Geometry.capsule(radius, vertOffset),
                    fill: 'white',
                    stroke: 'black',
                    style: 'stroke-width: 2;',
                    transform: Framework.matrix(x, y, scale, scale, Math.PI / 2)
                })
            ];
            vertOffset += borderOffset;
            return m('g', {
                'class': 'noselect',
                key: cvm.key,
                config: borderOffset ? cvm.groupConfig(vertOffset) : cvm.config()
            }, shape.concat(cvms.map(function (cvm, idx) { return ResultLineView.circleWithOffset(cvm, vertOffset - 2.14 * idx * r); })));
        };
        ResultLineView.circleWithOffset = function (vd, yOffset) {
            var circle = RxDiagram.CircleView.drawFromDef(vd);
            var y = vd.y + yOffset;
            circle.attrs['style'] = RxDiagram.CircleViewModel.circleStyle(vd.x, y, vd.scale, vd.rotation);
            return circle;
        };
        return ResultLineView;
    })();
    RxDiagram.ResultLineView = ResultLineView;



    var InputViewModel = (function (_super) {
        __extends(InputViewModel, _super);
        function InputViewModel(def) {
            var _this = this;
            _super.call(this, def);
            this.onOperationSelected = function (opt) {
                _this.methodHasChanged = true;
                _this.update();
                _this.updateResult(_this.index());
                var onMethodSelected = _this.onMethodSelected();
                if (onMethodSelected && opt !== _this.prevSelectedOption()) {
                    onMethodSelected();
                }
            };
            this.longMethodScrollEnd = function () {
                var onLongMethodScrolled = _this.onLongMethodScrolled();
                if (onLongMethodScrolled) {
                    onLongMethodScrolled();
                }
            };
            this.prevSelectedOption = m.prop('amb');
            this.prevArgsLength = m.prop(0);
            this.firstMethodArgumentCenter = m.prop(null);
            this.onMethodSelected = m.prop(null);
            this.onParameterSelected = m.prop(null);
            this.onLongMethodScrolled = m.prop(null);
            this.inputIndexes = [];
            this.methodHasChanged = false;
            var x = def.x, y = def.y, width = def.width, height = def.height;
            this.x = m.prop(x);
            this.y = m.prop(y);
            this.width = m.prop(width);
            this.height = m.prop(height);
            this.selectY = m.prop(y + height * 0.25);
            var xGap = this.selectY() - y;
            this.selectX = m.prop(x + xGap);
            this.selectWidth = m.prop(width * 0.2);
            this.selectHeight = m.prop(height * 0.5);
            this.selectedOption = Framework.prop(InputViewModel.operations()[0], this.onOperationSelected);
            this.lineSegmentNames = def.lineSegmentNames;
            this.validLineSegmentNames = def.validLineSegmentNames;
            var formatScale = (RxDiagram.screenHeight > InputViewModel.greaterHeight) ? 1 : InputViewModel.formatScale(RxDiagram.screenHeight);
            var formatHeight = this.selectHeight() * formatScale;
            this.formatX = m.prop(this.selectX() + this.selectWidth() + xGap);
            this.formatY = m.prop(this.selectY() - (formatHeight - this.selectHeight()) / 2);
            this.formatWidth = m.prop(width - this.formatX());
            this.formatHeight = m.prop(formatHeight);
            this.formatFontSize = m.prop(this.formatHeight() / 2);
            this.formatScrollWidth = m.prop(xGap * 1.2 * formatScale);
            this.onMethodSelected = def.onMethodSelected;
            this.onParameterSelected = def.onParameterSelected;
            this.onLongMethodScrolled = def.onLongMethodScrolled;
        }
        InputViewModel.formatScale = function (actualHeight) {
            var scale = (InputViewModel.greaterHeight - actualHeight) / (InputViewModel.greaterHeight - InputViewModel.lesserHeight);
            return (InputViewModel.lesserHeightCoef - InputViewModel.greaterHeightCoef) * scale + InputViewModel.greaterHeightCoef;
        };
        InputViewModel.prototype.scrollWidthInSymbols = function (index) {
            return this.formatArguments().length ? _.max(this.formatArguments()[this.mapFormatIndex(index)].map(function (x) { return x + ''; }), 'length').length : 1;
        };
        InputViewModel.prototype.formatLength = function () {
            var _this = this;
            return this.format().reduce(function (acc, x, idx) { return acc + (typeof x === 'string' ? x.length : _this.scrollWidthInSymbols(idx)); }, 0);
        };
        InputViewModel.prototype.areAllInputsValid = function () {
            var _this = this;
            var args = this.args();
            return args.every(function (x) { return !_.isString(x); }) || args.filter(_.isString).some(function (x) { return _this.validLineSegmentNames().indexOf(x) !== -1; });
        };
        InputViewModel.prototype.args = function () {
            var _this = this;
            return this.inputIndexes.length ? this.formatArguments().map(function (x, idx) { return x[_this.inputIndexes[idx]]; }) : [];
        };
        InputViewModel.prototype.operation = function () {
            return this.selectedOption();
        };
        InputViewModel.prototype.cleanUp = function () {
            this.selectedOption(InputViewModel.operations()[0]);
            this.inputIndexes = this.inputIndexes.map(function () { return 0; });
        };
        InputViewModel.prototype.format = function () {
            return RxDiagram.RxModel.format(this.selectedOption(), this.lineSegmentNames, this.validLineSegmentNames().length);
        };
        InputViewModel.prototype.isPreviousFormatValueString = function (index) {
            return index && _.isString(this.format()[index - 1]);
        };
        InputViewModel.prototype.formatConfig = function () {
            var _this = this;
            return function (el, init, context) {
                var option = _this.selectedOption();
                var argsLength = _this.args().length;
                if (_this.prevSelectedOption() !== option || _this.prevArgsLength() !== argsLength) {
                    context.component.scrollTo(0, 0);
                    Framework.Scroller.updateBounds(context.component, function (x) {
                        return x.getBoundingClientRect().width - (parseFloat(x.style.paddingLeft) || 0);
                    });
                }
                _this.prevSelectedOption(option);
                _this.prevArgsLength(argsLength);
            };
        };
        InputViewModel.prototype.formatElementConfig = function (index) {
            var _this = this;
            return function (element, isInitialized, context) {
                var scrollerChildCount = element.firstChild.childNodes.length;
                var scroller = context.component;
                if (isInitialized && context.size !== scrollerChildCount) {
                    scroller.refresh();
                }
                context.size = scrollerChildCount;
                if (_this.methodHasChanged) {
                    var idx = _this.mapFormatIndex(index);
                    scroller.goToPage(0, _this.inputIndexes[idx] || 0, 0);
                    if (idx === (_this.inputIndexes.length - 1)) {
                        _this.methodHasChanged = false;
                    }
                }
                if (index <= 1) {
                    var rect = element.getBoundingClientRect();
                    _this.firstMethodArgumentCenter({
                        x: rect.left + rect.width / 2,
                        y: rect.top + rect.height / 2
                    });
                }
            };
        };
        InputViewModel.prototype.updateInput = function (index, selected) {
            this.inputIndexes[this.mapFormatIndex(index)] = selected;
            this.updateResult(this.index());
            var onParameterSelected = this.onParameterSelected();
            if (onParameterSelected) {
                onParameterSelected();
            }
        };
        InputViewModel.prototype.update = function () {
            var _this = this;
            var formatArguments = this.formatArguments();
            this.inputIndexes.length = formatArguments.length;
            this.inputIndexes = _.map(this.inputIndexes, function (x) { return x ? x : 0; });
            formatArguments.forEach(function (x, i) {
                if (_this.inputIndexes[i] >= x.length) {
                    _this.inputIndexes[i] = x.length - 1;
                }
            });
        };
        InputViewModel.prototype.isValidInput = function (input) {
            return (_.isNaN(+input) ? this.validLineSegmentNames().indexOf(input) !== -1 : true);
        };
        InputViewModel.prototype.selectCenter = function () {
            return {
                x: this.selectX() + this.selectWidth() / 2,
                y: this.selectY() + this.selectHeight() / 2
            };
        };
        InputViewModel.prototype.methodCenter = function () {
            return {
                x: this.formatX() + this.formatWidth() / 2,
                y: this.formatY() + this.formatHeight() / 2
            };
        };
        InputViewModel.prototype.mapFormatIndex = function (idx) {
            return RxDiagram.RxModel.mapFormatIndex(this.selectedOption(), idx);
        };
        InputViewModel.prototype.formatArguments = function () {
            return RxDiagram.RxModel.formatArguments(this.format());
        };
        InputViewModel.operations = function () {
            return RxDiagram.RxModel.operations();
        };
        InputViewModel.greaterHeight = 768;
        InputViewModel.lesserHeight = 553;
        InputViewModel.greaterHeightCoef = 1;
        InputViewModel.lesserHeightCoef = 1.5;
        return InputViewModel;
    })(RxDiagram.SegmentViewModel);
    RxDiagram.InputViewModel = InputViewModel;



    var InputView = (function () {
        function InputView() {
        }
        InputView.draw = function (vm) {
            var rxy = RxDiagram.uiCornerRoundingCoef * vm.mainViewWidth();
            var selectedOption = vm.selectedOption();
            var rect = m('rect', {
                x: vm.x(),
                y: vm.y(),
                width: vm.width(),
                height: vm.height(),
                rx: rxy,
                ry: rxy,
                stroke: '#cccccc',
                style: 'fill: url(#' + vm.gradientId() + '); stroke-width: 2;'
            });
            var select = m('foreignObject', {
                width: vm.selectWidth(),
                height: vm.selectHeight(),
                x: vm.selectX(),
                y: vm.selectY()
            }, m('select', {
                xmlns: RxDiagram.xmlns,
                'class': 'select-operation noselect',
                style: 'height: ' + vm.selectHeight() + 'px;',
                value: selectedOption,
                onchange: function () {
                    vm.selectedOption(this.options[this.selectedIndex].value);
                }
            }, RxDiagram.InputViewModel.operations().map(function (option) {
                return m('option', selectedOption === option ? { value: option, selected: true } : { value: option }, option);
            })));
            var fontSize = vm.formatFontSize();
            var formatTextHeight = fontSize * (cordova.platformId === 'android' ? 1.1 : 1);
            var formatHeight = vm.formatHeight();
            var format = m('foreignObject', {
                width: vm.formatWidth(),
                height: formatHeight,
                x: vm.formatX(),
                y: vm.formatY()
            }, m('div', {
                xmlns: RxDiagram.xmlns,
                style: 'font-size: ' + fontSize + 'px;',
                'class': 'noselect'
            }, Framework.Scroller.view('div', {
                style: 'position: static; -webkit-backface-visibility: visible;',
                options: {
                    freeScroll: true,
                    scrollY: false,
                    scrollX: true,
                    scrollEnd: vm.longMethodScrollEnd
                },
                config: vm.formatConfig()
            }, m('div', {
                style: 'overflow: hidden;' + 'white-space: nowrap;' + 'height: ' + formatHeight + 'px;' + 'width: 100%;' + 'position: relative;' + 'top: ' + ((formatHeight - fontSize) / 2) + 'px;'
            }, vm.format().map(InputView.formatElement(vm, fontSize, formatTextHeight))))));
            return [rect, select, format];
        };
        InputView.formatElement = function (vm, fontSize, height) {
            return function (value, index) {
                if (_.isString(value)) {
                    var padding = index && !vm.isPreviousFormatValueString(index) ? vm.formatScrollWidth() * InputView.scrollWidthMultipliers[vm.scrollWidthInSymbols(index - 1) - 1] : 0;
                    return m('span', {
                        style: 'padding-left: ' + padding + 'px;',
                        key: index
                    }, value);
                }
                else {
                    var scrollWidth = vm.formatScrollWidth() * InputView.scrollWidthMultipliers[vm.scrollWidthInSymbols(index) - 1];
                    return Framework.Scroller.view('span', {
                        style: 'top: 0%; width: ' + scrollWidth + 'px; height: ' + height + 'px;',
                        key: index,
                        options: {
                            freeScroll: true,
                            scrollX: false,
                            snap: true,
                            scrollEnd: function (scroller) { return vm.updateInput(index, scroller.currentPage.pageY); }
                        },
                        config: vm.formatElementConfig(index)
                    }, m('ul', {
                        style: 'list-style: none; padding-left: 0; margin-top: 0px;'
                    }, value.map(function (x) { return m('li', {
                        style: 'height: ' + height + 'px; padding-left: 50%;'
                    }, m('div', {
                        style: 'position: absolute; ' + RxDiagram.prefixedTransform + ': translate(-50%,0%); font-size: ' + fontSize + 'px; color: ' + (vm.isValidInput(x) ? 'green;' : 'red;')
                    }, x)); })));
                }
            };
        };
        InputView.scrollWidthMultipliers = [0.9, 1.3, 1.8];
        return InputView;
    })();
    RxDiagram.InputView = InputView;



    var SVGPointer = (function () {
        function SVGPointer(x, y, key) {
            var _this = this;
            this.config = function (elem, init) {
                if (!init) {
                    _this.updatePipeline(elem, 0);
                }
            };
            this.pipelineCallback = function (elem, idx) {
                _this.updatePipeline(elem, _this.restartPipeline ? 0 : (idx + 1));
                _this.restartPipeline = false;
            };
            this.scale = 1;
            this.pipeline = [];
            this.isInfinite = false;
            this.restartPipeline = false;
            this.transform(x, y);
            this.key = key;
        }
        SVGPointer.prototype.transform = function (x, y) {
            return this.moveTo(0, x, y);
        };
        SVGPointer.prototype.moveBy = function (duration, x, y) {
            return this.moveTo(duration, this.x + x, this.y + y);
        };
        SVGPointer.prototype.moveTo = function (duration, x, y) {
            var scale = this.scale;
            this.x = x;
            this.y = y;
            this.pipeline.push(SVGPointer.createStep(Framework.matrix3d(x, y, scale, scale, 0), "" + RxDiagram.prefixedTransform + " " + duration + "ms"));
            return this;
        };
        SVGPointer.prototype.tap = function (duration, scale) {
            if (scale === void 0) { scale = 0.5; }
            duration = duration / 2;
            return this.scaleTo(duration, scale).scaleTo(duration, 1);
        };
        SVGPointer.prototype.tapAndHold = function (tapDuration, holdDuration, scale) {
            if (scale === void 0) { scale = 0.5; }
            tapDuration = tapDuration / 2;
            return this.scaleTo(tapDuration, scale).wait(holdDuration).scaleTo(tapDuration, 1);
        };
        SVGPointer.prototype.press = function (duration, scale) {
            if (scale === void 0) { scale = 0.5; }
            return this.scaleTo(duration, scale);
        };
        SVGPointer.prototype.release = function (duration) {
            return this.scaleTo(duration, 1);
        };
        SVGPointer.prototype.scaleTo = function (duration, value) {
            this.scale = value;
            this.pipeline.push(SVGPointer.createStep(Framework.matrix3d(this.x, this.y, value, value, 0), "" + RxDiagram.prefixedTransform + " " + duration + "ms"));
            return this;
        };
        SVGPointer.prototype.wait = function (duration) {
            var scale = this.scale + 0.001;
            this.pipeline.push(SVGPointer.createStep(Framework.matrix3d(this.x, this.y, scale, scale, 0), "" + RxDiagram.prefixedTransform + " 0ms " + duration + "ms"));
            return this;
        };
        SVGPointer.prototype.clear = function () {
            this.pipeline = [];
            this.isInfinite = false;
            this.restartPipeline = true;
            return this;
        };
        SVGPointer.prototype.infinite = function () {
            this.isInfinite = true;
            return this;
        };
        SVGPointer.prototype.view = function (elem, key) {
            if (key === void 0) { key = 0; }
            return m('g', {
                style: 'pointer-events: none;',
                key: key
            }, m('g', {
                config: this.config,
                key: this.key
            }, elem));
        };
        SVGPointer.prototype.updatePipeline = function (elem, idx) {
            var _this = this;
            var style = elem.style;
            var pipeline = this.pipeline;
            var len = pipeline.length;
            if (this.isInfinite && idx >= len) {
                idx = 0;
            }
            if (idx < len) {
                var transition = pipeline[idx][RxDiagram.prefixedTransition];
                var callback = function () { return _this.pipelineCallback(elem, idx); };
                style[RxDiagram.cssTransform] = pipeline[idx][RxDiagram.prefixedTransform];
                style[RxDiagram.cssTransition] = transition;
                if (SVGPointer.immediateTransitionCheck.test(transition)) {
                    _.delay(callback, 0);
                }
                else {
                    Rx.Observable.fromEvent(elem, RxDiagram.transitionEndEvent).take(1).subscribe(callback);
                }
            }
        };
        SVGPointer.createStep = function (transform, transition) {
            var step = {};
            step[RxDiagram.prefixedTransform] = transform;
            step[RxDiagram.prefixedTransition] = transition;
            return step;
        };
        SVGPointer.immediateTransitionCheck = /^.*? 0ms$/;
        return SVGPointer;
    })();
    RxDiagram.SVGPointer = SVGPointer;



    var MainPageViewModel = (function () {
        function MainPageViewModel(x, y, width, height, options, maxSegments) {
            var _this = this;
            if (maxSegments === void 0) { maxSegments = 5; }
            this.lineSegments = function () {
                return _this.drawSegments.filter(function (seg) { return seg.index() !== _this.resultSegmentIndex && seg.index() !== _this.inputSegmentIndex; });
            };
            this.transitToTutorial = function () {
                if (!_this.isAboutButtonPressed()) {
                    _this.stopMenuTransition();
                    RxDiagram.app.transitToTutorial(_this.removeMenu);
                }
            };
            this.transitToAbout = function () {
                if (!_this.isTutorialButtonPressed()) {
                    _this.stopMenuTransition();
                    RxDiagram.app.transitToAbout(_this.removeMenu);
                }
            };
            this.removeMenu = function () {
                _this.isMenuShown(false);
                _this.menu(null);
            };
            this.cleanUp = function () {
                _this.drawSegments.map(function (seg) { return seg.cleanUp(); });
                _this.removeMenu();
            };
            this.updateResult = function (segIndex, isDelayed) {
                if (isDelayed === void 0) { isDelayed = false; }
                if (segIndex !== _this.resultSegmentIndex) {
                    (isDelayed ? _this.delayedUpdateEmitter : _this.instantUpdateEmitter).onNext(null);
                }
            };
            this.onUpdate = function () {
                _.delay(function () {
                    m.startComputation();
                    _this.clearSegmentWithIndex(_this.resultSegmentIndex);
                    if (_this.inputSegment().areAllInputsValid()) {
                        RxDiagram.RxModel.computeWithColors(_this.inputSegment().operation(), _this.getInputs(), function (result, isFinishedOnTime, endTime) {
                            var line = _this.createLine(_this.resultSegmentIndex, isFinishedOnTime, RxDiagram.CircleViewModel.timeToX(endTime, _this.leftLimit, _this.rightLimit), { x: _this.x(), y: _this.height(), width: _this.width(), height: _this.segmentHeight() * 2 });
                            line.endCircle().isInteractable(false);
                            result.forEach(function (x) { return line.createCircle(RxDiagram.CircleViewModel.timeToX(x.timestamp, _this.leftLimit, _this.rightLimit), false, false, x.value.value === undefined ? 'U' : x.value.value, x.value.color); });
                            if (_.find(result, function (x) { return (x.value.value + '') === RxDiagram.RxModel.errorValue(); })) {
                                line.removeEndCircle();
                            }
                            m.endComputation();
                        });
                    }
                    else {
                        m.endComputation();
                    }
                }, 0);
            };
            this.inputSelectCenter = m.prop(null);
            this.inputMethodCenter = m.prop(null);
            this.isUiPresent = m.prop(true);
            this.isHighlighted = m.prop(false);
            this.isMenuShown = m.prop(false);
            this.isTutorialButtonPressed = m.prop(false);
            this.isAboutButtonPressed = m.prop(false);
            this.pointer = m.prop(null);
            this.onSegmentCreated = m.prop(null);
            this.onSegmentCleanUp = m.prop(null);
            this.onEndCircleMoved = m.prop(null);
            this.onCircleMoved = m.prop(null);
            this.onCircleCreated = m.prop(null);
            this.onInputMethodSelected = m.prop(null);
            this.onInputParameterSelected = m.prop(null);
            this.onInputMethodScrolled = m.prop(null);
            this.onResultCircleTapped = m.prop(null);
            this.onResultCircleDragged = m.prop(null);
            this.drawSegments = [];
            this.delayedUpdateEmitter = new Rx.Subject();
            this.instantUpdateEmitter = new Rx.Subject();
            this.menu = m.prop(null);
            this.menuTransitionStart = m.prop(0);
            this.x = m.prop(x);
            this.y = m.prop(y);
            this.width = m.prop(width);
            this.height = m.prop(height);
            this.leftLimit = width * 0.1;
            this.rightLimit = width * 0.9;
            this.gradientId = m.prop(_.uniqueId(RxDiagram.standardGradientId));
            this.segmentHeight = m.prop(height / (maxSegments + 1));
            this.clearSwipeMinDistance = width * 0.4;
            this.resultSegmentIndex = maxSegments - 1;
            this.inputSegmentIndex = maxSegments - 2;
            this.circleRadius = width * 0.025;
            this.createInput(this.inputSegmentIndex);
            this.delayedUpdateEmitter.throttleWithTimeout(MainPageViewModel.computeDelay).subscribeOnNext(this.onUpdate);
            this.instantUpdateEmitter.subscribeOnNext(this.onUpdate);
            if (options) {
                this.isUiPresent(options.isUiPresent);
                this.isHighlighted(options.isHighlighted);
            }
        }
        MainPageViewModel.prototype.segments = function () {
            var active = _.find(this.drawSegments, function (seg) { return seg.isActive(); });
            if (active) {
                _.pull(this.drawSegments, active).push(active);
            }
            return this.drawSegments;
        };
        MainPageViewModel.prototype.createElement = function (x, y) {
            var segment = this.findSegmentWithY(y);
            var segmentIndex = this.segmentIndex(y);
            var isEmptySegment = !segment;
            var isActiveSegment = segmentIndex < this.inputSegmentIndex;
            if (isActiveSegment) {
                if (isEmptySegment) {
                    this.createLine(segmentIndex);
                }
                else {
                    var isCircleCreated = segment.createCircle(x);
                    var onCircleCreated = this.onCircleCreated();
                    if (isCircleCreated && onCircleCreated) {
                        onCircleCreated(x, this.segmentY(segmentIndex));
                    }
                }
            }
        };
        MainPageViewModel.prototype.clearAll = function () {
            for (var i = 0, last = this.resultSegmentIndex; i <= last; i += 1) {
                this.clearSegmentWithIndex(i);
            }
            this.onSegmentCleanUp(null);
            this.onSegmentCreated(null);
            this.onCircleCreated(null);
            this.onEndCircleMoved(null);
            this.onCircleMoved(null);
            this.onInputMethodSelected(null);
            this.onInputParameterSelected(null);
            this.onInputMethodScrolled(null);
            this.onResultCircleTapped(null);
            this.onResultCircleDragged(null);
            this.pointer(null);
            this.inputSegment().cleanUp();
            this.cleanUp();
        };
        MainPageViewModel.prototype.clearSegmentWithY = function (y) {
            this.clearSegment(this.findSegmentWithY(y));
        };
        MainPageViewModel.prototype.clearSegmentWithIndex = function (index) {
            this.clearSegment(this.findSegmentWithIndex(index));
        };
        MainPageViewModel.prototype.clearSegment = function (segment) {
            var inputSegment = this.inputSegment();
            if (segment && segment !== inputSegment) {
                segment.cleanUp();
                var segments = this.drawSegments;
                segments.splice(segments.indexOf(segment), 1);
                inputSegment.update();
                this.updateResult(segment.index());
                var onSegmentCleanUp = this.onSegmentCleanUp();
                if (onSegmentCleanUp && segment.index() !== this.resultSegmentIndex) {
                    onSegmentCleanUp(this.segmentY(segment.index()));
                }
            }
        };
        MainPageViewModel.prototype.getInputs = function () {
            var _this = this;
            return this.inputSegment().args().map(function (arg) {
                var segment = _.find(_this.drawSegments, function (seg) { return seg.name() === arg; });
                return MainPageViewModel.lineNames.indexOf(arg) > -1 ? segment ? segment.getTimestamps() : [] : arg;
            });
        };
        MainPageViewModel.prototype.isAnyCircleDragged = function () {
            return this.drawSegments.some(function (seg) { return seg.isAnyCircleDragged(); });
        };
        MainPageViewModel.prototype.stopMenuTransition = function () {
            var menu = this.menu();
            if (menu) {
                menu.style[RxDiagram.cssTransition] = '';
                menu.style[RxDiagram.cssTransform] = '';
            }
        };
        MainPageViewModel.prototype.hideAreaConfig = function () {
            var _this = this;
            return function (element, isInitialized) {
                if (!isInitialized) {
                    Rx.Observable.merge(rxia.tap(element), rxia.press(element), rxia.swipe(element), rxia.pan(element).move).subscribe(function () {
                        if (_this.isMenuShown()) {
                            _this.hidePopup(_this.menu, _this.isMenuShown, _this.menuTransitionStart());
                        }
                    });
                }
            };
        };
        MainPageViewModel.prototype.menuConfig = function (offset) {
            var _this = this;
            return function (element, isInitialized) {
                if (!isInitialized) {
                    _this.menu(element);
                    _this.showPopup(element, _this.menuTransitionStart, offset, 0);
                }
            };
        };
        MainPageViewModel.prototype.showPopup = function (element, counter, offsetX, offsetY) {
            var style = element.style;
            style[RxDiagram.cssTransform] = MainPageViewModel.popupTransform();
            _.delay(function () {
                style[RxDiagram.cssTransform] = MainPageViewModel.popupTransform(offsetX, offsetY);
                style[RxDiagram.cssTransition] = MainPageViewModel.popupTransition(MainPageViewModel.animDelay);
                counter(Date.now());
            }, 10);
        };
        MainPageViewModel.prototype.hidePopup = function (element, isShown, startTime) {
            var fallbackDuration = Date.now() - startTime;
            var style = element().style;
            var duration = MainPageViewModel.animDelay;
            style[RxDiagram.cssTransform] = MainPageViewModel.popupTransform();
            style[RxDiagram.cssTransition] = MainPageViewModel.popupTransition(fallbackDuration > duration ? duration : fallbackDuration);
            Rx.Observable.fromEvent(element(), RxDiagram.transitionEndEvent).take(1).delay(50).subscribe(function () {
                m.startComputation();
                isShown(false);
                element(null);
                m.endComputation();
            });
        };
        MainPageViewModel.popupTransform = function (offsetX, offsetY) {
            if (offsetX === void 0) { offsetX = 0; }
            if (offsetY === void 0) { offsetY = 0; }
            return 'translate3d(' + offsetX + 'px,' + offsetY + 'px,0)';
        };
        MainPageViewModel.popupTransition = function (duration) {
            return RxDiagram.prefixedTransform + ' ' + duration + 'ms linear';
        };
        MainPageViewModel.prototype.createTestLine = function (segmentIndex) {
            return this.createLine(segmentIndex);
        };
        MainPageViewModel.prototype.createLine = function (segmentIndex, isFinite, x2, bounds) {
            var _this = this;
            if (isFinite === void 0) { isFinite = true; }
            var rightLimit = x2 || this.rightLimit;
            var posY = this.segmentY(segmentIndex);
            var isResultSegment = segmentIndex === this.resultSegmentIndex;
            var lineViewModel = new RxDiagram.LineViewModel({
                viewClass: isResultSegment ? RxDiagram.ResultLineView : RxDiagram.LineView,
                updateResult: this.updateResult,
                index: segmentIndex,
                canEditCircles: function () { return _this.drawSegments.every(function (s) { return !s.isAnyCircleEdited(); }); },
                key: _.uniqueId(),
                x1: this.leftLimit,
                y1: posY,
                x2: rightLimit,
                y2: posY,
                circleRadius: this.circleRadius,
                isFinite: isFinite,
                name: MainPageViewModel.lineNames[segmentIndex],
                mainViewWidth: this.width(),
                mainViewHeight: this.height(),
                mainViewX: this.x(),
                mainViewY: this.y(),
                leftLimit: this.leftLimit,
                rightLimit: this.rightLimit,
                gradientId: this.gradientId(),
                bounds: bounds,
                onEndCircleMoved: this.onEndCircleMoved,
                onCircleMoved: this.onCircleMoved,
                onCircleTapped: isResultSegment ? this.onResultCircleTapped : undefined,
                onCircleDragged: isResultSegment ? this.onResultCircleDragged : undefined
            });
            this.drawSegments.push(lineViewModel);
            this.inputSegment().update();
            this.updateResult(segmentIndex);
            var callback = this.onSegmentCreated();
            if (callback && !isResultSegment) {
                callback(posY);
            }
            return lineViewModel;
        };
        MainPageViewModel.prototype.createInput = function (segmentIndex) {
            var _this = this;
            var segmentHeight = this.segmentHeight();
            var width = this.width();
            var inputViewModel = new RxDiagram.InputViewModel({
                viewClass: RxDiagram.InputView,
                updateResult: this.updateResult,
                index: this.inputSegmentIndex,
                lineSegmentNames: MainPageViewModel.lineNames.split(''),
                validLineSegmentNames: function () { return _this.lineSegments().map(function (x) { return x.name(); }); },
                x: width * 0.02,
                y: this.segmentY(segmentIndex) - segmentHeight / 2,
                width: width * 0.96,
                height: segmentHeight,
                gradientId: this.gradientId(),
                mainViewWidth: this.width(),
                mainViewHeight: this.height(),
                onMethodSelected: this.onInputMethodSelected,
                onParameterSelected: this.onInputParameterSelected,
                onLongMethodScrolled: this.onInputMethodScrolled
            });
            this.inputSelectCenter(inputViewModel.selectCenter());
            this.inputMethodCenter(inputViewModel.methodCenter());
            this.inputMethodFirstArgumentCenter = function () {
                var p = inputViewModel.firstMethodArgumentCenter();
                return {
                    x: p.x - _this.x(),
                    y: p.y - _this.y()
                };
            };
            this.selectInputOption = function (opt) { return inputViewModel.selectedOption(opt); };
            this.drawSegments.push(inputViewModel);
        };
        MainPageViewModel.prototype.segmentY = function (segmentIndex) {
            return this.segmentHeight() * (segmentIndex + (segmentIndex === this.resultSegmentIndex ? 1 : 0.5));
        };
        MainPageViewModel.prototype.segmentIndex = function (y) {
            var step = this.segmentHeight();
            var index = 0;
            while (y > step * index) {
                index += 1;
            }
            return index - 1;
        };
        MainPageViewModel.prototype.findSegmentWithIndex = function (index) {
            return _.find(this.drawSegments, function (seg) { return seg.index() === index; });
        };
        MainPageViewModel.prototype.findSegmentWithY = function (y) {
            return this.findSegmentWithIndex(this.segmentIndex(y));
        };
        MainPageViewModel.prototype.inputSegment = function () {
            return this.findSegmentWithIndex(this.inputSegmentIndex);
        };
        MainPageViewModel.prototype.config = function () {
            var _this = this;
            return function (element, isInitialized) {
                if (!isInitialized) {
                    _this.initTap(element);
                    _this.initSwipe(element);
                }
            };
        };
        MainPageViewModel.prototype.initTap = function (element) {
            var _this = this;
            var onTap = function (ev) {
                var x = ev.centerX - _this.x();
                var y = ev.centerY - _this.y();
                var target = (ev.target);
                var isTapInsideLimits = x >= _this.leftLimit && x <= _this.rightLimit;
                var shouldHandleTap = target === element || target.tagName === 'line';
                if (isTapInsideLimits && shouldHandleTap) {
                    m.startComputation();
                    _this.createElement(x, y);
                    m.endComputation();
                }
            };
            rxia.tap(element, {
                threshold: 50,
                preventDefault: false
            }).subscribe(onTap);
        };
        MainPageViewModel.prototype.initSwipe = function (element) {
            var _this = this;
            var onSwipe = function (ev) {
                var target = (ev.target);
                var segment = _this.findSegmentWithY(ev.centerY);
                var isValidGestureTarget = target === element || target.nodeName === 'line';
                var isLongSwipe = ev.distance >= _this.clearSwipeMinDistance;
                var isLastSegment = segment && (segment.index() === _this.resultSegmentIndex);
                var isHorizontalSwipe = RxDiagram.Geometry.isInsideRange(ev.angle, 135, 225) || RxDiagram.Geometry.isInsideRange(ev.angle, 315, 360) || RxDiagram.Geometry.isInsideRange(ev.angle, 0, 45);
                var canSwipe = isValidGestureTarget && isLongSwipe && isHorizontalSwipe && !isLastSegment;
                if (canSwipe) {
                    m.startComputation();
                    _this.clearSegmentWithY(ev.centerY - _this.y());
                    m.endComputation();
                }
            };
            rxia.swipe(element, {
                preventDefault: false
            }).subscribe(onSwipe);
        };
        MainPageViewModel.prototype.defaultEndCircleX = function () {
            return this.rightLimit;
        };
        MainPageViewModel.prototype.resultLineY = function () {
            return this.segmentY(this.resultSegmentIndex);
        };
        MainPageViewModel.computeDelay = 300;
        MainPageViewModel.lineNames = 'ABC';
        MainPageViewModel.animDelay = 150;
        return MainPageViewModel;
    })();
    RxDiagram.MainPageViewModel = MainPageViewModel;



    function l(text) {
        return Locales.localize(text);
    }
    RxDiagram.l = l;
    var Locales = (function () {
        function Locales() {
        }
        Locales.setLocale = function (locale) {
            Locales.currentLocale = locale.match(/[^-]*/)[0];
        };
        Locales.localize = function (text) {
            var locale = Locales.currentLocale;
            return (Locales.defaultLocale === Locales.currentLocale) ? text : Locales.strings[text][locale];
        };
        Locales.defaultLocale = 'en';
        Locales.currentLocale = 'en';
        Locales.strings = {
            'Tutorial': {
                ru: 'Туториал'
            },
            'About': {
                ru: 'О приложении'
            },
            "This app demonstrates some capabilities of awesome ": {
                ru: 'Это приложение демонстрирует некоторые возможности замечательной библиотеки '
            },
            " library. Inspired by André Staltz's ": {
                ru: '. Основано на идее André Staltz\'а - '
            },
            "RxJS version: ": {
                ru: 'Версия RxJS: '
            },
            'Send feedback': {
                ru: 'Написать отзыв'
            },
            '1. General info.': {
                ru: '1. Общая информация.'
            },
            'Screen is divided into three parts: ': {
                ru: 'Экран разделен на три части: '
            },
            'observables input area': {
                ru: 'область ввода наблюдаемых объектов (observables)'
            },
            'Rx.Observable method selection area': {
                ru: 'область выбора методов класса Rx.Observable'
            },
            ' and ': {
                ru: ' и '
            },
            'result area': {
                ru: 'область вывода'
            },
            '. You can create up to three observables, add numeric (0..99) or error values, ': {
                ru: '. Вы можете создать от одного до трех наблюдаемых объектов, добавлять числовые значения (0..99)' + ' и значения-ошибки, '
            },
            'select methods and adjust some of their parameters.': {
                ru: 'выбирать методы и изменять некоторые из их параметров.'
            },
            '2. Creating and deleting observables.': {
                ru: '2. Создание и удаление наблюдаемых объектов.'
            },
            'To create an observable just tap in input area. ': {
                ru: 'Чтобы создать наблюдаемый объект просто нажмите в области ввода. '
            },
            'Observables are marked as \'A\', \'B\' and \'C\'. ': {
                ru: 'Наблюдаемые объекты отмечены буквами \'A\', \'B\' и \'C\'. '
            },
            'Drag the right end of observable to change its length. ': {
                ru: 'Потяните за правый край наблюдаемого объекта, чтобы изменить его длину. '
            },
            'To delete created observable - swipe near it horizontally.': {
                ru: 'Чтобы удалить наблюдаемый объект - проведите около него пальцем горизонтально.'
            },
            '3. Creating values.': {
                ru: '3. Создание значений.'
            },
            'To create a value you need to tap on existing observable. ': {
                ru: 'Для создания значения нужно нажать на наблюдаемый объект. '
            },
            'Note that result area is also updated when you add values to \'A\' observable. ': {
                ru: 'Обратите внимание, что область вывода тоже обновляется, когда Вы добавляете значения к ' + 'наблюдаемому объекту \'A\'. '
            },
            'Also you can drag values to change their positions.': {
                ru: 'Также Вы можете тянуть значения пальцем, чтобы поменять их позиции.'
            },
            '4. Editing and deleting values.': {
                ru: '4. Изменение и удаление значений.'
            },
            'To edit or delete some value - press on it and hold until small menu appears. ': {
                ru: 'Чтобы изменить или удалить значение - нажмите на нем и удерживайте, пока не появится ' + 'небольшое меню. '
            },
            'Now it is possible ': {
                ru: 'Теперь можно '
            },
            'to edit selected value by scrolling vertically the left panel': {
                ru: 'изменить выбранное значение с помощью прокрутки левой панели'
            },
            'delete the value by pressing the right \'X\'-button ': {
                ru: 'удалить значение, нажав на правую кнопку \'X\' '
            },
            'or ': {
                ru: 'или '
            },
            'close this menu by tapping outside of it.': {
                ru: 'закрыть это меню нажав за его пределами.'
            },
            '7. Value types.': {
                ru: '7. Типы значений.'
            },
            '5. Choosing methods.': {
                ru: '5. Выбор методов.'
            },
            'You can choose one of the Rx.Observable class or instance methods ': {
                ru: 'Вы можете выбрать один из методов класса Rx.Observable или объекта типа Rx.Observable '
            },
            'with help of the drop down menu. ': {
                ru: 'с помощью выпадающего меню. '
            },
            'Also it is possible to customize colored arguments by vertical scrolling. ': {
                ru: 'Также возможно изменить цветные аргументы с помощью вертикальной прокрутки. '
            },
            'In case when the whole method invocation string is not visible ': {
                ru: 'В случае, когда строка метода не видна полностью, '
            },
            'you can scroll it horizontally.': {
                ru: 'Вы можете прокрутить ее горизонтально.'
            },
            '6. Interacting with result.': {
                ru: '6. Взаимодействие с результатом.'
            },
            'If you tap on a partially covered value it will be moved to the front. ': {
                ru: 'Если Вы нажмете на частично закрытое значение, то оно будет показано поверх остальных значений. '
            },
            'When vertical \'capsule\' is not fully visible - just drag it up.': {
                ru: 'Если вертикальная \'капсула\' видна не полностью - просто потяните ее вверх.'
            },
            'Boolean (T or F)': {
                ru: 'Boolean (T или F)'
            },
            'Values with equal positions': {
                ru: 'Значения с одинаковыми позициями'
            },
            'Do you want to see the tutorial?': {
                ru: 'Хотите ли Вы посмотреть туториал?'
            },
            'Yes': {
                ru: 'Да'
            },
            'No': {
                ru: 'Нет'
            },
            'Confirm': {
                ru: 'Вопрос'
            }
        };
        return Locales;
    })();
    RxDiagram.Locales = Locales;



    var MainPageView = (function () {
        function MainPageView() {
        }
        MainPageView.draw = function (vm) {
            var mainView = MainPageView.main(vm, 1);
            return vm.isUiPresent() ? [mainView, MainPageView.ui(vm, 2)] : mainView;
        };
        MainPageView.ui = function (vm, key) {
            var isMenuShown = vm.isMenuShown();
            var elements = isMenuShown ? [MainPageView.hideArea(vm), MainPageView.menu(vm)] : [];
            return m('svg', {
                width: vm.width(),
                height: vm.height(),
                style: 'position: absolute; pointer-events: ' + (isMenuShown ? 'auto;' : 'none;'),
                'class': 'noselect',
                key: key
            }, elements);
        };
        MainPageView.main = function (vm, key) {
            var elements = [
                RxDiagram.GradientView.draw(vm.gradientId()),
                MainPageView.bottomSegment(vm),
                MainPageView.groupedSegments(vm, 1),
                vm.isHighlighted() ? MainPageView.highlightedAreas(vm, 2) : null,
                vm.isUiPresent() ? MainPageView.menuButton(vm, 3) : null,
                vm.pointer() ? MainPageView.pointer(vm, 4) : null
            ];
            return m('svg', {
                width: vm.width(),
                height: vm.height(),
                style: 'position: absolute; ' + RxDiagram.prefixedTransform + ': translate(' + vm.x() + 'px,' + vm.y() + 'px);' + 'background-color: white;',
                'class': 'noselect',
                config: vm.config(),
                key: key
            }, elements);
        };
        MainPageView.pointer = function (vm, key) {
            return vm.pointer().view(m('circle', {
                r: vm.width() / 40,
                fill: 'red'
            }), key);
        };
        MainPageView.highlightedAreas = function (vm, key) {
            var width = vm.width(), height = vm.height(), segmentHeight = vm.segmentHeight();
            var strokeWidth = width * 0.025;
            var observablesHeight = (segmentHeight * 3);
            var inputHeight = segmentHeight;
            var resultHeight = (segmentHeight * 2);
            var offset = width * 0.025;
            var rectWidth = width * 0.95;
            return m('g', {
                key: key,
                style: 'stroke-opacity: 0.5; opacity: 0.25; stroke-width: ' + strokeWidth + 'px;'
            }, [
                m('rect', {
                    x: 0,
                    y: 0,
                    width: width,
                    height: height,
                    style: 'fill: transparent;'
                }),
                m('rect', {
                    x: offset,
                    y: offset,
                    width: rectWidth,
                    height: observablesHeight - offset * 2,
                    style: 'fill: red; stroke: red;'
                }),
                m('rect', {
                    x: offset,
                    y: observablesHeight + offset,
                    width: rectWidth,
                    height: inputHeight - offset * 2,
                    style: 'fill: green; stroke: green;'
                }),
                m('rect', {
                    x: offset,
                    y: observablesHeight + inputHeight + offset,
                    width: rectWidth,
                    height: resultHeight - offset * 2,
                    style: 'fill: blue; stroke: blue;'
                })
            ]);
        };
        MainPageView.bottomSegment = function (vm) {
            return vm.segments().filter(function (s) { return !s.drawInsideGroup(); }).map(function (s) { return s.drawView(); })[0];
        };
        MainPageView.groupedSegments = function (vm, key) {
            return m('g', { key: key }, vm.segments().filter(function (s) { return s.drawInsideGroup(); }).map(function (s) { return s.drawView(); }));
        };
        MainPageView.menuButton = function (vm, key) {
            var width = vm.width();
            var x = width * 0.97;
            var y = width * 0.03;
            var radius = width * 0.0045;
            var offset = width * 0.012;
            return m('g', {
                transform: Framework.matrix(x, y, 1, 1, 0),
                onclick: function () { return vm.isMenuShown(true); },
                key: key
            }, [
                m('rect', {
                    x: -y,
                    y: -y,
                    width: 2 * y,
                    height: 2 * y,
                    fill: 'transparent'
                }),
                m('path', {
                    d: RxDiagram.Geometry.capsule(radius, offset),
                    fill: 'gray',
                    transform: Framework.matrix(0, -2.5 * radius, 1, 1, 0)
                }),
                m('path', {
                    d: RxDiagram.Geometry.capsule(radius, offset),
                    fill: 'gray'
                }),
                m('path', {
                    d: RxDiagram.Geometry.capsule(radius, offset),
                    fill: 'gray',
                    transform: Framework.matrix(0, 2.5 * radius, 1, 1, 0)
                })
            ]);
        };
        MainPageView.hideArea = function (vm) {
            return m('rect', {
                x: 0,
                y: 0,
                width: vm.width(),
                height: vm.height(),
                fill: 'transparent',
                config: vm.hideAreaConfig()
            });
        };
        MainPageView.menu = function (vm) {
            var width = vm.width(), height = vm.height();
            var menuWidth = width * 0.25;
            var menuHeight = height * 0.2;
            var offset = width * 0.01;
            var rxy = RxDiagram.uiCornerRoundingCoef * width;
            var x = width + offset;
            var buttonX = menuWidth * 0.05 + x;
            var buttonY = menuHeight * 0.05 + offset;
            var fontSize = width * 0.02;
            var animOffset = -(menuWidth + 2 * offset);
            return m('g', {
                config: vm.menuConfig(animOffset),
                key: 4
            }, [
                m('rect', {
                    x: x,
                    y: offset,
                    rx: rxy,
                    ry: rxy,
                    width: menuWidth,
                    height: menuHeight,
                    stroke: '#cccccc',
                    style: 'fill: url(#' + vm.gradientId() + '); stroke-width: 2;'
                }),
                RxDiagram.SVGButtonWithText.view({
                    x: buttonX,
                    y: buttonY,
                    width: menuWidth * 0.9,
                    height: menuHeight * 0.4,
                    fill: RxDiagram.buttonColor,
                    press: vm.isTutorialButtonPressed,
                    onClick: vm.transitToTutorial,
                    text: RxDiagram.l('Tutorial'),
                    fontSize: fontSize,
                    radius: rxy
                }),
                RxDiagram.SVGButtonWithText.view({
                    x: buttonX,
                    y: buttonY + menuHeight * 0.5,
                    width: menuWidth * 0.9,
                    height: menuHeight * 0.4,
                    fill: RxDiagram.buttonColor,
                    press: vm.isAboutButtonPressed,
                    onClick: vm.transitToAbout,
                    text: RxDiagram.l('About'),
                    fontSize: fontSize,
                    radius: rxy
                })
            ]);
        };
        return MainPageView;
    })();
    RxDiagram.MainPageView = MainPageView;



    var MainPage = (function () {
        function MainPage() {
        }
        MainPage.create = function (x, y, width, height) {
            var vm = new RxDiagram.MainPageViewModel(x, y, width, height);
            return function () { return RxDiagram.MainPageView.draw(vm); };
        };
        return MainPage;
    })();
    RxDiagram.MainPage = MainPage;



    var BackButtonView = (function () {
        function BackButtonView() {
        }
        BackButtonView.draw = function (onClick, key, angle) {
            if (angle === void 0) { angle = 135; }
            var width = RxDiagram.screenWidth;
            var x = width * 0.97;
            var y = width * 0.03;
            var radius = width * 0.0045;
            var offset = width * 0.012;
            var arrowOffset = offset * 0.6;
            var arrowAngle = RxDiagram.Geometry.degToRad(angle);
            var arrowCenter = RxDiagram.Geometry.pointOnCircle(offset, 0, arrowOffset, arrowAngle);
            return m('g', {
                transform: Framework.matrix(x, y, 1, 1, 0),
                key: key,
                onclick: onClick
            }, [
                m('rect', {
                    x: -y,
                    y: -y,
                    width: 2 * y,
                    height: 2 * y,
                    fill: 'transparent'
                }),
                m('path', {
                    d: RxDiagram.Geometry.capsule(radius, arrowOffset),
                    fill: 'gray',
                    transform: Framework.matrix(arrowCenter.x, arrowCenter.y, 1, 1, RxDiagram.Geometry.degToRad(angle - 180))
                }),
                m('path', {
                    d: RxDiagram.Geometry.capsule(radius, offset),
                    fill: 'gray'
                }),
                m('path', {
                    d: RxDiagram.Geometry.capsule(radius, arrowOffset),
                    fill: 'gray',
                    transform: Framework.matrix(arrowCenter.x, -arrowCenter.y, 1, 1, RxDiagram.Geometry.degToRad(180 - angle))
                })
            ]);
        };
        return BackButtonView;
    })();
    RxDiagram.BackButtonView = BackButtonView;



    var TutorialPageViewModel = (function () {
        function TutorialPageViewModel() {
            var _this = this;
            this.transitBack = function () {
                RxDiagram.app.transitBackToMain(function () {
                    _this.pageModel().cleanUp();
                    _this.pageModel(null);
                    clearTimeout(_this.currentTimerId);
                });
            };
            this.onPrevClicked = function () {
                var page = _this.selectedPage();
                if (page > 0) {
                    _this.selectedPage(page - 1);
                }
            };
            this.onNextClicked = function () {
                var page = _this.selectedPage();
                if (page < (_this.pageCount() - 1)) {
                    _this.selectedPage(page + 1);
                }
            };
            this.onPageSelect = function () {
                _this.pageModel().clearAll();
                clearTimeout(_this.currentTimerId);
                if (_this.isGeneralInfoPage()) {
                    _this.startGeneralInfoScenario();
                }
                else if (_this.isCreatingAndDeletingObservablesPage()) {
                    _this.startCreatingAndDeletingObservablesScenario();
                }
                else if (_this.isCreatingValuesPage()) {
                    _this.startCreatingValuesScenario();
                }
                else if (_this.isEditingAndDeletingValuesPage()) {
                    _this.startEditingAndDeletingValuesScenario();
                }
                else if (_this.isChoosingMethodsPage()) {
                    _this.startChoosingMethodsScenario();
                }
                else if (_this.isInteractingWithResultPage()) {
                    _this.startInteractingWithResultScenario();
                }
            };
            this.removePointer = function () {
                var pageModel = _this.pageModel();
                m.startComputation();
                _this.highlightRed();
                pageModel.pointer(null);
                m.endComputation();
                pageModel.onSegmentCleanUp(null);
            };
            this.menuWidth = m.prop(TutorialPageViewModel.width());
            this.isPrevPressed = m.prop(false);
            this.isNextPressed = m.prop(false);
            this.pageCount = m.prop(7);
            this.highlights = m.prop(['black', 'black', 'black', 'black']);
            this.pageModel = m.prop(new RxDiagram.MainPageViewModel(TutorialPageViewModel.x(), TutorialPageViewModel.y(), TutorialPageViewModel.width(), TutorialPageViewModel.height(), {
                isUiPresent: false,
                isHighlighted: true
            }));
            this.selectedPage = Framework.prop(0, this.onPageSelect);
        }
        TutorialPageViewModel.prototype.infoConfig = function () {
            var _this = this;
            return function (elem, init, context) {
                if (init && context.page !== _this.selectedPage()) {
                    context.component.scrollTo(0, 0);
                    Framework.Scroller.updateBounds(context.component, undefined, function (x) {
                        return x.getBoundingClientRect().height;
                    });
                }
                context.page = _this.selectedPage();
            };
        };
        TutorialPageViewModel.prototype.isGeneralInfoPage = function () {
            return this.selectedPage() === 0;
        };
        TutorialPageViewModel.prototype.isCreatingAndDeletingObservablesPage = function () {
            return this.selectedPage() === 1;
        };
        TutorialPageViewModel.prototype.isCreatingValuesPage = function () {
            return this.selectedPage() === 2;
        };
        TutorialPageViewModel.prototype.isEditingAndDeletingValuesPage = function () {
            return this.selectedPage() === 3;
        };
        TutorialPageViewModel.prototype.isChoosingMethodsPage = function () {
            return this.selectedPage() === 4;
        };
        TutorialPageViewModel.prototype.isInteractingWithResultPage = function () {
            return this.selectedPage() === 5;
        };
        TutorialPageViewModel.prototype.isValueTypesPage = function () {
            return this.selectedPage() === 6;
        };
        TutorialPageViewModel.prototype.startGeneralInfoScenario = function () {
            var pageModel = this.pageModel();
            m.startComputation();
            pageModel.isHighlighted(true);
            m.endComputation();
        };
        TutorialPageViewModel.prototype.startCreatingAndDeletingObservablesScenario = function () {
            var _this = this;
            var pageModel = this.pageModel();
            var halfWidth = TutorialPageViewModel.width() / 2;
            var quarterWidth = halfWidth / 2;
            m.startComputation();
            this.highlightRed(0);
            pageModel.pointer(new RxDiagram.SVGPointer(halfWidth, TutorialPageViewModel.height() / 4, 1).tap(1500).infinite());
            pageModel.isHighlighted(false);
            m.endComputation();
            pageModel.onSegmentCreated(function (segY) {
                pageModel.onSegmentCreated(null);
                m.startComputation();
                _this.highlightRed(1);
                pageModel.pointer(new RxDiagram.SVGPointer(pageModel.defaultEndCircleX(), segY, 2).press(750).moveBy(2000, -quarterWidth, 0).release(750).infinite());
                m.endComputation();
                pageModel.onEndCircleMoved(_.debounce(function (x, y) {
                    if (y === segY) {
                        pageModel.onEndCircleMoved(null);
                        var offset = TutorialPageViewModel.height() / 24;
                        m.startComputation();
                        _this.highlightRed(2);
                        pageModel.pointer(new RxDiagram.SVGPointer(quarterWidth, y + offset, 3).press(750).moveBy(1000, halfWidth, -2 * offset).release(750).infinite());
                        m.endComputation();
                    }
                }, TutorialPageViewModel.debounceInterval));
                pageModel.onSegmentCleanUp(function (y) {
                    if (y === segY) {
                        _this.removePointer();
                        pageModel.onEndCircleMoved(null);
                    }
                });
            });
        };
        TutorialPageViewModel.prototype.startCreatingValuesScenario = function () {
            var _this = this;
            var pageModel = this.pageModel();
            var halfWidth = TutorialPageViewModel.width() / 2;
            var quarterWidth = halfWidth / 2;
            this.highlightRed(0);
            pageModel.onSegmentCreated(function (segY) {
                pageModel.onSegmentCreated(null);
                m.startComputation();
                pageModel.pointer(new RxDiagram.SVGPointer(quarterWidth, segY, 1).tap(1500).infinite());
                m.endComputation();
                pageModel.onCircleCreated(function (x, y) {
                    if (y === segY) {
                        pageModel.onCircleCreated(null);
                        m.endComputation();
                        _this.highlightRed(1);
                        pageModel.pointer(new RxDiagram.SVGPointer(x, y, 2).press(750).moveBy(2000, (x > halfWidth) ? -quarterWidth : quarterWidth, 0).release(750).infinite());
                        m.endComputation();
                    }
                });
                pageModel.onSegmentCleanUp(function (y) {
                    if (y === segY) {
                        _this.removePointer();
                        pageModel.onCircleMoved(null);
                        pageModel.onCircleCreated(null);
                    }
                });
            });
            pageModel.onCircleMoved(_.debounce(function () {
                pageModel.onCircleMoved(null);
                _this.removePointer();
            }, TutorialPageViewModel.debounceInterval));
            pageModel.createTestLine(0);
        };
        TutorialPageViewModel.prototype.startEditingAndDeletingValuesScenario = function () {
            var _this = this;
            var pageModel = this.pageModel();
            var circleX = TutorialPageViewModel.width() / 2;
            this.highlightRed(0);
            pageModel.onInputMethodSelected(null);
            pageModel.onSegmentCreated(function (circleY) {
                pageModel.onSegmentCreated(null);
                m.startComputation();
                pageModel.pointer(new RxDiagram.SVGPointer(circleX, circleY, 1).tapAndHold(1500, 2500).infinite());
                m.endComputation();
                pageModel.onSegmentCleanUp(_this.removePointer);
            });
            var line = pageModel.createTestLine(1);
            var circle = line.createTestCircle(circleX, '99');
            circle.onEditMenuCreated(function (x, y) {
                circle.onEditMenuCreated(null);
                m.startComputation();
                _this.highlights(['black', 'red', 'green', 'blue']);
                pageModel.pointer(new RxDiagram.SVGPointer(x, y, 2).press(750).moveBy(500, 0, -TutorialPageViewModel.height() / 6).release(750).infinite());
                m.endComputation();
                circle.onEditMenuInteraction(function () {
                    pageModel.onCircleMoved(null);
                    _this.removePointer();
                });
            });
        };
        TutorialPageViewModel.prototype.startChoosingMethodsScenario = function () {
            var _this = this;
            var pageModel = this.pageModel();
            var circleOffset = TutorialPageViewModel.width() / 8;
            var firstLine = pageModel.createTestLine(0);
            var secondLine = pageModel.createTestLine(1);
            var thirdLine = pageModel.createTestLine(2);
            pageModel.onResultCircleTapped(null);
            pageModel.onResultCircleDragged(null);
            firstLine.createTestCircle(circleOffset * 6, '1');
            secondLine.createTestCircle(circleOffset * 4, '2');
            thirdLine.createTestCircle(circleOffset * 5, '3');
            var selectCenter = pageModel.inputSelectCenter();
            m.startComputation();
            this.highlightRed(0);
            pageModel.pointer(new RxDiagram.SVGPointer(selectCenter.x, selectCenter.y, 1).tap(1500).infinite());
            m.endComputation();
            pageModel.onInputMethodSelected(function () {
                pageModel.onInputMethodSelected(null);
                _this.currentTimerId = _.delay(function () {
                    var firstArgumentCenter = pageModel.inputMethodFirstArgumentCenter();
                    m.startComputation();
                    _this.highlightRed(1);
                    pageModel.pointer(new RxDiagram.SVGPointer(firstArgumentCenter.x, firstArgumentCenter.y, 2).press(750).moveBy(500, 0, -TutorialPageViewModel.height() / 10).release(750).infinite());
                    m.endComputation();
                }, TutorialPageViewModel.inputUpdateDelay);
                pageModel.onInputParameterSelected(function () {
                    pageModel.onInputParameterSelected(null);
                    m.startComputation();
                    _this.highlightRed();
                    pageModel.pointer(null);
                    m.endComputation();
                    _this.currentTimerId = _.delay(function () {
                        var methodCenter = pageModel.inputMethodCenter();
                        m.startComputation();
                        pageModel.selectInputOption(TutorialPageViewModel.longMethodName);
                        _this.highlightRed(2);
                        pageModel.pointer(new RxDiagram.SVGPointer(methodCenter.x, methodCenter.y, 3).press(750).moveBy(1000, -TutorialPageViewModel.width() / 4, 0).release(750).infinite());
                        m.endComputation();
                        pageModel.onInputMethodScrolled(function () {
                            pageModel.onInputMethodScrolled(null);
                            _this.removePointer();
                        });
                    }, TutorialPageViewModel.reactionDelay);
                });
            });
        };
        TutorialPageViewModel.prototype.startInteractingWithResultScenario = function () {
            var _this = this;
            var pageModel = this.pageModel();
            var line = pageModel.createTestLine(0);
            var circleOffset = TutorialPageViewModel.width() / 38;
            var startX = line.x1();
            pageModel.onInputMethodSelected(null);
            for (var i = 0; i < 8; i += 1) {
                line.createTestCircle(i * circleOffset + startX, (i * 10) + '');
            }
            pageModel.onResultCircleTapped(function () {
                pageModel.onResultCircleTapped(null);
                m.startComputation();
                _this.highlightRed();
                pageModel.pointer(null);
                m.endComputation();
                _this.currentTimerId = _.delay(function () {
                    m.startComputation();
                    _this.highlightRed(1);
                    line.endCircle().x(line.x1());
                    pageModel.selectInputOption(TutorialPageViewModel.capsuleResultMethodName);
                    pageModel.pointer(new RxDiagram.SVGPointer(startX, pageModel.resultLineY(), 2).press(750).moveBy(1000, 0, -TutorialPageViewModel.height() / 2).release(750).infinite());
                    m.endComputation();
                    pageModel.onResultCircleDragged(function () {
                        pageModel.onResultCircleDragged(null);
                        _this.removePointer();
                    });
                }, TutorialPageViewModel.reactionDelay);
            });
            m.startComputation();
            this.highlightRed(0);
            pageModel.selectInputOption(TutorialPageViewModel.arrayResultMethodName);
            pageModel.pointer(new RxDiagram.SVGPointer(startX + 2 * circleOffset, pageModel.resultLineY(), 1).tap(1500).infinite());
            m.endComputation();
            pageModel.onSegmentCleanUp(this.removePointer);
        };
        TutorialPageViewModel.prototype.highlightRed = function (index) {
            this.highlights(this.highlights().map(function () { return 'black'; }));
            if (index !== undefined) {
                this.highlights()[index] = 'red';
            }
        };
        TutorialPageViewModel.standaloneValue = function (value, fill, x, y, offest) {
            var width = TutorialPageViewModel.width();
            var radius = width / 12;
            var fontSize = radius * 1.25;
            return {
                value: value,
                x: x,
                y: y,
                scale: 1,
                rotation: 0,
                key: +_.uniqueId(),
                isError: value === RxDiagram.RxModel.errorValue(),
                radius: radius,
                fill: fill,
                stroke: 'black',
                capsuleOffset: offest === undefined ? radius : offest,
                fontSize: fontSize,
                textY: fontSize / 2.8,
                textFill: 'white',
                isTextStroked: true,
                config: function () {
                }
            };
        };
        TutorialPageViewModel.standaloneValueCaptionFontSize = function (def) {
            return def.fontSize / 3;
        };
        TutorialPageViewModel.x = m.prop(RxDiagram.screenWidth / 5);
        TutorialPageViewModel.y = m.prop(RxDiagram.screenHeight / 4);
        TutorialPageViewModel.width = m.prop(RxDiagram.screenWidth * 0.6);
        TutorialPageViewModel.height = m.prop(RxDiagram.screenHeight * 0.6);
        TutorialPageViewModel.debounceInterval = 100;
        TutorialPageViewModel.inputUpdateDelay = 100;
        TutorialPageViewModel.reactionDelay = 2000;
        TutorialPageViewModel.longMethodName = 'combineLatest';
        TutorialPageViewModel.arrayResultMethodName = 'buffer';
        TutorialPageViewModel.capsuleResultMethodName = 'amb';
        return TutorialPageViewModel;
    })();
    RxDiagram.TutorialPageViewModel = TutorialPageViewModel;



    var TutorialPageView = (function () {
        function TutorialPageView() {
        }
        TutorialPageView.draw = function (vm) {
            var isValueTypesPage = vm.isValueTypesPage();
            var ui = m('svg', {
                width: RxDiagram.screenWidth,
                height: RxDiagram.screenHeight,
                style: 'background-color: whitesmoke;',
                'class': 'noselect'
            }, [
                TutorialPageView.info(vm),
                RxDiagram.BackButtonView.draw(vm.transitBack, 1),
                TutorialPageView.pageControl(vm),
                isValueTypesPage ? TutorialPageView.values() : null
            ]);
            return [
                m('div', {
                    key: vm.selectedPage(),
                }, isValueTypesPage ? null : RxDiagram.MainPageView.draw(vm.pageModel())),
                ui
            ];
        };
        TutorialPageView.info = function (vm) {
            var text;
            var fontSize = RxDiagram.screenHeight * 0.02;
            var padding = RxDiagram.screenHeight * 0.01;
            var x = RxDiagram.screenWidth * 0.2;
            var y = RxDiagram.screenHeight * 0.05;
            var width = vm.menuWidth();
            var height = RxDiagram.screenHeight * 0.15;
            var isWebkit = Framework.isWebkit();
            return m('foreignObject', {
                x: x,
                y: y,
                width: width,
                height: height
            }, Framework.Scroller.view('div', {
                xmlns: RxDiagram.xmlns,
                style: 'top: ' + (isWebkit ? y : 0) + 'px;' + 'left: ' + (isWebkit ? x : 0) + 'px;' + 'height: ' + height + 'px;' + 'position: fixed;',
                options: {
                    scrollX: false,
                    bounce: false,
                    scrollbars: true
                },
                config: vm.infoConfig()
            }, m('div', m('div', {
                style: 'font-size: ' + fontSize + 'px; ' + 'background-color: white; ' + 'text-align: justify; ' + 'padding-left: ' + padding + 'px; ' + 'padding-right: ' + (padding * 1.5) + 'px; ' + 'padding-bottom: ' + padding + 'px;'
            }, TutorialPageView.pageDescription(vm)))));
        };
        TutorialPageView.pageControl = function (vm) {
            var pageModel = vm.pageModel();
            var buttonTop = pageModel.y() + pageModel.height();
            var buttonY = (RxDiagram.screenHeight - buttonTop) / 2 + buttonTop;
            var buttonX = RxDiagram.screenHeight - buttonY;
            var circleOffset = pageModel.x();
            var circleStep = pageModel.width() / (vm.pageCount() + 1);
            return [
                TutorialPageView.pageControlButton(buttonX, buttonY, -1, vm.isPrevPressed, vm.onPrevClicked),
                _.times(vm.pageCount(), function (idx) { return TutorialPageView.pageControlCircle(circleOffset + circleStep * (idx + 1), buttonY, idx === vm.selectedPage(), idx * 10); }),
                TutorialPageView.pageControlButton(RxDiagram.screenWidth - buttonX, buttonY, 1, vm.isNextPressed, vm.onNextClicked)
            ];
        };
        TutorialPageView.pageControlCircle = function (x, y, isSelected, key) {
            var radius = (RxDiagram.screenHeight - y) * 0.2;
            return m('g', {
                key: key
            }, [
                m('circle', {
                    r: radius,
                    fill: '#777777',
                    cx: x,
                    cy: y
                }),
                isSelected ? m('circle', {
                    r: radius * 0.85,
                    fill: 'whitesmoke',
                    cx: x,
                    cy: y
                }) : null
            ]);
        };
        TutorialPageView.pageControlButton = function (x, y, facing, isPressed, onClick) {
            var side = RxDiagram.screenHeight - y;
            var halfSize = side / 2;
            return RxDiagram.SVGButton.view({
                x: x - halfSize,
                y: y - halfSize,
                width: side,
                height: side,
                fill: RxDiagram.buttonColor,
                press: isPressed,
                onClick: onClick,
                key: facing + 3,
                children: [
                    TutorialPageView.arrow(x, y, side * facing)
                ]
            });
        };
        TutorialPageView.arrow = function (x, y, buttonSide) {
            var offsetX = buttonSide / 4;
            var offsetY = buttonSide / 4;
            return m('polyline', {
                points: "" + (x - offsetX) + "," + (y - offsetY) + " " + (x + offsetX) + "," + y + " " + (x - offsetX) + "," + (y + offsetY),
                stroke: 'black'
            });
        };
        TutorialPageView.pageDescription = function (vm) {
            var highlights = vm.highlights();
            if (vm.isGeneralInfoPage()) {
                return [
                    m('h2', {
                        style: 'margin-top: 0%;'
                    }, RxDiagram.l('1. General info.')),
                    m('a', RxDiagram.l('Screen is divided into three parts: ')),
                    m('a', {
                        style: 'color: red;'
                    }, RxDiagram.l('observables input area')),
                    m('a', ', '),
                    m('a', {
                        style: 'color: green;'
                    }, RxDiagram.l('Rx.Observable method selection area')),
                    m('a', RxDiagram.l(' and ')),
                    m('a', {
                        style: 'color: blue;'
                    }, RxDiagram.l('result area')),
                    m('a', RxDiagram.l('. You can create up to three observables, add numeric (0..99) or error values, ') + RxDiagram.l('select methods and adjust some of their parameters.'))
                ];
            }
            if (vm.isCreatingAndDeletingObservablesPage()) {
                return [
                    m('h2', {
                        style: 'margin-top: 0%;'
                    }, RxDiagram.l('2. Creating and deleting observables.')),
                    m('a', {
                        style: "color: " + highlights[0] + ";"
                    }, RxDiagram.l('To create an observable just tap in input area. ')),
                    m('a', RxDiagram.l('Observables are marked as \'A\', \'B\' and \'C\'. ')),
                    m('a', {
                        style: "color: " + highlights[1] + ";"
                    }, RxDiagram.l('Drag the right end of observable to change its length. ')),
                    m('a', {
                        style: "color: " + highlights[2] + ";"
                    }, RxDiagram.l('To delete created observable - swipe near it horizontally.'))
                ];
            }
            if (vm.isCreatingValuesPage()) {
                return [
                    m('h2', {
                        style: 'margin-top: 0%;'
                    }, RxDiagram.l('3. Creating values.')),
                    m('a', {
                        style: "color: " + highlights[0] + ";"
                    }, RxDiagram.l('To create a value you need to tap on existing observable. ')),
                    m('a', RxDiagram.l('Note that result area is also updated when you add values to \'A\' observable. ')),
                    m('a', {
                        style: "color: " + highlights[1] + ";"
                    }, RxDiagram.l('Also you can drag values to change their positions.'))
                ];
            }
            if (vm.isEditingAndDeletingValuesPage()) {
                return [
                    m('h2', {
                        style: 'margin-top: 0%;'
                    }, RxDiagram.l('4. Editing and deleting values.')),
                    m('a', {
                        style: "color: " + highlights[0] + ";"
                    }, RxDiagram.l('To edit or delete some value - press on it and hold until small menu appears. ')),
                    m('a', RxDiagram.l('Now it is possible ')),
                    m('a', {
                        style: "color: " + highlights[1] + ";"
                    }, RxDiagram.l('to edit selected value by scrolling vertically the left panel')),
                    m('a', ', '),
                    m('a', {
                        style: "color: " + highlights[2] + ";"
                    }, RxDiagram.l('delete the value by pressing the right \'X\'-button ')),
                    m('a', RxDiagram.l('or ')),
                    m('a', {
                        style: "color: " + highlights[3] + ";"
                    }, RxDiagram.l('close this menu by tapping outside of it.')),
                ];
            }
            if (vm.isValueTypesPage()) {
                return [
                    m('h2', {
                        style: 'margin-top: 0%;'
                    }, RxDiagram.l('7. Value types.'))
                ];
            }
            if (vm.isChoosingMethodsPage()) {
                return [
                    m('h2', {
                        style: 'margin-top: 0%;'
                    }, RxDiagram.l('5. Choosing methods.')),
                    m('a', {
                        style: "color: " + highlights[0] + ";"
                    }, RxDiagram.l('You can choose one of the Rx.Observable class or instance methods ') + RxDiagram.l('with help of the drop down menu. ')),
                    m('a', {
                        style: "color: " + highlights[1] + ";"
                    }, RxDiagram.l('Also it is possible to customize colored arguments by vertical scrolling. ')),
                    m('a', {
                        style: "color: " + highlights[2] + ";"
                    }, RxDiagram.l('In case when the whole method invocation string is not visible ') + RxDiagram.l('you can scroll it horizontally.')),
                ];
            }
            if (vm.isInteractingWithResultPage()) {
                return [
                    m('h2', {
                        style: 'margin-top: 0%;'
                    }, RxDiagram.l('6. Interacting with result.')),
                    m('a', {
                        style: "color: " + highlights[0] + ";"
                    }, RxDiagram.l('If you tap on a partially covered value it will be moved to the front. ')),
                    m('a', {
                        style: "color: " + highlights[1] + ";"
                    }, RxDiagram.l('When vertical \'capsule\' is not fully visible - just drag it up.')),
                ];
            }
        };
        TutorialPageView.values = function () {
            var x = RxDiagram.TutorialPageViewModel.x();
            var y = RxDiagram.TutorialPageViewModel.y() * 0.7;
            var width = RxDiagram.TutorialPageViewModel.width();
            var height = RxDiagram.TutorialPageViewModel.height();
            var firstRowY = (y + height / 4) * 0.8;
            var secondRowY = y + height / 4 * 3;
            var firstColX = x + width / 6;
            var secondColX = x + width / 2;
            var thirdColX = x + width / 6 * 5;
            var red = RxDiagram.CircleViewModel.redFiller();
            var blue = RxDiagram.CircleViewModel.blueFiller();
            var gray = RxDiagram.CircleViewModel.grayFiller();
            var purple = RxDiagram.CircleViewModel.purpleFiller();
            var groupFirstDef = RxDiagram.TutorialPageViewModel.standaloneValue(10, red, thirdColX, secondRowY, 0);
            var groupCaptionSize = RxDiagram.TutorialPageViewModel.standaloneValueCaptionFontSize(groupFirstDef);
            return [
                TutorialPageView.drawValue(99, 'Number', red, firstColX, firstRowY),
                TutorialPageView.drawValue(true, RxDiagram.l('Boolean (T or F)'), blue, secondColX, firstRowY),
                TutorialPageView.drawValue(RxDiagram.RxModel.errorValue(), 'Error', '', thirdColX, firstRowY),
                TutorialPageView.drawValue(undefined, 'undefined', gray, firstColX, secondRowY),
                TutorialPageView.drawValue([1, 2, 3], 'Array', purple, secondColX, secondRowY),
                RxDiagram.ResultLineView.group([
                    groupFirstDef,
                    RxDiagram.TutorialPageViewModel.standaloneValue(20, RxDiagram.CircleViewModel.greenFiller(), thirdColX, secondRowY, 0)
                ], y),
                TutorialPageView.valueCaption(RxDiagram.l('Values with equal positions'), groupCaptionSize, thirdColX, secondRowY + groupFirstDef.fontSize * 2.1)
            ];
        };
        TutorialPageView.drawValue = function (value, text, fill, x, y) {
            var def = RxDiagram.TutorialPageViewModel.standaloneValue(value, fill, x, y);
            return [
                RxDiagram.CircleView.drawFromDef(def),
                TutorialPageView.valueCaption(text, RxDiagram.TutorialPageViewModel.standaloneValueCaptionFontSize(def), x, y + def.radius * 1.5)
            ];
        };
        TutorialPageView.valueCaption = function (text, fontSize, x, y) {
            return m('text', {
                fill: 'black',
                'text-anchor': 'middle',
                'font-size': fontSize,
                style: "" + RxDiagram.prefixedTransform + ": " + Framework.matrix3d(x, y, 1, 1, 0)
            }, text);
        };
        return TutorialPageView;
    })();
    RxDiagram.TutorialPageView = TutorialPageView;



    var TutorialPage = (function () {
        function TutorialPage() {
        }
        TutorialPage.create = function () {
            var vm = new RxDiagram.TutorialPageViewModel();
            return function () { return RxDiagram.TutorialPageView.draw(vm); };
        };
        return TutorialPage;
    })();
    RxDiagram.TutorialPage = TutorialPage;



    var AboutViewModel = (function () {
        function AboutViewModel() {
            this.sendFeedback = function () {
                var emailComposer = cordova.plugins.email;
                emailComposer.isAvailable(function (isAvailable) {
                    if (isAvailable) {
                        emailComposer.open({
                            to: 'iyegoroff@yandex.ru',
                            subject: 'RxDiagram'
                        });
                    }
                    else {
                        alert('Email is not available.');
                    }
                });
            };
            this.gradientId = m.prop(_.uniqueId(RxDiagram.standardGradientId));
            this.feedbackPressed = m.prop(false);
        }
        return AboutViewModel;
    })();
    RxDiagram.AboutViewModel = AboutViewModel;



    var AboutView = (function () {
        function AboutView() {
        }
        AboutView.draw = function (vm) {
            var width = RxDiagram.screenWidth, height = RxDiagram.screenHeight;
            var x = width * 0.1;
            var y = width * 0.1;
            var aboutWidth = width * 0.8;
            var aboutHeight = height - 2 * x;
            var fontSize = height * 0.04;
            var buttonFontSize = width * 0.02;
            var offset = x * 0.5;
            var buttonWidth = width * 0.21;
            var buttonHeight = width * 0.07;
            var buttonX = x + (aboutWidth - buttonWidth) * 0.5;
            var buttonY = y + aboutHeight - buttonHeight - offset;
            return m('svg', {
                width: width,
                height: height,
                'class': 'noselect'
            }, [
                RxDiagram.GradientView.draw(vm.gradientId()),
                RxDiagram.BackButtonView.draw(RxDiagram.app.transitBackToMain),
                m('rect', {
                    x: x,
                    y: y,
                    width: aboutWidth,
                    height: aboutHeight,
                    rx: 10,
                    ry: 10,
                    stroke: '#cccccc',
                    style: 'fill: url(#' + vm.gradientId() + '); stroke-width: 2;'
                }),
                m('foreignObject', {
                    x: x + offset,
                    y: y + offset,
                    width: aboutWidth - 2 * offset,
                    height: aboutHeight - 2 * offset
                }, m('div', {
                    xmlns: RxDiagram.xmlns,
                    style: 'text-align: justify; font-size: ' + fontSize + 'px;',
                    'class': 'noselect'
                }, m.trust(RxDiagram.l("This app demonstrates some capabilities of awesome ") + "<a href='#' onclick=\"window.open(\'https://github.com/Reactive-Extensions/RxJS/\', \'_system\');\">RxJS</a>" + RxDiagram.l(" library. Inspired by André Staltz's ") + "<a href='#' onclick=\"window.open(\'https://github.com/staltz/rxmarbles/\', \'_system\');\">rxmarbles</a>" + ".<p>" + RxDiagram.l("RxJS version: ") + RxDiagram.rxVersion + "</p>"))),
                RxDiagram.SVGButtonWithText.view({
                    x: buttonX,
                    y: buttonY,
                    fontSize: buttonFontSize,
                    fill: RxDiagram.buttonColor,
                    width: buttonWidth,
                    height: buttonHeight,
                    press: vm.feedbackPressed,
                    onClick: vm.sendFeedback,
                    text: RxDiagram.l('Send feedback')
                })
            ]);
        };
        return AboutView;
    })();
    RxDiagram.AboutView = AboutView;



    var AboutPage = (function () {
        function AboutPage() {
        }
        AboutPage.create = function () {
            var vm = new RxDiagram.AboutViewModel();
            return function () { return RxDiagram.AboutView.draw(vm); };
        };
        return AboutPage;
    })();
    RxDiagram.AboutPage = AboutPage;



    var App = (function (_super) {
        __extends(App, _super);
        function App() {
            var _this = this;
            _super.call(this, RxDiagram.MainPage.create(0, 0, RxDiagram.screenWidth, RxDiagram.screenHeight), function () {
                window.StatusBar.hide();
                navigator.globalization.getPreferredLanguage(function (language) { return RxDiagram.Locales.setLocale(language.value); }, function () {
                });
            });
            this.transitToAbout = function (onFinish) {
                _this.transitToPage(RxDiagram.AboutPage.create(), Framework.Transition.slideFromLeft(App.delay), onFinish);
            };
            this.transitToTutorial = function (onFinish) {
                if (App.isTutorialNeeded()) {
                    App.tutorialWasShown();
                }
                _this.transitToPage(RxDiagram.TutorialPage.create(), Framework.Transition.slideFromLeft(App.delay), onFinish);
            };
            this.transitBackToMain = function (onFinish) {
                _this.popBack(Framework.Transition.slideFromRight(App.delay), onFinish);
            };
            this.askForTurorial();
        }
        App.prototype.askForTurorial = function () {
            var _this = this;
            if (App.isTutorialNeeded()) {
                _.delay(function () {
                    if (App.isTutorialNeeded()) {
                        navigator.notification.confirm(RxDiagram.l('Do you want to see the tutorial?'), function (choice) {
                            if (choice === 1) {
                                m.startComputation();
                                _this.transitToTutorial();
                                m.endComputation();
                            }
                            App.tutorialWasShown();
                        }, RxDiagram.l('Confirm'), [RxDiagram.l('Yes'), RxDiagram.l('No')]);
                    }
                }, App.tutorialAlertDelay);
            }
        };
        App.isTutorialNeeded = function () {
            return !localStorage.getItem(App.tutorialKey);
        };
        App.tutorialWasShown = function () {
            localStorage.setItem(App.tutorialKey, 'true');
        };
        App.tutorialAlertDelay = 5000;
        App.delay = 150;
        App.tutorialKey = 'tutorial_was_shown';
        return App;
    })(Framework.App);
    RxDiagram.App = App;
    RxDiagram.app = new App();

})(RxDiagram || (RxDiagram = {}));
